#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Progress Map\n"
"POT-Creation-Date: 2016-05-10 01:31+0000\n"
"PO-Revision-Date: 2016-03-06 23:34+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;esc_attr__;esc_html__;esc_attr_e;esc_html_e\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: css\n"
"X-Poedit-SearchPathExcluded-1: img\n"
"X-Poedit-SearchPathExcluded-2: js\n"
"X-Poedit-SearchPathExcluded-3: libs\n"
"X-Poedit-SearchPathExcluded-4: settings\n"

#: progress-map.php:377
msgid "More"
msgstr ""

#: progress-map.php:690
msgid "Enter City & Province, or Postal code"
msgstr ""

#: progress-map.php:691
msgid "Expand the search area up to"
msgstr ""

#: progress-map.php:692
msgid "We could not find any location"
msgstr ""

#: progress-map.php:693
msgid "We could not understand the location"
msgstr ""

#: progress-map.php:694
msgid "- Make sure all street and city names are spelled correctly."
msgstr ""

#: progress-map.php:695
msgid "- Make sure your address includes a city and state."
msgstr ""

#: progress-map.php:696
msgid "- Try entering a zip code."
msgstr ""

#: progress-map.php:697
msgid "Find it"
msgstr ""

#: progress-map.php:798
msgid "Progress map"
msgstr ""

#: progress-map.php:862
msgid "Getting started"
msgstr ""

#: progress-map.php:863
msgid "Documentation"
msgstr ""

#: progress-map.php:1193
msgid "Give Maps permission to use your location!"
msgstr ""

#: progress-map.php:1194
msgid ""
"If you can't center the map on your location, a couple of things might be "
"going on. It's possible you denied Google Maps access to your location in "
"the past, or your browser might have an error."
msgstr ""

#: progress-map.php:1195
msgid ""
"IMPORTANT NOTE: Browsers no longer supports obtaining the user's location "
"using the HTML5 Geolocation API from pages delivered by non-secure "
"connections. This means that the page that's making the Geolocation API call "
"must be served from a secure context such as HTTPS."
msgstr ""

#: progress-map.php:1201
msgid "Click to view all markers in this area"
msgstr ""

#: progress-map.php:1558
msgid "PM. Coordinates"
msgstr ""

#: progress-map.php:1594 progress-map.php:1608
msgid "Progress Map: Add Locations"
msgstr ""

#: progress-map.php:1651
msgid "Enter an address"
msgstr ""

#: progress-map.php:1655 progress-map.php:3266 progress-map.php:3271
#: progress-map.php:7902
msgid "Search"
msgstr ""

#: progress-map.php:1674
msgid "Marker Icon"
msgstr ""

#: progress-map.php:1676
msgid "Upload"
msgstr ""

#: progress-map.php:1679
msgid ""
"Upload a custom marker icon for this post. This will override the default "
"marker icon and the one selected for this post's category in \"Marker "
"categories settings\"."
msgstr ""

#: progress-map.php:1696 progress-map.php:3305
msgid "Latitude"
msgstr ""

#: progress-map.php:1707 progress-map.php:3310
msgid "Longitude"
msgstr ""

#: progress-map.php:1713 progress-map.php:3315
msgid "Get Pinpoint"
msgstr ""

#: progress-map.php:1717
msgid "Mandatory fields"
msgstr ""

#: progress-map.php:1731 progress-map.php:1748
msgid "Add more locations"
msgstr ""

#: progress-map.php:1737
msgid ""
"This field allows you to display the same post on multiple places on the "
"map. \n"
"\t\t\t\t\t\t\tFor example, let's say that this post is about \"McDonald's\" "
"and that you want to use it to show your website's visitors all the "
"locations in your country/city/town...\n"
"\t\t\t\t\t\t\twhere they can find \"McDonald's\". So, instead of creating - "
"for instance - 10 posts with the same content but with different "
"coordinates, this field will allow you to share the same content with all "
"the different locations that points to \"McDonald's\".<br />\n"
"\t\t\t\t\t\t\t<br /><strong>How to use it?</strong><br /><br />\n"
"\t\t\t\t\t\t\t1. Insert the coordinates of one location in the fields "
"<strong>Latitude</strong> & <strong>Longitude</strong>.\n"
"\t\t\t\t\t\t\t<br />\n"
"\t\t\t\t\t\t\t2. Enter the coordinates of the remaining locations in the "
"field <strong>\"Add more locations\"</strong> by dragging the marker on the "
"map to the exact location or by entering the location's address in the field "
"<strong>\"Enter an address\"</strong>, then, click on the button <strong>"
"\"Add more locations\".</strong> <br /><br /> \n"
"\t\t\t\t\t\t\t<strong>Note:</strong> All the locations will share the same "
"title, content, link and featured image. Each location represents a new item "
"on the carousel!"
msgstr ""

#: progress-map.php:3267
msgid "Enter an address and search"
msgstr ""

#: progress-map.php:3335
msgid "Add Location"
msgstr ""

#: progress-map.php:4581 progress-map.php:4846 progress-map.php:7128
msgid "Zoom in"
msgstr ""

#: progress-map.php:4584 progress-map.php:4849 progress-map.php:7131
msgid "Zoom out"
msgstr ""

#: progress-map.php:6296
msgid ""
"The map center of the map is incorrect. Please make sure that the Latitude & "
"the Longitude in \"Map Settings / Map center\" are comma separated!"
msgstr ""

#: progress-map.php:7145
msgid "Show your position"
msgstr ""

#: progress-map.php:7504 progress-map.php:7555
msgid "Toggle carousel"
msgstr ""

#: progress-map.php:7793
msgid "Filter"
msgstr ""

#: wp-settings-framework.php:244 wp-settings-framework.php:254
msgid "Posts"
msgstr ""

#: wp-settings-framework.php:244
msgid "Pages"
msgstr ""

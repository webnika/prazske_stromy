/* @Version 1.0 */

/**
 * Ajax Pagination
 *
 * @since 1.0
 */
function cspml_ajax_pagination(){
	
	jQuery('div[class^=cspml_pagination_] ul li a').livequery('click', function(e){ //check when pagination link is clicked and stop its action.         
		
		e.preventDefault(); /* Prevent going to href link */
		
		var map_id = jQuery(this).parent().parent().parent().attr('id');				
		
		/**
		 * Get the current view to start loading listings from it */
		 
		var current_view = jQuery('div[id^='+map_id+'_listing_item_]').attr('data-view');
		
		/**
		 * Get the sort value to start sorting listings from there */
		 
		var current_sort_html = jQuery('ul.cspml_sort_list li.cspml_active').html();
		var current_sort = jQuery('ul.cspml_sort_list li.cspml_active').attr('data-sort');
		var current_sort_order = jQuery('ul.cspml_sort_list li.cspml_active').attr('data-order');
		
		var link = jQuery(this).attr('href'); 
				
		jQuery('html, body').animate({
			scrollTop: jQuery("div[class^=cspml_listings_area_]").offset().top-140
		},function(){
			cspml_show_listings_animation(map_id);
		});	

		var wrapper_selector = 'div.cspml_listings_area_'+map_id;
		
		jQuery(wrapper_selector).load(link + ' ' + wrapper_selector, function(response, status, xhr){	
			
			if(status == "error"){
				
				var msg = "An error has been occurred. Please try again later: ";
				
				alert(msg + xhr.status + " " + xhr.statusText);
			
			}else if(status == "success"){
				
				/**
				 * Remove the parents of the set of matched elements from the DOM, leaving the matched elements in their place */
				 
				jQuery(wrapper_selector).children(wrapper_selector).unwrap();
								
				if(current_view == "grid")
					cspml_grid_view_classes(map_id);
							
				jQuery('div.cspml_sort_list_container span.cspml_sort_val').empty().html(current_sort_html);
				
				jQuery('li[data-sort='+current_sort+'][data-order='+current_sort_order+']').addClass('cspml_active');
	
				cspml_hide_listings_animation(map_id);
				
			}
			
			return false;
			
		});
		
	});	
		
}

function cspml_filter_listings(map_id, posts_to_retrieve, save_session, init_posts_count, reset_list){

	var shortcode_page_id = jQuery('div.cspml_listings_area_'+map_id+' input[name=shortcode_page_id]').val();
	var page_template = jQuery('div.cspml_listings_area_'+map_id+' input[name=page_template]').val();
	var template_tax_query = jQuery('div.cspml_listings_area_'+map_id+' input[name=template_tax_query]').val();
	var init_post_ids = jQuery('div.cspml_listings_area_'+map_id+' input[name=init_post_ids]').val();
	var post_type = jQuery('div.cspml_listings_area_'+map_id+' input[name=post_type]').val();
	var divider = jQuery('div.cspml_listings_area_'+map_id+' input[name=divider]').val();
	var show_listings = jQuery('div.cspml_listings_area_'+map_id+' input[name=show_listings]').val();
	var optional_latlng = jQuery('div.cspml_listings_area_'+map_id+' input[name=optional_latlng]').val();
	var current_view = jQuery('div[id^='+map_id+'_listing_item_]').attr('data-view');
	var paginate_position = jQuery('div.cspml_listings_area_'+map_id).attr('data-paginate-position');
	var paginate_align = jQuery('div.cspml_listings_area_'+map_id).attr('data-paginate-align');
	var posts_per_page = jQuery('div.cspml_listings_area_'+map_id).attr('data-posts-per-page');

	/**
	 * Get the sort value to start sorting listings from it */
	 
	var current_sort_html = jQuery('ul.cspml_sort_list li.cspml_active').html();
	var current_sort = jQuery('ul.cspml_sort_list li.cspml_active').attr('data-sort');
	var current_sort_order = jQuery('ul.cspml_sort_list li.cspml_active').attr('data-order');
		
	var request_object = {
		action: 'cspml_listings_html',
		post_type: post_type,
		init_post_ids: init_post_ids,
		post_ids: posts_to_retrieve,
		map_id: map_id,
		shortcode_page_id: shortcode_page_id,
		page_template: page_template,
		template_tax_query: template_tax_query,
		divider: divider,
		ajax_call: true,
		current_view: current_view,
		paginate_position: paginate_position,
		paginate_align: paginate_align,
		posts_per_page: posts_per_page,
		optional_latlng: optional_latlng,
	};
						  
	if(save_session) request_object.save_session = true;
	if(reset_list) request_object.reset_list = true;

	jQuery.post(
		cspml_vars.ajax_url,
		request_object,
		function(data){
			
			if(init_posts_count == ''){
				
				var nbr_of_items = (init_post_ids != '') ? posts_to_retrieve.length : '0';
				
			}else var nbr_of_items = init_posts_count;
			
			if(show_listings == 'true'){
								
				jQuery('div.cspml_listings_area_'+map_id).html(data);
				jQuery('span.cspml_the_count_'+map_id).empty().html(nbr_of_items);					
				jQuery('div.cspml_sort_list_container span.cspml_sort_val').empty().html(current_sort_html);
				jQuery('li[data-sort='+current_sort+'][data-order='+current_sort_order+']').addClass('cspml_active');			
				if(current_view == "grid") cspml_grid_view_classes(map_id);
				cspml_hide_listings_animation(map_id);	
			
			}
			
		}
	);	
				
}

function cspml_show_listings_animation(map_id){
	
	jQuery("div.cspml_loading_container").fadeIn();
	
	jQuery('div.cspml_listings_area_'+map_id+' div.cspml_item_holder').removeClass('cspm_animated fadeIn').animate({'opacity':'0.5'});
	
	jQuery('div.cspml_transparent_layer').show();
	
}

function cspml_hide_listings_animation(map_id){
	
	jQuery("div.cspml_listings_area_"+map_id+" div.cspml_item_holder").addClass('cspm_animated fadeIn');
	
	jQuery("div.cspml_loading_container").fadeOut();
	
	jQuery('div.cspml_transparent_layer').hide();
	
	/**
	 * Adjust the height of the listings content to the image height (list view only) */
	
	setTimeout(function(){
		cspml_adjust_list_content_height();
	}, 100);
	
}	

/**
 * Prepare all the CSS classes the list view will need ...
 * ... and replace grid view classes with list view classes
 *
 * @since 1.0
 */
function cspml_list_view_classes(map_id){
			
	jQuery('div[class^=cspml_options_bar] div.list_view a').removeClass('enabled');
	
	jQuery('div[class^=cspml_options_bar] div.grid_view a').addClass('enabled');
			
	if(cspml_vars.grid_cols == 'cols1'){
		
		var grid_classes = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
		
	}else if(cspml_vars.grid_cols == 'cols2'){
		
		var grid_classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
		
	}else if(cspml_vars.grid_cols == 'cols4'){
		
		var grid_classes = 'col-lg-3 col-md-3 col-sm-6 col-xs-12';
	
	}else if(cspml_vars.grid_cols == 'cols6'){
		
		var grid_classes = 'col-lg-2 col-md-2 col-sm-6 col-xs-12';
	
	}else var grid_classes = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';
			
	jQuery('div[id^='+map_id+'_listing_item_]').addClass('cspm_animated fadeIn');
	
	jQuery('div[id^='+map_id+'_listing_item_]').attr('data-view', 'list');
	
	jQuery('div[id^='+map_id+'_listing_item_]').removeClass('grid '+grid_classes).addClass('row');
	
	jQuery('div.list_view_holder').addClass('col-lg-12 col-xs-12 col-sm-12 col-md-12');
	
	jQuery('div[id^='+map_id+'_listing_item_] .cspml_thumb_container').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').addClass('col-lg-2 col-md-2 col-sm-2 col-xs-12');
	
	jQuery('div[id^='+map_id+'_listing_item_] div.cspml_details_container').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-12').attr('data-view', 'list');
	
	jQuery('div[id^='+map_id+'_listing_item_] div.cspml_details_content').removeClass('grid');
	
	setTimeout(function(){
		
		jQuery('div[id^='+map_id+'_listing_item_]').removeClass('cspm_animated fadeIn');
	
	}, 2000);
	
}

/**
 * Prepare all the CSS classes the grid view will need ...
 * ... and replace list view classes with grid view classes
 *
 * @since 1.0
 */
function cspml_grid_view_classes(map_id){
			
	jQuery('div[class^=cspml_options_bar] div.grid_view a').removeClass('enabled');
	
	jQuery('div[class^=cspml_options_bar] div.list_view a').addClass('enabled');
			
	if(cspml_vars.grid_cols == 'cols1'){
		
		var grid_classes = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
		
	}else if(cspml_vars.grid_cols == 'cols2'){
		
		var grid_classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
		
	}else if(cspml_vars.grid_cols == 'cols4'){
		
		var grid_classes = 'col-lg-3 col-md-3 col-sm-6 col-xs-12';
	
	}else if(cspml_vars.grid_cols == 'cols6'){
		
		var grid_classes = 'col-lg-2 col-md-2 col-sm-6 col-xs-12';
	
	}else var grid_classes = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';
			
	jQuery('div[id^='+map_id+'_listing_item_]').addClass('cspm_animated fadeIn');
	
	jQuery('div[id^='+map_id+'_listing_item_]').attr('data-view', 'grid');		
	
	jQuery('div[id^='+map_id+'_listing_item_]').removeClass('row').addClass('grid '+grid_classes);
	
	jQuery('div.list_view_holder').removeClass('col-lg-12 col-xs-12 col-sm-12 col-md-12');
	
	jQuery('div[id^='+map_id+'_listing_item_] .cspml_thumb_container').removeClass('col-lg-2 col-md-2 col-sm-2 col-xs-12').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12');
	
	jQuery('div[id^='+map_id+'_listing_item_] div.cspml_details_container').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-12').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').attr('data-view', 'grid');
	
	jQuery('div[id^='+map_id+'_listing_item_] div.cspml_details_content').addClass('grid');
	
	setTimeout(function(){
		
		jQuery('div[id^='+map_id+'_listing_item_]').removeClass('cspm_animated fadeIn');
	
	}, 2000);
		
}

		
/**
 * Make sure to adjust the hight of the listings content to the image height (list view only) 
 *
 * @since 1.0
 */		
function cspml_adjust_list_content_height(){
	 
	var thumb_height = jQuery('div.cspml_thumb_container').innerHeight();
	var title_container_height = jQuery('div.cspml_details_title').innerHeight();
	var content_height = thumb_height - title_container_height - 40;

	jQuery('div.cspml_details_container[data-view=list]').attr('style', 'height:'+thumb_height+'px;');	
	jQuery('div.cspml_details_container[data-view=list] .cspml_details_content').attr('style', 'height:'+content_height+'px;');
	
}


/**
 * This will add hover/active style to an item in the list
 *
 * @since 1.0
 */
function cspml_animate_list_item(map_id, post_id){

	var list_item_target = jQuery('div.cspml_item_holder[data-map-id='+map_id+'][data-post-id='+post_id+']');
	
	jQuery('div.cspml_item_holder .cspml_item').removeClass('cspml_active_item');

	if(typeof jQuery('.cspml_list_and_filter_container').attr('style') != 'undefined'){

		setTimeout(function(){
			if(list_item_target.length){
				jQuery('body').scrollTo((jQuery('#cspml_listings_container[data-map-id='+map_id+']').offset().top - 50), 1000, function(){
					jQuery('.cspml_list_and_filter_container').scrollTo(list_item_target, 1000, function(){
						setTimeout(function(){
							jQuery('div.cspml_item_holder[data-map-id='+map_id+'][data-post-id='+post_id+'] .cspml_item').addClass('cspml_active_item');
						}, 100);
					});
				});
			}
		}, 1000);
		
	}else{

		setTimeout(function(){
			if(list_item_target.length){
				jQuery('body').scrollTo((list_item_target.offset().top - 50), 1000, function(){
					setTimeout(function(){
						jQuery('div.cspml_item_holder[data-map-id='+map_id+'][data-post-id='+post_id+'] .cspml_item').addClass('cspml_active_item');
					}, 100);
				});
			}
		}, 1000);
		
	}
	
}


/**
 * Reset filter form input
 *
 * @since 1.0
 */
function cspml_reset_filter_fields($select){
		 
	jQuery('form.cspml_filter_form input[type=radio]:checked, form.cspml_filter_form input[type=checkbox]:checked').each(function() {
		jQuery(this).prop("checked", false).trigger("change");
		jQuery(this).trigger("stateChanged");	
	});
	
	jQuery('form.cspml_filter_form input[type=text]').each(function() {
		jQuery(this).val('');
	});
	
	jQuery("input.cspml_fs_slider_range").ionRangeSlider('update');
	
	for(var i=0; i<$select.length; i++){
		var current = $select[i].selectize;
		current.clear();
	}
			
}

jQuery(document).ready(function($) {		
	
	
	/**
	 * Customize Checkboxes and Radios button */
	 
	$("div.cspml_fs_item_container input[type='radio'], div.cspml_fs_item_container input[type='checkbox']").ionCheckRadio();
	
	
	/**
	 * Customize the select box */
	
	var $select = $('select.cspml_fs_selectize').selectize();
	
	
	/**
	 * Customize the slider text box */
	 
	var faceted_search_slider = $("input.cspml_fs_slider_range").ionRangeSlider({
		grid: false,
		prettify_enabled: true,
		keyboard: true,
		decorate_both: false,
		force_edges: true,
	});
	
	
	/**
	 * Customize the text box to the number field */
	 
	$("input[type=text].cspml_fs_number_field").spinner();
	
		
	/**
	 * Call pagination function */
	 
	cspml_ajax_pagination();


	/**
	 * Adjust the height of the listings content to the image height (list view only) */
	
	cspml_adjust_list_content_height();
	
	
	/**
	 * Toggle the options list in the listings faceted search */

	$('div.cspml_fs_label span.cspml_toggle_btn').livequery('click', function(){
		var field_name = $(this).attr('data-field-name');
		var display_location = $(this).attr('data-display-location');
		var degrees = 0;


		$('div.cspml_fs_options_list[data-field-name='+field_name+'][data-display-location='+display_location+']').slideToggle('fast', function(){
			
			/*if($('div.cspml_fs_options_list[data-field-name='+field_name+'][data-display-location='+display_location+']').is(':visible'))
				degrees = 180;
			$('span.cspml_toggle_btn[data-field-name='+field_name+'] img').css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
					 '-moz-transform' : 'rotate('+ degrees +'deg)',
					 '-ms-transform' : 'rotate('+ degrees +'deg)',
					 'transform' : 'rotate('+ degrees +'deg)',
					 '-webkit-transition': 'all .25s ease-out',
					 '-moz-transition': 'all .25s ease-out',
					 '-ms-transition': 'all .25s ease-out',
					 'transition': 'all .25s ease-out'});*/
					 
		});
	});
	
	/**
	 * Set the value of field the "spinner" to ANY */
	 
	$('div.cspml_reset_spinner a').livequery('click', function(){
		var field_name = $(this).attr('data-field-name');
		var display_location = $(this).attr('data-display-location');
		$('div.cspml_fs_options_list[data-display-location='+display_location+'] div.cspml_input_container input[type=text][name='+field_name+']').attr('value', '');	
	});
		
	/**
	 * Open the "Sort by" options list */
	 
	$('div.cspml_sort_list_container').livequery('click', function(){
		$('ul.cspml_sort_list').addClass('cspm_animated fadeIn').slideToggle(function(){
			$('ul.cspml_sort_list').mCustomScrollbar("destroy");
			$('ul.cspml_sort_list').mCustomScrollbar({
				autoHideScrollbar:true,
				mouseWheel:{
					enable: true,
					preventDefault: true,
				},
				theme:"dark-thin"
			});
		});
	});
	
	/**
	 * By default add the class "active" to the first element in the list on page load */
	 
	$('ul.cspml_sort_list li').first().addClass('cspml_active');

	/**
	 * Sort the listings */
	 
	$('ul.cspml_sort_list li').livequery('click', function(){
		
		/**
		 * Update the default list value */
		 
		$('ul.cspml_sort_list li').removeClass('cspml_active');
		$(this).addClass('cspml_active');
		$('ul.cspml_sort_list').fadeOut('fast');
		
		var current_sort_html = $(this).html();
		$('div.cspml_sort_list_container span.cspml_sort_val').empty().html(current_sort_html);
		
		var data_sort = $(this).attr('data-sort');
		var data_order = $(this).attr('data-order');		
		var map_id = $(this).attr('data-map-id');
		
		cspml_show_listings_animation(map_id);
		
		var post_type = jQuery('div.cspml_listings_area_'+map_id+' input[name=post_type]').val();
		var init_post_ids = $('div.cspml_listings_area_'+map_id+' input[name=init_post_ids]').val();
		var post_ids = $('div.cspml_listings_area_'+map_id+' input[name=post_ids]').val();
		var shortcode_page_id = $('div.cspml_listings_area_'+map_id+' input[name=shortcode_page_id]').val();
		var divider = $('div.cspml_listings_area_'+map_id+' input[name=divider]').val();
		var current_view = $('div[id^='+map_id+'_listing_item_]').attr('data-view');
		var page_template = $('div.cspml_listings_area_'+map_id+' input[name=page_template]').val();
		var template_tax_query = $('div.cspml_listings_area_'+map_id+' input[name=template_tax_query]').val();
		var optional_latlng = jQuery('div.cspml_listings_area_'+map_id+' input[name=optional_latlng]').val();
		var paginate_position = $('div.cspml_listings_area_'+map_id).attr('data-paginate-position');
		var paginate_align = $('div.cspml_listings_area_'+map_id).attr('data-paginate-align');
		var posts_per_page = jQuery('div.cspml_listings_area_'+map_id).attr('data-posts-per-page');

		/**
		 * Send the ajax request */
		 
		jQuery.post(
			cspml_vars.ajax_url,
			{
				action: 'cspml_listings_html',
				map_id: map_id,
				data_sort: data_sort,
				data_order: data_order,
				shortcode_page_id: shortcode_page_id,
				post_type: post_type,
				post_ids: post_ids,
				init_post_ids: init_post_ids,
				divider: divider,
				sort_call: true,
				current_view: current_view,
				page_template: page_template,
				template_tax_query: template_tax_query,
				paginate_position: paginate_position,
				paginate_align: paginate_align,
				posts_per_page: posts_per_page,
				optional_latlng: optional_latlng,
			},
			function(data){
				
				$('div.cspml_listings_area_'+map_id).html(data);
				
				if(current_view == "grid")
					cspml_grid_view_classes(map_id);
					
				$('div.cspml_sort_list_container span.cspml_sort_val').empty().html(current_sort_html);
				
				$('li[data-sort='+data_sort+'][data-order='+data_order+']').addClass('cspml_active');
				
				cspml_hide_listings_animation(map_id);
				
			}
		);	
		
	});	
	
	
	/**
	 * Switch the listing's view */
	 
	if(cspml_vars.show_view_options == "yes"){

		$('div.view_option a').livequery('click', function(){
			
			var map_id = $(this).attr('data-map-id');
			var current_view = $(this).attr('data-current-view');
			var next_view = $(this).attr('data-next-view');
			
			/**
			 * Change current view to next view and vise versa */
			 		
			$(this).attr('data-current-view', next_view);
			$(this).attr('data-next-view', current_view);
			
			/**
			 * Change link title */
			 
			var title = $('div.view_option[data-map-id='+map_id+'] a').attr('title');
			$('div.view_option[data-map-id='+map_id+'] a').attr('title', title.replace(next_view, current_view));			
			
			/**
			 * Change view icon */
			 
			var img_src = $('div.view_option[data-map-id='+map_id+'] a img').attr('src');
			$('div.view_option[data-map-id='+map_id+'] a img').attr('src', img_src.replace(next_view+'.png', current_view+'.png'));			
			
			/**
			 * Set the current view */
			 
			if(next_view == 'grid'){
				
				cspml_grid_view_classes(map_id);
				
			}else if(next_view == 'list'){
				
				cspml_list_view_classes(map_id);

				/**
				 * Adjust the height of the listings content to the image height (list view only) */
				
				cspml_adjust_list_content_height();
	
			}
			
		});
		
	}	
	
	
	/**
	 * Init Draggable option for the container of Map filter form 
	 
	$("div.cspml_map_filter_form_container").draggable({ 
		handle:"div.cspml_drag_handle",
		cursor: "move",
		containment: "div[id^=codespacing_progress_map_div_]",
		scroll: false
	});*/
	
	
	/**
	 * Filter the listings from the "Listings filter form" OR the "Map filter form" */
	 
	$('a.cspml_submit_listings_filter, a.cspml_mfs_submit_listings_filter').livequery('click', function(){
		
		var map_id = $(this).attr('data-map-id');
		var display_location = $(this).attr('data-display-location'); // Listings OR Map
		var request_type = $(this).attr('data-request-type');
		//var required_custom_fields = $('div.cspml_listings_area_'+map_id+' input[name=required_custom_fields]').val();
		var post_type = jQuery('div.cspml_listings_area_'+map_id+' input[name=post_type]').val();
		var init_post_ids = $('div.cspml_listings_area_'+map_id+' input[name=init_post_ids]').val();		
		var current_view = $('div[id^='+map_id+'_listing_item_]').attr('data-view');
		var shortcode_page_id = $('div.cspml_listings_area_'+map_id+' input[name=shortcode_page_id]').val();
		var page_template = $('div.cspml_listings_area_'+map_id+' input[name=page_template]').val();
		var template_tax_query = $('div.cspml_listings_area_'+map_id+' input[name=template_tax_query]').val();
		var show_listings = $('div.cspml_listings_area_'+map_id+' input[name=show_listings]').val();

		if(display_location != "map"){
			$('html, body').animate({
				scrollTop: $("div[class^=cspml_listings_area_]").offset().top-140
			},function(){
				cspml_show_listings_animation(map_id);
			});	
		}else{
			
			if(typeof NProgress !== 'undefined'){
				NProgress.configure({
				  parent: 'div#codespacing_progress_map_div_'+map_id,
				  showSpinner: true
				});				
				NProgress.start();
			}
			
			if(show_listings == 'true')
				cspml_show_listings_animation(map_id);
		}
			
		var filter_form_fields_val = {};
		var multi_input_values = [];
		var filter_field_data = [];
		
		var posts_to_retrieve = {};
		posts_to_retrieve[map_id] = [];

		$('form#cspml_'+display_location+'_filter_form input[type=text], form#cspml_'+display_location+'_filter_form input[type=radio]:checked, form#cspml_'+display_location+'_filter_form input[type=checkbox]:checked, form#cspml_'+display_location+'_filter_form select').each(function() {
			
			var this_field_name = (typeof $(this).attr('name') != 'undefined') ? $(this).attr('name').replace('[]', '') : false;
		
			if(this_field_name){

				var filter_type = $('input[name='+this_field_name+'_filter_type]').val(); //$(this).attr('data-filter-type');
				filter_field_data = [];
				filter_field_data.push(filter_type);
				
				if($(this).is("input[type=checkbox]")){
					
					var name_array = $(this).attr('name').split('[]');
					var name = name_array[0];
							
					/**
					 * Clear the array */
					 
					if(!(name in filter_form_fields_val))
						multi_input_values = [];
						
					if(name_array.length == 2){
																			
						if($(this).attr('value').indexOf(',') > -1 && $(this).hasClass('cspml_show_all')){
							var myArray = $(this).attr('value').split(',');
							for(var i = 0; i < myArray.length; i = i + 1){
								multi_input_values.push(myArray[i]);
							}	
						}else{
							multi_input_values.push($(this).val());
						}
						
						filter_form_fields_val[name] = jQuery.merge(filter_field_data, multi_input_values);
					
					}else{
						
						filter_field_data.push($(this).val());
						filter_form_fields_val[$(this).attr('name')] = filter_field_data;
						
					}
					
				}else{
					
					var name = $(this).attr('name');
					if($(this).attr('value').indexOf(',') > -1 && $(this).hasClass('cspml_show_all')){
						var myArray = $(this).attr('value').split(',');
						for(var i = 0; i < myArray.length; i = i + 1){
							multi_input_values.push(myArray[i]);
						}	
						filter_form_fields_val[name] = jQuery.merge(filter_field_data, multi_input_values);
					}else{
						if($(this).val() != ''){
							filter_field_data.push($(this).val());
							filter_form_fields_val[name] = filter_field_data;
						}
					}
					
				}
			
			}
			
		});

		/**
		 * Send the ajax request */
		 
		jQuery.post(
			cspml_vars.ajax_url,
			{
				action: 'cspml_faceted_search_query',
				map_id: map_id,
				post_type: post_type,
				init_post_ids: init_post_ids,
				filter_args: filter_form_fields_val,
				//required_custom_fields: required_custom_fields,				
				filter_form_call: true,
				current_view: current_view,
				shortcode_page_id: shortcode_page_id,
				page_template: page_template,
				template_tax_query: template_tax_query,
			},
			function(data){
				
				posts_to_retrieve[map_id] = JSON.parse(data);

				var plugin_map = $('div#codespacing_progress_map_div_'+map_id);
				
				if(plugin_map.length != 0){
					
					/**
					 * Hide all markers */
					 
					cspm_hide_all_markers(plugin_map).done(function(){
	
						/**
						 * Show markers within the selected filter query */
						 
						cspm_set_markers_visibility(plugin_map, map_id, null, 0, post_ids_and_categories[map_id], posts_to_retrieve[map_id], true);
						cspm_simple_clustering(plugin_map, map_id);
						
						plugin_map.gmap3({
							get: {
								name: 'marker',
								all:  true,										
							},
							autofit:{}
						});
						
						if(progress_map_vars.show_posts_count == "yes")
							$('span.the_count_'+map_id).empty().html(posts_to_retrieve[map_id].length);
	
						if(typeof NProgress !== 'undefined')
							NProgress.done();
						
					});
					
				}
					
				/**
				 * Filter HTML listings */
				 
				if(show_listings == 'true')
					cspml_filter_listings(map_id, posts_to_retrieve[map_id], false, '', false);					
								
			}
			
		);
		
	});


	/**
	 * Reset the listings from the "Listings filter form" OR the "Map filter form" */
	 
	$('a.cspml_reset_lsitings_filter, a.cspml_mfs_reset_lsitings_filter').livequery('click', function(){

		var map_id = $(this).attr('data-map-id');
		var plugin_map = $('div#codespacing_progress_map_div_'+map_id);
		var display_location = $(this).attr('data-display-location');
		//var required_custom_fields = $('div.cspml_listings_area_'+map_id+' input[name=required_custom_fields]').val();
		var post_type = jQuery('div.cspml_listings_area_'+map_id+' input[name=post_type]').val();
		var init_post_ids = $('div.cspml_listings_area_'+map_id+' input[name=init_post_ids]').val();
		var count_init_post_ids = $('div.cspml_listings_area_'+map_id+' input[name=count_init_post_ids]').val();
		var shortcode_page_id = $('div.cspml_listings_area_'+map_id+' input[name=shortcode_page_id]').val();
		var page_template = $('div.cspml_listings_area_'+map_id+' input[name=page_template]').val();
		var template_tax_query = $('div.cspml_listings_area_'+map_id+' input[name=template_tax_query]').val();
		var show_listings = $('div.cspml_listings_area_'+map_id+' input[name=show_listings]').val();
		
		if(display_location != "map"){
			
			$('html, body').animate({
				scrollTop: $("div[class^=cspml_listings_area_]").offset().top-140
			},function(){
				cspml_show_listings_animation(map_id);
			});	
		
		}else cspml_show_listings_animation(map_id);
		
		var posts_to_retrieve = {};
		posts_to_retrieve[map_id] = [];				
		
		if(plugin_map.length != 0){
		
			cspm_set_markers_visibility(plugin_map, map_id, null, 0, post_ids_and_categories[map_id], posts_to_retrieve[map_id], false);
			cspm_simple_clustering(plugin_map, map_id);
						
			plugin_map.gmap3({
				get: {
					name: 'marker',
					all:  true,										
				},
				autofit:{}
			});
						
			if(progress_map_vars.show_posts_count == "yes")
				$('span.the_count_'+map_id).empty().html(posts_to_retrieve[map_id].length);
		
		}
		
		var filter_form_fields_val = [];//posts_to_retrieve[map_id];

		/**
		 * Send the ajax request */

		jQuery.post(
			cspml_vars.ajax_url,
			{
				action: 'cspml_faceted_search_query',
				map_id: map_id,
				post_type: post_type,
				init_post_ids: init_post_ids,
				post_ids: posts_to_retrieve[map_id],				
				filter_args: filter_form_fields_val,
				//required_custom_fields: required_custom_fields,				
				filter_form_call: true,	
				shortcode_page_id: shortcode_page_id,
				page_template: page_template,	
				template_tax_query: template_tax_query,
			},
			function(data){
			
				/**
				 * Reset filter form input */
				 
				cspml_reset_filter_fields($select);
			
				/**
				 * Filter HTML listings */

				if(show_listings == 'true') 
					cspml_filter_listings(map_id, posts_to_retrieve[map_id], true, count_init_post_ids, true);	
						
			}
		);
		
		/**
		 * Reset "Progress Map" search form */
		 
		if($('form#search_form_'+map_id).is(':visible')){		
			$('form#search_form_'+map_id+' input#cspm_address_'+map_id).attr('value', '').focus();
			$('div.cspm_reset_search_form_'+map_id).removeClass('fadeIn').hide('fast');
		}
		
	});	
	
	
	/**
	 * Hide the "Map filter form" */
	 
	$('div.cspml_hide_handle').livequery('click', function(){
		
		$('div.cspml_map_filter_form_container div.cspml_map_filter_form_body').addClass('cspm_animated fadeOutUp');
		
		$('div.cspml_map_filter_form_container div.cspml_map_filter_form_body').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			
			$('div.cspml_map_filter_form_container div.cspml_map_filter_form_body').removeClass('cspm_animated fadeOutUp').hide();
			$('div.cspml_map_filter_form_controls_container').hide();
			$('div.cspml_show_map_filter_form_controls_container').show();				
			
			var element = $("div[id^=codespacing_progress_map_div_]");
			var width = element.width();
			var height = element.height();
			var position = element.position();
			
			var bottomLeftX = position.left;
			var bottomLeftY = position.top + height;
			
			var bottomRightX = position.left + width;
			var bottomRightY = position.top + height;

			$('div.cspml_map_filter_form_container').animate({top:(bottomLeftY-60)+'px'});
					
		});
				
	});
	
	
	/**
	 * Remove the attribute "style" from the map filter container.
	 * The style attribute contains the left & top position of the map filter.
	 * The style attribute was added by the jQuery Drag */
	 
	var windowWidth = $(window).width();
	
	$(window).resize(function(){
		
		/**
		 * Check if window width has actually changed and it's not just iOS triggering a resize event on scroll */
		 
		if ($(window).width() != windowWidth) {

			/**
			 * Update the window width for next time */
			 
			windowWidth = $(window).width();
			windowHeight = $(window).height();
			
			$("div.cspml_map_filter_form_container").removeAttr('style');
			
			var element = $("div[id^=codespacing_progress_map_div_]");
			var height = element.height();
			
			$("div.cspml_map_filter_form_container").attr('style', 'height:100px;');
		
		}
	
		/**
		 * Adjust the height of the listings content to the image height (list view only) */
		
		cspml_adjust_list_content_height();
		
	});
	
	
	/**
	 * Show the "Map filter form" */
	 
	$('div.cspml_show_handle').livequery('click', function(){
			
		var element = $("div[id^=codespacing_progress_map_div_]");
		var width = element.width();

		if(width < 768)
			$('div.cspml_map_filter_form_container').animate({top:'10px', right:'10px'});	
		else $('div.cspml_map_filter_form_container').animate({top:'30px', right:'50px'});	
		
		$('div.cspml_show_map_filter_form_controls_container').hide(function(){
			$('div.cspml_map_filter_form_controls_container').show();
			$('div.cspml_map_filter_form_container div.cspml_map_filter_form_body').show();
		});
				
	});		
	
		
	/**
	 * The event handler of the listings items.
	 * Show the location of the item in the map */
	
	$('.cspml_fire_pinpoint').livequery('click', function(){

		var map_id = $(this).attr('data-map-id'); 
		var post_id = $(this).attr('data-post-id'); 
		var plugin_map = $('div#codespacing_progress_map_div_'+map_id);

		if(plugin_map.length > 0){
			
			var map = plugin_map.gmap3("get");
			var marker_position = $(this).attr('data-coords').split('_');
	
			$('html, body').animate({
				scrollTop: $("div[id^=codespacing_progress_map_div_]").offset().top-40
			}, 500, function(){
				cspm_center_map_at_point(plugin_map, marker_position[0], marker_position[1], 'zoom');
				setTimeout(function(){cspm_animate_marker(plugin_map, map_id, post_id);},200);
				if(progress_map_vars.infowindow_type != 'content_style'){		
					// Add overlay active style (used only for bubble infowindow style) 
					$('div.marker_holder div.pin_overlay_content').removeClass('pin_overlay_content-active');
					$('div#bubble_'+post_id+'_'+map_id+' div.pin_overlay_content').addClass('pin_overlay_content-active');	
				}
			});	
			
		}
		
	}).css('cursor','pointer');
	
	/**
	 * @Event handler */
	
	
	/**
	 * This code will filter the listings when there's a faceted search request from "Progress Map".
	 * It'll get the result of the post_ids to display from the global var 'cspm_global_object'. */
	
	$('form.faceted_search_form input').livequery('ifChanged', function(){
						
		/*var map_id = $(this).attr('class').split(' ')[1];
		var carousel = $(this).attr('class').split(' ')[2];*/
		var map_id = $(this).attr('data-map-id');
		var carousel = $(this).attr('data-show-carousel');		
		var ext_name = $('form#faceted_search_form_'+map_id).attr('data-ext'); // This data detects if the Progress Map is used with another extension/addon

		if(carousel == 'no' && ext_name == 'cspml_list'){
			if(typeof cspml_show_listings_animation == 'function')
				cspml_show_listings_animation(map_id);
		}

		setTimeout(function(){
			if(carousel == 'no' && ext_name == 'cspml_list'){
				if(typeof cspml_filter_listings == 'function' && typeof window.cspm_global_object.posts_to_retrieve[map_id] !== 'undefined')
					cspml_filter_listings(map_id, window.cspm_global_object.posts_to_retrieve[map_id], true, '', false);
			}else{
				if(typeof cspml_hide_listings_animation == 'function')
					cspml_hide_listings_animation(map_id);	
			}
		}, 1000);
					
	});
	
	
	/**
	 * This code will filter the listings when there's a search request from "Progress Map"
	 * It'll get the result of the post_ids to display from the global var 'cspm_global_object' */
	 
	$('div[class^=cspm_reset_search_form], div[class^=cspm_submit_search_form_]').livequery('click', function(){

		var map_id = $(this).attr('data-map-id');
		var carousel = $(this).attr('data-show-carousel');
		var ext_name = $(this).attr('data-ext'); // This data detects if the Progress Map is used with another extension/addon
	
		if(carousel == 'no' && ext_name == 'cspml_list'){
			
			if(typeof cspml_show_listings_animation == 'function')
				cspml_show_listings_animation(map_id);
			
			/**
			 * Reset filter form input */
				 
			if(typeof cspml_reset_filter_fields == 'function')
				cspml_reset_filter_fields($select);
			
				
		}
		
		var reset_list = ($(this).attr('class').split(' ')[0] == 'cspm_reset_search_form_'+map_id) ? true : false ;
		
		setTimeout(function(){
			if(carousel == 'no' && ext_name == 'cspml_list'){
				if(typeof cspml_filter_listings == 'function' && typeof window.cspm_global_object.posts_to_retrieve[map_id] !== 'undefined')
					cspml_filter_listings(map_id, window.cspm_global_object.posts_to_retrieve[map_id], true, '', reset_list);
			}else{
				if(typeof cspml_hide_listings_animation == 'function')
					cspml_hide_listings_animation(map_id);	
			}
		}, 1000);
					
	});
	
	/**
	 * Show & Hide the listing's filter form */
	
	$('span.cspml_close_fs').livequery('click', function(){
		
		var map_id = $(this).attr('data-map-id');
		
		if($('.cspml_fs_container[data-map-id='+ map_id +']').is(':visible')){
		
			$('.cspml_listing_items_container_'+map_id).removeClass('col-lg-9 col-md-9').addClass('col-lg-12 col-md-12');
		
		}else $('.cspml_listing_items_container_'+map_id).removeClass('col-lg-12 col-md-12').addClass('col-lg-9 col-md-9');
		
		$('.cspml_fs_container[data-map-id='+ map_id +']').toggle();
	
		/**
		 * Adjust the height of the listings content to the image height (list view only) */
		
		cspml_adjust_list_content_height();
		
	});
	
	
	/**
	 * Detect when a user opens "Progress Map" search form and/or faceted search and resize the map accordingly */
	 
	$('div.search_form_btn, div.faceted_search_btn').livequery('click', function(){
		
		var map_id = $(this).attr('id');
			
		var map_height = $('div.cspml_resize_map[data-map-id='+map_id+']').attr('data-map-height');
		
		$map_div = $('div#codespacing_progress_map_div_'+map_id);
		 
		$resize_img_selector = $('div.cspml_resize_map img');
		
		var resize_img_src = $resize_img_selector.attr('src');
		
		if(typeof map_height !== 'undefined' && typeof resize_img_src !== 'undefined'){
			
			if($map_div.innerHeight() != map_height.replace('px', '')){
				
				$map_div.animate({height: map_height}, 600, function(){
				
					$resize_img_selector.attr('src', resize_img_src.replace('expand.png', 'collapse.png'));	
				
				});
				
			}
			
		}
		
	});
	
	/**
	 * Resize the Map vertically */
	
	$('div.cspml_resize_map').livequery('click', function(){
		
		var map_id = $(this).attr('data-map-id');
		
		var map_height = $(this).attr('data-map-height');
		 
		$resize_img_selector = $('div.cspml_resize_map img');
		
		var resize_img_src = $resize_img_selector.attr('src');
		
		$map_div = $('div#codespacing_progress_map_div_'+map_id);
		
		if(typeof map_height !== 'undefined' && typeof resize_img_src !== 'undefined'){
			
			if($map_div.innerHeight() != map_height.replace('px', '')){
				
				$map_div.animate({height: map_height}, 600, function(){
				
					$resize_img_selector.attr('src', resize_img_src.replace('expand.png', 'collapse.png'));	
				
				});
				
			}else{
				
				$map_div.animate({height: "100px"}, 600, function(){
							
					$resize_img_selector.attr('src', resize_img_src.replace('collapse.png', 'expand.png'));	
					
					/**
					 * Hide "Progress Map" search form */
					 
					if($('div.search_form_container_'+map_id).is(':visible')){
						$('div.search_form_container_'+map_id).removeClass('fadeInUp').addClass('cspm_animated slideOutLeft');
						setTimeout(function(){
							$('div.search_form_container_'+map_id).css({'display':'none'});							
						},200);
					}		
					
					/**
					 * Hide "Progress Map" Faceted search form */
					 
					if($('div.faceted_search_container_'+map_id).is(':visible')){	
						$('div.faceted_search_container_'+map_id).removeClass('slideInLeft').addClass('cspm_animated slideOutLeft');
						setTimeout(function(){$('div.faceted_search_container_'+map_id).css({'display':'none'});},200);
					}
				
				});
				
			}
			
		}
		
	});

});

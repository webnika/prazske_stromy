<?php

/**
 * Plugin Name: Progress Map, List & Filter by CodeSpacing
 * Plugin URI: http://codecanyon.net/user/codespacing/portfolio
 * Description: <strong>Progress Map List & Filter</strong> is an extension of <strong>Progress Map Wordpress plugin</strong>. This extension allows to switch "Progress Map" carousel to list & to filter the list items using an advanced search filter tool.
 * Version: 1.0
 * Author: Hicham Radi (CodeSpacing)
 * Author URI: http://www.codespacing.com/
 * Text Domain: cspml
 * Domain Path: /languages
 */

if( !class_exists( 'ProgressMapList' ) )
{
	
	class ProgressMapList{
		
		private static $_this;	
		private $plugin_path;		
		private $plugin_url;
		private $plugin_version = '1.0';
		
		private $plugin_get_var = 'cspml';
		private $settings;
			
		public $cspml_plugin_path;
		public $cspml_plugin_url;

		public $post_type = '';
		
		/**
		 * Layout settings */
		
		public $list_layout = 'vertical'; // Possible values are, "vertical", "horizontal-left", "horizontal-right"
		public $map_height = '400';
		public $list_height = '';
		
		/**
		 * Options Bar Settings */
		 
		public $cspml_show_options_bar = 'yes';
		
		/**
		 * View Options Settings */
		 
		public $cspml_show_view_options = 'yes';
		public $cspml_default_view_option = 'list';
		public $cspml_grid_cols = 'cols3';
		
		/**
		 * Listings Count Settings */
		 
		public $cspml_show_posts_count = 'yes';
		public $cspml_posts_count_clause = '[posts_count] Result(s)';
		
		/**
		 * Content Settings */
		 
		public $cspml_listings_title = '';
		public $cspml_listings_details = '[l=700]';
		
		/**
		 * "Marker Position" Button Settings */
		 
		public $cspml_show_fire_pinpoint_btn = 'yes';
		
		/**
		 * Sort Listings Settings */
		 
		public $cspml_show_sort_option = 'yes';
		public $cspml_sort_options = array(
			'default|Default|init',
			'data-date-created|Date (Newest first)|desc',
			'data-date-created|Date (Oldest first)|asc',
			'data-title|A -> Z|asc',
			'data-title|Z -> A|desc'
		);
		public $cspml_custom_sort_options = '';
		
		/**
		 * Pagination Settings */
		 
		public $cspml_posts_per_page = '';
		public $cspml_pagination_position = 'bottom'; // Possible values are, "top", "bottom", "both"
		public $cspml_pagination_align = 'center'; // Possible values are, "left", "right", "center"
		public $cspml_prev_page_text = '&laquo;';
		public $cspml_next_page_text = '&raquo;';
		public $cspml_show_all = 'false';
		
		/**
		 * Listing's Filter Form Settings */
		 
		public $cspml_faceted_search_option = 'yes';
		public $cspml_faceted_search_position = 'left';
		
		public $cspml_taxonomies = array();
		public $cspml_taxonomy_relation_param = 'AND';
		
		public $cspml_custom_fields = array();
		public $cspml_custom_field_relation_param = 'AND';
		
		public $cspml_filter_btn_text = 'Filter';

		/**
		 * Map Filter Form Settings */
		 
		public $cspml_mfs_faceted_search_option = 'no';

		/**
		 * Custom settings */
		
		public $cspml_exclude_from_field_options = array(); 
	
		function __construct(){	
	
			self::$_this = $this;       
			
			$this->plugin_path = plugin_dir_path( __FILE__ );
			$this->plugin_url = plugin_dir_url( __FILE__ );
			
			$this->csnm_plugin_path = $this->plugin_path;
			$this->csnm_plugin_url = $this->plugin_url;
			
			/**
			 * Include and create a new WordPressSettingsFramework */
			 
			require_once( $this->plugin_path .'wp-settings-framework.php' );
			$this->cspml_wpsf = new CsPml_WordPressSettingsFramework( $this->plugin_path .'settings/cspml.php' );
					
			/**
			 * Call plugin settings */
			 
			$this->settings = cspml_wpsf_get_settings( $this->plugin_path .'settings/cspml.php' );
				
			/**
			 * Load plugin textdomain.
			 * @since 2.8 */
			 
			add_action('init', array(&$this, 'cspml_load_textdomain')); 
			
			/** 
			 * Load plugin settings */

			if (!class_exists('CodespacingProgressMap'))
				return; 
				
			$ProgressMapClass = CodespacingProgressMap::this();
			
			/**
			 * List Layout Settings */
	
			$this->cspml_list_layout = $this->cspml_get_setting('layout', 'list_layout', 'vertical');
			$this->cspml_map_height = $this->cspml_get_setting('layout', 'map_height', '400');
			$this->cspml_list_height = $this->cspml_get_setting('layout', 'list_height', '');

			/**
			 * Options Bar Settings */
	
			$this->cspml_show_options_bar = $this->cspml_get_setting('options_bar', 'show_options_bar', 'yes');
			$this->cspml_show_view_options = $this->cspml_get_setting('options_bar', 'show_view_options', 'yes');
			$this->cspml_default_view_option = $this->cspml_get_setting('options_bar', 'default_view_option', 'list');
			$this->cspml_grid_cols = $this->cspml_get_setting('options_bar', 'grid_cols', 'cols3');
			$this->cspml_show_posts_count = $this->cspml_get_setting('options_bar', 'show_posts_count', 'yes');
			$this->cspml_posts_count_clause = $this->cspml_get_setting('options_bar', 'posts_count_clause', '[posts_count] Result(s)');
	
			/**
			 * List Items settings */
	
			$this->cspml_listings_title = $this->cspml_get_setting('list_items', 'listings_title', '');
			$this->cspml_listings_details = $this->cspml_get_setting('list_items', 'listings_details', '[l=700]');
			$this->cspml_show_fire_pinpoint_btn = $this->cspml_get_setting('list_items', 'show_fire_pinpoint_btn', 'yes');
	 
			/**
			 * Sort listings settings */
			
			$this->cspml_show_sort_option = $this->cspml_get_setting('sort_option', 'show_sort_option', 'yes');
			$this->cspml_sort_options = $this->cspml_get_setting('sort_option', 'sort_options', array());
			$this->cspml_custom_sort_options = $this->cspml_get_setting('sort_option', 'custom_sort_options', '');
	
			/**
			 * Pagination Settings */
			
			$this->cspml_posts_per_page = $this->cspml_get_setting('pagiantion', 'posts_per_page', get_option('posts_per_page'));
			$this->cspml_pagination_position = $this->cspml_get_setting('pagiantion', 'pagination_position', 'bottom');
			$this->cspml_pagination_align = $this->cspml_get_setting('pagiantion', 'pagination_align', 'center');
			$this->cspml_prev_page_text = $this->cspml_get_setting('pagiantion', 'prev_page_text', '&laquo;');
			$this->cspml_next_page_text = $this->cspml_get_setting('pagiantion', 'next_page_text', '&raquo;');
			$this->cspml_show_all = $this->cspml_get_setting('pagiantion', 'show_all', 'false');
	
			/**
			 * Filter serch settings section */
			
			$this->cspml_faceted_search_option = $this->cspml_get_setting('list_filter', 'faceted_search_option', 'no');
			$this->cspml_faceted_search_position = $this->cspml_get_setting('list_filter', 'faceted_search_position', 'left');
			$this->cspml_taxonomies = json_decode($this->cspml_get_setting('list_filter', 'taxonomies', ''));
			$this->cspml_taxonomy_relation_param = $this->cspml_get_setting('list_filter', 'taxonomy_relation_param', 'AND');
			$this->cspml_custom_fields = json_decode($this->cspml_get_setting('list_filter', 'custom_fields', ''));
			$this->cspml_custom_field_relation_param = $this->cspml_get_setting('list_filter', 'custom_field_relation_param', 'AND');
			$this->cspml_filter_btn_text = $this->cspml_get_setting('list_filter', 'filter_btn_text', esc_html__('Filter'));

			if(is_admin()){
				
				/**
				 * Add plugin menu */
				 
				add_action( 'admin_menu', array(&$this, 'cspml_admin_menu'), 100 );
				
				/**
				 * Add custom links to plugin instalation area */
				 
				add_filter( 'plugin_row_meta', array(&$this, 'cspml_plugin_meta_links'), 10, 2 );
				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array(&$this, 'cspml_add_plugin_action_links') );
				
				/**
				 * Get out if the loaded page is not our plguin settings page */
				 
				if (isset($_GET['page']) && $_GET['page'] == $this->plugin_get_var ){
		
					// Call custom functions
					add_action( 'cspml_wpsf_before_settings', array(&$this, 'cspml_wpsf_before_settings') );
					add_action( 'cspml_wpsf_after_settings', array(&$this, 'cspml_wpsf_after_settings') );
				
					// Add an optional settings validation filter (recommended)
					add_filter( $this->cspml_wpsf->cspml_get_option_group() .'_settings_validate', array(&$this, 'cspml_validate_settings') );		
				
				}
			
				add_action( 'admin_notices', array(&$this, 'cspml_required_plugin_notice') );
		
			}else{
				
				/**
				 * Call .js and .css files */
				 
				add_action('wp_enqueue_scripts', array(&$this, 'cspml_styles'));
				add_action('wp_enqueue_scripts', array(&$this, 'cspml_scripts'));
				
				/**
				 * Add new attributes to Progress Map's main shortcode to use with this extension */
				 
				add_filter('shortcode_atts_codespacing_progress_map', array(&$this, 'cspml_add_main_map_atts'), 10, 3);
				
				/**
				 * Declare the name of this extension */
				 
				add_filter('cspm_ext_name', array(&$this, 'cspml_this_ext_name'), 10, 2);			
				
				/**
				 * This are all the hooks that communicates with Progress Map by displaying the listings and the filter forms */
				 
				add_filter('cspm_main_map_output_atts', array(&$this, 'cspml_add_main_map_output_atts'), 10, 2);
				add_filter('cspm_main_map_output', array(&$this, 'cspml_listings_map'), 10, 2);
				add_filter('cspm_main_map_post_ids', array(&$this, 'cspml_overide_post_ids_array'), 10, 3);
				add_action('cspm_before_main_map_query', array(&$this, 'cspml_clear_session_on_page_load'), 10, 2);
			
				/**
				 * Add or Remove scripts & styles depending the settings of "Progress Map" */
				 
				if($ProgressMapClass->loading_scripts != 'entire_site'){
					
					add_action('cspm_enqueue_ext_styles', array(&$this, 'cspml_styles'));
					add_action('cspm_dequeue_ext_styles', array(&$this, 'cspml_deregister_styles'));
					
					add_action('cspm_enqueue_ext_scripts', array(&$this, 'cspml_scripts'));
					add_action('cspm_dequeue_ext_scripts', array(&$this, 'cspml_deregister_scripts'));
					
				}
				
			}
			
			add_action('init', array(&$this, 'cspml_start_session'), 1);
			
			/**
			 * Ajax function */
			 
			add_action('wp_ajax_cspml_listings_html', array(&$this, 'cspml_listings_html'));
			add_action('wp_ajax_nopriv_cspml_listings_html', array(&$this, 'cspml_listings_html'));
			
			add_action('wp_ajax_cspml_faceted_search_query', array(&$this, 'cspml_faceted_search_query'));
			add_action('wp_ajax_nopriv_cspml_faceted_search_query', array(&$this, 'cspml_faceted_search_query'));
		
			/**
			 * Make sure that this plugin always runs after the main plugin */
			 
			add_action('activated_plugin', array(&$this, 'cspml_run_this_plugin_last'));
		
			/**
			 * Add Images Size */
			 
			if(function_exists('add_image_size'))
				add_image_size( 'cspml-listings-thumbnail', 650, 415, true );

		}

		
		/**
		 * Load plugin text domain
		 *
		 * @since 1.1
		 */
		function cspml_load_textdomain(){
			
			/**
			 * To translate the plugin, create a new folder in "wp-content/languages" ...
			 * ... and name it "cs-progress-map". Inside "cs-progress-map", paste your .mo & . po files.
			 * The plugin will detect the language of your website and display the appropriate language. */
			 
			$domain = 'cspml';
			
			$locale = apply_filters('plugin_locale', get_locale(), $domain);
		
			load_textdomain($domain, WP_LANG_DIR.'/cs-progress-map/'.$domain.'-'.$locale.'.mo');
	
			load_plugin_textdomain($domain, FALSE, $this->plugin_path.'/languages/');
			
		}

		
		/**
		 * This will display an admin notice if the main plugin "Progress Map" is not installed 
		 *
		 * @since 1.0
		 */		 
		function cspml_required_plugin_notice() {

			if(!class_exists('CodespacingProgressMap')){
				
				echo '<div class="update-nag"><p>';
					_e( 'The addon <strong>"Progress Map List & Filter"</strong> requires the plugin <strong>"Progress Map" (version 2.8.2 or upper)</strong>! Please navigate to your downloads page on Codecanyon, download the plugin <strong>"Progress Map Wordpress Plugin"</strong>, then, install and activate it. If you did not yet purchased this plugin, we recommend you to <a href="http://codecanyon.net/item/progress-map-wordpress-plugin/5581719?ref=codespacing" target="_blank">buy it from here</a>', 'cspml' );
				echo '</p></div>';
			
			}elseif(class_exists('CodespacingProgressMap')){
											
				$ProgressMapClass = CodespacingProgressMap::this();
			
				if($ProgressMapClass->plugin_version < '2.8.2'){
				
					echo '<div class="update-nag"><p>';
						_e( 'The addon <strong>"Progress Map List & Filter"</strong> requires the version <strong>2.8.2 or upper</strong> of <strong>"Progress Map"</strong>! Please upgrade the plugin "Progress Map" to the leatest version available on Codecanyon!', 'cspml' );
					echo '</p></div>';
			
				}
				
			}
            
		}
		
		
		static function this() {
			
			return self::$_this;
		
		}
		
		
		/**
		 * With this function, we'll make sure that this plugin always runs after the main plugin
		 *
		 * @since 1.0 
		 */			 
		function cspml_run_this_plugin_last() {
			
			$wp_path_to_this_file = preg_replace('/(.*)plugins\/(.*)$/', $this->plugin_path."/$2", __FILE__);
			
			$this_plugin = plugin_basename(trim($wp_path_to_this_file));
			
			$active_plugins = get_option('active_plugins');
			
			$this_plugin_key = array_search($this_plugin, $active_plugins);
			
			if ($this_plugin_key !== false) {
				
				array_splice($active_plugins, $this_plugin_key, 1);
				
				$active_plugins[] = $this_plugin;
				
				update_option('active_plugins', $active_plugins);
			}
			
		}
		
    	
		/**
		 * Add this plugin to the admin menu
		 *
		 * @since 1.0
		 */
		function cspml_admin_menu(){	
			
			if (!class_exists('CodespacingProgressMap'))
				return;
				
			add_submenu_page( 'cs_progress_map_plugin', esc_html__( 'List & Filter', 'cspml' ), esc_html__( 'List & Filter', 'cspml' ), 'manage_options', 'cspml', array(&$this, 'cspml_settings_page') );
	
		}
		
		
		/**
		 * This will display the plugin settings form 
		 *
		 * @since 1.0
		 */
		function cspml_settings_page(){
								
			/**
			 * Display the plugin settings form */
			 
			echo '<div class="wrap">';
				 
				$this->cspml_wpsf->cspml_settings(); 
				
			echo '</div>';
			
		}
		
		
		/**
		 * Get the value of a setting 
		 *
		 * @since 1.0
		 */
		function cspml_get_setting($section_id, $setting_id, $default_value = ''){
			
			return $this->cspml_setting_exists('cspml_'.$section_id.'_'.$setting_id.'', $this->settings, $default_value);
			
		}
	
    
		/**
		 * Check if array_key_exists and if empty() doesn't return false
		 * Replace the empty value with the default value if available 
		 * @empty() return false when the value is (null, 0, "0", "", 0.0, false, array())
		 *
		 * @since 1.0
		 */
		function cspml_setting_exists($key, $array, $default = ''){
			
			$array_value = isset($array[$key]) ? $array[$key] : $default;
			
			$setting_value = empty($array_value) ? $default : $array_value;
			
			return $setting_value;
			
		}
		
		
		/**
		 * Add link to the settings page of the plugin plugin in the instalation area
		 *
		 * @since 1.0
		 */
		function cspml_add_plugin_action_links($links){
		 
			return array_merge(
				array(
					'settings' => '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page='.$this->plugin_get_var.'">'.esc_html__('Settings', 'cspml').'</a>'
				),
				$links
			);
		 
		}
			
	
		/**
		 * Add plugin site link to plugin instalation area
		 *
		 * @since 1.0
		 */
		function cspml_plugin_meta_links($links, $file){
		 
			$plugin = plugin_basename(__FILE__);
		 
			/**
			 * Created by link */
			 
			if ( $file == $plugin ) {
				return array_merge(
					$links,
					array( '<a href="http://www.codespacing.com">CodeSpacing</a>' )
				);
			}
			return $links;
		 
		}
		
		
		/**
		 * Validate Settings
		 *
		 * @since 1.0
		 */
		function cspml_validate_settings($input){	    
				
			// Do your settings validation here
			// Same as $sanitize_callback from http://codex.wordpress.org/Function_Reference/register_setting
			return $input;
		}	
	
			
		/**
		 * Register & Enqueue CSS files
		 * 
		 * @since 1.0
		 */
		function cspml_styles(){

			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			if($ProgressMapClass->combine_files == "combine"){
					
				wp_register_style('cspml_combined_styles', $this->plugin_url .'css/min/cspml_combined_styles.min.css', array(), $this->plugin_version);
				wp_enqueue_style('cspml_combined_styles');
								
			}else{
								
				$min_path = $ProgressMapClass->combine_files == 'seperate_minify' ? 'min/' : '';
				$min_prefix = $ProgressMapClass->combine_files == 'seperate_minify' ? '.min' : '';
			
				/**
				 * Selectize
				 * @since 1.0 */
				 
				wp_register_style('cspml_selectize', $this->plugin_url .'css/'.$min_path.'selectize'.$min_prefix.'.css', array(), $this->plugin_version);	
				wp_enqueue_style('cspml_selectize');
								
				wp_register_style('cspml_selectize_skin', $this->plugin_url .'css/'.$min_path.'selectize.bootstrap3'.$min_prefix.'.css', array(), $this->plugin_version);	
				wp_enqueue_style('cspml_selectize_skin');
							
				/**
				 * ion-Check Radio
				 * @since 1.0 */
				 
				wp_register_style('cspml_ion_check_radio', $this->plugin_url .'css/'.$min_path.'ion.checkRadio'.$min_prefix.'.css', array(), $this->plugin_version);	
				wp_enqueue_style('cspml_ion_check_radio');								
				
				wp_register_style('cspml_ion_check_radio_skin', $this->plugin_url .'css/'.$min_path.'ion.checkRadio.html5'.$min_prefix.'.css', array(), $this->plugin_version);	
				wp_enqueue_style('cspml_ion_check_radio_skin');
				
				/**
				 * Spinner
				 * @since 1.0 */
				 
				wp_register_style('cspml_spinner', $this->plugin_url .'css/'.$min_path.'bootstrap-spinner'.$min_prefix.'.css', array(), $this->plugin_version);		
				wp_enqueue_style('cspml_spinner');
				
				/**
				 * Hover
				 * @since 1.0 */
				 
				wp_register_style('cspml_hover', $this->plugin_url .'css/'.$min_path.'hover'.$min_prefix.'.css', array(), $this->plugin_version);			
				wp_enqueue_style('cspml_hover');
				
				/**
				 * Progress Map List Style
				 * @since 1.0 */
				 			
				wp_register_style('cspml_styles', $this->plugin_url .'css/'.$min_path.'style'.$min_prefix.'.css', array(), $this->plugin_version);		
				wp_enqueue_style('cspml_styles');
				
			}
			
		}	
		
		
		/**
		 * Deregister styles
		 *
		 * @since 1.0
		 */
		function cspml_deregister_styles(){		

			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			if($ProgressMapClass->combine_files == "combine"){
				
				wp_dequeue_style('cspml_combined_styles');
				
			}else{
		
				wp_dequeue_style('cspml_selectize');
				wp_dequeue_style('cspml_selectize_skin');
				wp_dequeue_style('cspml_ion_check_radio');
				wp_dequeue_style('cspml_ion_check_radio_skin');
				wp_dequeue_style('cspml_spinner');
				wp_dequeue_style('cspml_hover');
				wp_dequeue_style('cspml_styles');
					
			}
			
		}	
		
		
		/**
		 * Register & Enqueue JS files
		 *
		 * @since 1.0
		 */
		function cspml_scripts(){		

			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			$wp_localize_script_args = array(
				'ajax_url' => admin_url('admin-ajax.php'),
				'plugin_url' => $this->plugin_url,
				'show_view_options' => $this->cspml_show_view_options,
				'grid_cols' => $this->cspml_grid_cols
			);
	
			if($ProgressMapClass->combine_files == "combine"){
			
				wp_register_script('cspml_combined_scripts', $this->plugin_url .'js/min/cspml_combined_scripts.min.js', array( 'jquery' ), $this->plugin_version, true);
				wp_enqueue_script('cspml_combined_scripts');	
				
				$localize_script_handle = 'cspml_combined_scripts';
	
			}else{
								
				$min_path = $ProgressMapClass->combine_files == 'seperate_minify' ? 'min/' : '';
				$min_prefix = $ProgressMapClass->combine_files == 'seperate_minify' ? '.min' : '';
			
				/**
				 * Selectize
				 * @since 1.0 */
				 
				wp_register_script('cspml_selectize_js', $this->plugin_url .'js/'.$min_path.'selectize'.$min_prefix.'.js', array( 'jquery' ), $this->plugin_version, true);				
				wp_enqueue_script('cspml_selectize_js');
			
				/**
				 * ion-Check Radio
				 * @since 1.0 */
				 
				wp_register_script('cspml_ion_check_radio_js', $this->plugin_url .'js/'.$min_path.'ion.checkRadio'.$min_prefix.'.js', array( 'jquery' ), $this->plugin_version, true);		
				wp_enqueue_script('cspml_ion_check_radio_js');
				
				/**
				 * Spinner
				 * @since 1.0 */
				 
				wp_register_script('cspml_spinner_js', $this->plugin_url .'js/'.$min_path.'jquery.spinner'.$min_prefix.'.js', array( 'jquery' ), $this->plugin_version, true);
				wp_enqueue_script('cspml_spinner_js');
			
				/**
				 * Progress Map List Custom Functions
				 * @since 1.0 */
			 
				wp_register_script('cspml_scripts', $this->plugin_url .'js/'.$min_path.'progress-map-list'.$min_prefix.'.js', array( 'jquery' ), $this->plugin_version, true);			
				wp_enqueue_script('cspml_scripts');
				 
				$localize_script_handle = 'cspml_scripts';
	
			}
			
			wp_localize_script($localize_script_handle, 'cspml_vars', $wp_localize_script_args);
			
		}
		
		
		/**
		 * Deregister scripts
		 *
		 * @since 1.0
		 */
		function cspml_deregister_scripts(){				 

			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			if($ProgressMapClass->combine_files == "combine"){
				
				wp_dequeue_script('cspml_combined_scripts');
				
			}else{
					
				wp_dequeue_script('cspml_selectize_js');
				wp_dequeue_script('cspml_ion_check_radio_js');
				wp_dequeue_script('cspml_spinner_js');
				wp_dequeue_script('cspml_scripts');
				
			}
			
		}
		
		
		/**
		 * Start Session
		 *
		 * @since 1.0
		 */
		function cspml_start_session() {
			
			if(!session_id()) @session_start();		
			
		}
	

		function cspml_this_ext_name($val, $atts = array()){
			
			extract( wp_parse_args( $atts, array() ) );
			
			if(isset($list_ext) && $list_ext == 'yes')
				return 'cspml_list';
				
			else return $val;
				
		}
		
		
		/**
		 * This will contain all the attributes to add to "Progress Map" shortcode.
		 * This will also contains the default value of each attribute*
		 *
		 * @since 1.0
		 */
		function cspml_default_shortcode_atts(){
		
			$default_atts = array();
			
			$default_atts['list_ext'] = 'no';
			$default_atts['listings'] = 'yes';
			$default_atts['list_layout'] = $this->cspml_list_layout; // Possible values are, "vertical", "horizontal-left", "horizontal-right"
			$default_atts['map_height'] = $this->cspml_map_height.'px';
			$default_atts['list_height'] = $this->cspml_list_height;
			$default_atts['posts_per_page'] = $this->cspml_posts_per_page;
			$default_atts['list_filter'] = $this->cspml_faceted_search_option; // Hide/Show the list search filter. Possible values are, "true" & "false".
			$default_atts['map_filter'] = $this->cspml_mfs_faceted_search_option; // Hide/Show the map search filter. Possible values are, "true" & "false".
			$default_atts['default_view'] = $this->cspml_default_view_option; // Possible values are, "grid", "list"
			$default_atts['filter_position'] = $this->cspml_faceted_search_position; // Possible values are, "left", "right"
			$default_atts['paginate_position'] = $this->cspml_pagination_position; // Possible values are, "top", "bottom", "both"
			$default_atts['paginate_align'] = $this->cspml_pagination_align; // Possible values are, "left", "right", "center"
			$default_atts['show_options_bar'] = $this->cspml_show_options_bar; // Possible values are, "yes", "no"
			
			return $default_atts;
			
		}


		/**
		 * Add custom shortcode attributes to the main map shortcode
		 * This new attributes are used with this extension
		 *
		 * @since 1.0
		 */
		function cspml_add_main_map_atts($out, $pairs, $atts){
			
			$default_atts = $this->cspml_default_shortcode_atts();
			
			foreach($default_atts as $key => $value)
				$out[$key] = $value;
			
			/**
			 * Detects when the listings extension is called and set the following attributes
			 * The listings extension is defined by calling the shortcode attributes "list_ext" and "listings" */
			 
			if(isset($atts['list_ext']) && $atts['list_ext'] == 'yes'){
				
				/**
				 * force to hide "Progress Map" carousel */
				 
				$out['carousel'] = 'no'; 
				
				/**
				 * force to hide "Progress Map" post count */
				 
				$out['show_post_count'] = 'no'; 
				
			}
			
			return $out;
			
		}
		
		
		/**
		 * Add custom attributes to the array of attributes sent to the main map output (carousel)
		 *
		 * @since 1.0
		 */
		function cspml_add_main_map_output_atts($val, $atts){
			
			if(!is_array($atts))
				return;
			
			$atts = array_merge($this->cspml_default_shortcode_atts(), $atts);
			
			foreach($atts as $key => $value)
				$val[$key] = $value;
			
			return $val;
			
		}
		
		
		/**
		 * If there was no pagination passed, clean the filtering session	
		 * In other words, we clean all session when the page is refreshed ...
		 * ... because everything works with ajax, and the only case when the page is reloaded ...		
		 * ... is when using/ the pagination.
		 * We use the URL attribute "paginate" to detect the pagination
		 *
		 * @since 1.0
		 */
		function cspml_clear_session_on_page_load($map_id, $atts = array()){
						
			extract( wp_parse_args( $atts, array() ) );
	
			if(isset($list_ext) && $list_ext == 'yes' && !isset($_GET['paginate'])){
	
				unset($_SESSION['cspml_posts_in_search'][$map_id]);	
				unset($_SESSION['cspml_sort_args'][$map_id]);				
				unset($_SESSION['cspml_listings_filter_post_ids'][$map_id]);
				
			}
			
		}

		
		/**
		 * Used in the filter hook that overides the post_ids array when there's a faceted search request
		 * The faceted search request can be detected by the presence of the $_SESSION['cspml_listings_filter_post_ids'][$map_id]
		 *
		 * @since 1.0
		 */
		function cspml_overide_post_ids_array($value, $map_id, $atts = array()){
			
			extract( wp_parse_args( $atts, array() ) );
					
			if(isset($list_ext) && $list_ext == 'yes' && isset($_SESSION['cspml_listings_filter_post_ids'][$map_id]))
				return $_SESSION['cspml_listings_filter_post_ids'][$map_id];
			else return $value;
				
		}
		
		
		function cspml_listings_map($output, $atts = array()){
			
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			extract( wp_parse_args( $atts, array() ) );

			if(isset($list_ext) && $list_ext == 'yes'){

				/**
				 * Define fixed/fullwidth layout height and width */
				
				if($ProgressMapClass->layout_type == 'fixed')
					$layout_style = "width:".$ProgressMapClass->layout_fixed_width."px; height:auto;";
				else $layout_style = $layout_style = "width:100%; height:auto;";
					
				$output = '';
				
				/**
				 * Set layout CSS classes */
				 
				if(isset($list_layout) && ($list_layout == 'horizontal-left' || $list_layout == 'horizontal-right')){
					
					$map_position = ($list_layout == 'horizontal-right') ? ' pull-right' : '';
					
					$row_classes = ' class="row no-margin"';
					$map_container_classes = ' class="col-lg-4 col-md-4 col-sm-12 col-xs-12'.$map_position.'"';
					$list_container_classes = ' class="col-lg-8 col-md-8 col-sm-12 col-xs-12"';
					$clearfix_div_classes = ' class="visible-sm visible-xs margin-top-30"';
					
				}else{
					
					$row_classes = $map_container_classes = $clearfix_div_classes = '';
					$list_container_classes = ' class="margin-top-30"';
					
				}
				
				$output .= '<div'.$row_classes.'>';
				
					$output .= '<div'.$map_container_classes.'>';
				
						/**
						 * Plugin Container */
							
						$output .= '<div class="codespacing_progress_map_area" style="'.$layout_style.'">';
							
							/**
							 * force to hide "Progress Map" faceted search if the listing's faceted search is active */
							 
							if($list_filter == 'yes' || $map_filter == 'yes')
								$faceted_search = 'no';
																			
							/**
							 * Interface elements */
							 
							$output .= $ProgressMapClass->cspm_map_interface_element($map_id, "no", $faceted_search, $search_form, $faceted_search_tax_slug, $faceted_search_tax_terms, $geo, array('list_ext' => $list_ext));
													
							/**
							 * Map */
							 
							$output .= '<div class="position-relative">';
										
								$output .= '<div id="codespacing_progress_map_div_'.$map_id.'" class="col-lg-12 col-xs-12 col-sm-12 col-md-12" style="height:'.$map_height.'"></div>';
								
								if(isset($list_layout) && $list_layout == 'vertical'){
									
									$output .= '<div class="clearfix"></div>';
									
									$output .= '<div class="cspml_resize_map cspm_border_top_radius cspm_border_shadow" data-map-id="'.$map_id.'" data-map-height="'.$map_height.'" title="'.esc_html__('Skrýt / Zobrazit mapu', 'cspml').'">';
										$output .= '<img src="'.$this->plugin_url.'img/collapse.png" class="cspm_animated push" alt="'.esc_html__('Skrýt / Zobrazit mapu', 'cspml').'" />';
									$output .= '</div>';
									
								}
								
							$output .= '</div>';			
						
						$output .= '</div>';	
				
					$output .= '</div>';
				
					/**
					 * HTML Listings */
					
					if(!isset($listings) || (isset($listings) && $listings == "yes")){				
	
						$attr = array();
						
						if(!empty($post_ids)) $attr['post_ids'] = esc_attr($post_ids);
						if(!empty($force_post_ids)) $attr['force_post_ids'] = esc_attr($force_post_ids);
						if(!empty($post_type)) $attr['post_type'] = esc_attr($post_type);						
						if(!empty($post_status)) $attr['post_status'] = esc_attr($post_status); 
						if(!empty($number_of_posts)) $attr['number_of_posts'] = esc_attr($number_of_posts);
						if(!empty($tax_query)) $attr['tax_query'] = esc_attr($tax_query);
						if(!empty($tax_query_relation)) $attr['tax_query_relation'] = esc_attr($tax_query_relation);
						if(!empty($cache_results)) $attr['cache_results'] = esc_attr($cache_results);
						if(!empty($update_post_meta_cache)) $attr['update_post_meta_cache'] = esc_attr($update_post_meta_cache);
						if(!empty($update_post_term_cache)) $attr['update_post_term_cache'] = esc_attr($update_post_term_cache);
						if(!empty($post_in)) $attr['post_in'] = esc_attr($post_in);
						if(!empty($post_not_in)) $attr['post_not_in'] = esc_attr($post_not_in);
						if(!empty($custom_fields)) $attr['custom_fields'] = esc_attr($custom_fields);
						if(!empty($custom_field_relation)) $attr['custom_field_relation'] = esc_attr($custom_field_relation);
						if(!empty($authors)) $attr['authors'] = esc_attr($authors);
						if(!empty($orderby)) $attr['orderby'] = esc_attr($orderby);
						if(!empty($orderby_meta_key)) $attr['orderby_meta_key'] = esc_attr($orderby_meta_key);
						if(!empty($order)) $attr['order'] = esc_attr($order);
						
						$attr['optional_latlng'] = (!empty($optional_latlng)) ? esc_attr($optional_latlng) : 'true';
						
						/**
						 * Posts per page */
						 
						$posts_per_page = (isset($posts_per_page) && !empty($posts_per_page)) ? $posts_per_page : $this->cspml_posts_per_page;
	
						/**
						 * Set the list height */
						
						if(isset($list_height) && !empty($list_height)){
						
							$list_height = $list_height;
							
						}else $list_height = '';
						
						/**
						 * Display the list */

						$output .= '<div'.$list_container_classes.'>';
						
							$output .= '<div'.$clearfix_div_classes.'></div>';
							
							$output .= $this->cspml_listings_html(
								array_merge(
									array(				
										'map_id' => $map_id,
										'posts_per_page' => (int) esc_attr($posts_per_page),
										'list_height' => $list_height,
										'list_filter' => $list_filter,
										'default_view' => $default_view,
										'filter_position' => $filter_position,
										'paginate_position' => $paginate_position,
										'paginate_align' => $paginate_align,
										'show_options_bar' => $show_options_bar,
									),
									$attr
								)
							);
					
						$output .= '</div>';
	
					}
				
				$output .= '</div>';
			
			}
			
			return $output;
			
		}
		
		
		/**
		 * This will display all Listings & the faceted search
		 *
		 * @since 1.0
		 */	
		function cspml_listings_html($atts = array()){

			if (!class_exists('CodespacingProgressMap'))
				return; 
				
			$ProgressMapClass = CodespacingProgressMap::this();
										
			global $post, $wp_query, $wp_rewrite, $paged;
		
			/**
			 * Get The ID Of The Page Where The Shortcode was Executed */
			 
			$shortcode_page_id = isset($_POST['shortcode_page_id']) ? esc_attr($_POST['shortcode_page_id']) : $wp_query->get_queried_object_id();
			
			if(is_tax())
				$page_template = get_query_var('taxonomy');
			elseif(is_author())
				$page_template = 'author';
			else $page_template = get_page_template_slug($shortcode_page_id);

			$template_tax_query = is_tax() ? $page_template.','.get_query_var('term') : '';
			
			if(isset($_POST['page_template'])) $page_template = esc_attr($_POST['page_template']);
			if(isset($_POST['template_tax_query'])) $template_tax_query = esc_attr($_POST['template_tax_query']);			
			
			/**
			 * Get the page template slug used to render the listings
			 * Usefull for re-executing hooks after AJAX load */
			 
			do_action('cspml_before_listings', $shortcode_page_id, $page_template, $template_tax_query);

			/**
			 * Override the default post_ids array by the shortcode atts post_ids */
			 
			extract( wp_parse_args( $atts, array(
				'map_id' => 'initial',		
				'post_ids' => '',
				'force_post_ids' => 'no', // whetear to force retreiving posts by the attribute 'post_ids' or to call the main query when 'post_ids' is empty
				'post_type' => '',
				'post_status' => '', 
				'number_of_posts' => '',
				'tax_query' => '',
				'tax_query_field' => 'id', //@since 2.6.1
				'tax_query_relation' => '',
				'cache_results' => '',
				'update_post_meta_cache' => '',
				'update_post_term_cache' => '',
				'post_in' => '',
				'post_not_in' => '',
				'custom_fields' => '',
				'custom_field_relation' => '',
				'authors' => '',
				'orderby' => 'post__in',
				'orderby_meta_key' => '',
				'order' => 'ASC',
				'posts_per_page' => '',
				'optional_latlng' => '', // Wether we will display all posts event those with no Lat & Lng
				'show_listings' => 'true',
				'list_filter' => '',
				'filter_position' => '',
				'list_height' => '',
				'default_view' => '',
				'paginate_position' => '',
				'paginate_align' => '',
				'show_options_bar' => '',
			)));
			
			/**
			 * Clear the following sessions after the page refresh.
			 * Note: The page refresh on, Pagination, Sort Listings, Filter listings & on any other Ajax Request. ...
			 * ... This code will detect a simple page refresh (F5), that's what we want. */
			 
			if(!isset($_GET['paginate']) && !isset($_POST['sort_call']) && !isset($_POST['ajax_call']) && !isset($_POST['filter_form_call'])){
				
				unset($_SESSION['cspml_posts_in_search'][$map_id]);	
				unset($_SESSION['cspml_sort_args'][$map_id]);				
				unset($_SESSION['cspml_listings_filter_post_ids'][$map_id]);

			}else{				
														
				/**
				 * Recover shortcode atts missed during an AJAX request.
				 * Note: When runing an AJAX request, some shortcode atts became empty. 
				 * A workaround fix is to send them whith the AJAX request to get/use them later! */
				
				if(isset($_POST['paginate_position']))
					$paginate_position = esc_attr($_POST['paginate_position']);
					
				if(isset($_POST['paginate_align']))
					$paginate_align = esc_attr($_POST['paginate_align']);
				
				if(isset($_POST['posts_per_page']))
					$posts_per_page = esc_attr($_POST['posts_per_page']);
				
				if(isset($_POST['post_type']))
					$post_type = esc_attr($_POST['post_type']);

			}
			
			$map_id = esc_attr($map_id);
			
			/**
			 * Specify which divider to use for the pagination */
			 
			$exploded_URI = explode('?', esc_url($_SERVER['REQUEST_URI']));
			
			if(is_home() || is_front_page()) $divider = '?';
			else $divider = (count($exploded_URI) > 1) ? '&' : '?';
			
			/**
			 * The number of posts per page	*/
			 
			$posts_per_page = (!empty($posts_per_page) && $posts_per_page != 0) ? esc_attr($posts_per_page) : $this->cspml_posts_per_page;
			
			/**
			 * This will save the current view (List or Grid) even after AJAX request */
			 
			$current_view = (isset($_POST['current_view'])) ? esc_attr($_POST['current_view']) : $default_view;
			
			/**
			 * Init empty sort_args array() */
			 
			$sort_args = array();
			
			/**
			 * Detect when a sort call was sent and return data */
			 
			if(isset($_POST['sort_call'])){
				
				$shortcode_page_id = esc_attr($_POST['shortcode_page_id']);
				$divider = esc_attr($_POST['divider']);			
				$post_ids = esc_attr($_POST['init_post_ids']);
				$optional_latlng = esc_attr($_POST['optional_latlng']);
				
			}
			
			/**
			 * $_POST['ajax_call'] is only used when the user filter listings OR uses the search/filter form of the map */
			 
			if(isset($_POST['ajax_call'])){
				
				$map_id = esc_attr($_POST['map_id']);
				$post_ids = $_POST['post_ids'];
				$shortcode_page_id = esc_attr($_POST['shortcode_page_id']);
				$divider = esc_attr($_POST['divider']);
				$optional_latlng = esc_attr($_POST['optional_latlng']);
				
				if(isset($_POST['save_session'])){
	
					/**
					 * After filtering, we store the post_ids retreived from the map ...
					 * ... in order to use them again in the pagination, this way the pagination ...
					 * ... wont start again from the begining.
					 * After the page has been loaded from the pagination ...
					 * ... the $_POST['ajax_call'] will not exist ...
					 * ... now we check if $_SESSION['cspml_posts_in_search'][$map_id]['filtering'] equals TRUE ...
					 * ... if so, we call the post_ids stored in $_SESSION['cspml_posts_in_search'][$map_id]['post_ids'] */
					 
					$_SESSION['cspml_posts_in_search'][$map_id]['filtering'] = true;
					$_SESSION['cspml_posts_in_search'][$map_id]['post_ids'] = $post_ids;	
								
					/**
					 * Save the post_ids in a session in order to not start ...
					 * ... calling all posts when paginating or sorting by or filtering */
					 
					$_SESSION['cspml_listings_filter_post_ids'][$map_id] = $post_ids;
					
				}
				
			/**
			 * This case means that no filtering has been done from the faceted search in the map ...
			 * ... or that we've just simply start loading the script */
			 
			}else{

				/**
				 * Check if there was no filtering done before by the faceted search in the map */
				 
				if(!isset($_SESSION['cspml_posts_in_search'][$map_id]['filtering'])){
					
					/**
					 * Remove $_SESSION['cspml_posts_in_search'][$map_id]['filtering'] And laod post_ids */
					 
					unset($_SESSION['cspml_posts_in_search'][$map_id]);
					
					if(empty($post_ids) && isset($_POST['sort_call'])){
						
						$post_ids = explode(',', $init_post_ids);
						
					/**
					 * If post ids being pased from the shortcode parameter @post_ids
					 * Check the format of the @post_ids value */
					 
					}elseif(!empty($post_ids)){
						
						$query_post_ids = explode(',', $post_ids);	
										
					}else{
	
						/**
						 * The main query */
						 
						if(!empty($post_type) && empty($post_ids) && $force_post_ids == 'no'){
											
							$query_post_ids = $ProgressMapClass->cspm_main_query(
								array(
									'post_type' => $post_type, 
									'post_status' => $post_status,
									'number_of_posts' => $number_of_posts, 
									'tax_query' => $tax_query,
									'tax_query_field' => $tax_query_field,
									'tax_query_relation' => $tax_query_relation, 
									'cache_results' => $cache_results, 
									'update_post_meta_cache' => $update_post_meta_cache,
									'update_post_term_cache' => $update_post_term_cache,
									'post_in' => $post_in,
									'post_not_in' => $post_not_in,
									'custom_fields' => $custom_fields,
									'custom_field_relation' => $custom_field_relation,
									'authors' => $authors,
									'orderby' => $orderby,
									'orderby_meta_key' => $orderby_meta_key,
									'order' => $order,
									'optional_latlng' => $optional_latlng
								)								
							);
																				 
						}elseif($force_post_ids == 'no'){
							
							$query_post_ids = $ProgressMapClass->cspm_main_query();
						
						}else $query_post_ids = array();

					}
					
					$post_ids = $query_post_ids;				

				/**
				 * If there was a filetring before, we call post_ids stored in the session */
				 
				}else $post_ids = $_SESSION['cspml_posts_in_search'][$map_id]['post_ids'];
	
			}

			$post_type = empty($post_type) ? $ProgressMapClass->post_type : $post_type;
			
			/**
			 * Get the sort data (Of the "sort by" select box) after posts filtering & pagination */
			 
			if((isset($_POST['ajax_call']) || isset($_GET['paginate'])) && isset($_SESSION['cspml_sort_args'][$map_id]))
				$sort_args = $_SESSION['cspml_sort_args'][$map_id];
	
			/**
			 * Get "post_ids" when a request for the listings filter form (Of the listings's faceted search) was sent */

			if(isset($_SESSION['cspml_listings_filter_post_ids'][$map_id]))
				$post_ids = $_SESSION['cspml_listings_filter_post_ids'][$map_id];
			
			/**
			 * Set "init_post_ids" as "post_ids" when we reset the filter or the "Progress Map" search */

			if(isset($_POST['reset_list']) && isset($_POST['init_post_ids'])){
				
				$post_ids = explode(',', esc_attr($_POST['init_post_ids']));
				 
				$_SESSION['cspml_listings_filter_post_ids'][$map_id] = $post_ids;
				$_SESSION['cspml_posts_in_search'][$map_id]['post_ids'] = $post_ids;	
				
			}
			
			/**
			 * This will contain all the post IDs of the first page load
			 * This is usefull to know the post_ids we begins with in order to do other calculations */		
			 	
			$init_post_ids = (isset($_POST['init_post_ids'])) ? esc_attr($_POST['init_post_ids']) : implode(',', $post_ids);
			$count_init_post_ids = !empty($init_post_ids) ? count(explode(',', $init_post_ids)) : '0';

			$output = '';	
			
			if(esc_attr($show_listings) == 'true'){
					
				/**
				 * Note: When doing an AJAX request, the result returns this element. 
				 * We need this element only when loading the listings for the first time.
				 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
					 
				if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
					$output .= '<div id="cspml_listings_container" data-map-id="'.$map_id.'" class="row">';
									
					/**
					 * == The options bar
					 * Note: When doing an AJAX request, the result returns this element. 
					 * We need this element only when loading the listings for the first time.
					 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
					
					if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']) && $show_options_bar == 'yes'){
						
						$output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
							$output .= $this->cspml_options_bar($map_id, $count_init_post_ids, $shortcode_page_id, $current_view, $list_filter, $filter_position);
						$output .= '</div>';
					
					}
					
					/**
					 * Set the list height & style */
					 					
					if(!empty($list_height)){
						
						$list_height = (strpos($list_height, 'px') !== false) ? $list_height : $list_height.'px';
						$list_style = 'style="max-height:'.$list_height.'; overflow:auto;"';
						
					}else $list_style = '';
										
					/**
					 * Note: When doing an AJAX request, the result returns this element. 
					 * We need this element only when loading the listings for the first time.
					 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
					 
					if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call'])){
					
						$output .= '<div class="clear-both"></div>';
						
						$output .= '<div class="cspml_list_and_filter_container row no-margin" '.$list_style.'>';
					
					}
					
						if(esc_attr($list_filter) == 'yes'){
							
							/**
							 * Note: When doing an AJAX request, the result returns this element. 
							 * We need this element only when loading the listings for the first time.
							 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
						 
							if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call'])){
							
								/**
								 * == The faceted search form */
								
								$pull_direction = ($filter_position == 'left') ? '' : 'pull-right';
						
								$output .= '<div class="cspml_fs_container col-lg-3 col-md-3 col-sm-12 col-xs-12 '.$pull_direction.'" data-map-id="'.$map_id.'">';
								
									$output .= $this->cspml_listings_faceted_search_form($map_id, $post_type, $post_ids, $list_filter);
								
								$output .= '</div>';
							 
							}
		
							$listings_area_cols = 'col-lg-9 col-md-9 col-sm-12 col-xs-12';
							
						}else $listings_area_cols = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
						
						/**
						 * Note: When doing an AJAX request, the result returns this element. 
						 * We need this element only when loading the listings for the first time.
						 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
						 
						if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
							$output .= '<div class="cspml_listing_items_container_'.$map_id.' '.$listings_area_cols.'">';
							
							/**
							 * == The loading spinner
							 * Note: When doing an AJAX request, the result returns this element. 
							 * We need this element only when loading the listings for the first time.
							 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
							 
							if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call'])){
									
								$output .= '<div class="cspml_loading_container cspm_border_shadow cspm_border_radius">';
									$output .= '<span>'.esc_html__('Loading', 'cspml').'</span>';
									$output .= '<span class="wrapper"><span class="cssload-loader"></span></span>';
								$output .= '</div>';
								
							}
							
							/**
							 * == Listings
							 * Note: When doing an AJAX request, the result returns this element. 
							 * We need this element only when loading the listings for the first time.
							 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
							 
							if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
								$output .= '<div class="cspml_listings_area_'.$map_id.' row" data-paginate-position="'.$paginate_position.'" data-paginate-align="'.$paginate_align.'" data-posts-per-page="'.$posts_per_page.'">'; 
							
								/** 
								 * Post Type of list items */
								 
								$output .= '<input type="hidden" name="post_type" id="post_type" value="'.$post_type.'" />';
								
								/** 
								 * Post IDs loaded for the first time */
								 
								$output .= '<input type="hidden" name="init_post_ids" id="init_post_ids" value="'.$init_post_ids.'" />';
								
								/**
								 * Number of posts loaded for the first time */
								 
								$output .= '<input type="hidden" name="count_init_post_ids" id="count_init_post_ids" value="'.$count_init_post_ids.'" />';							
								
								/**
								 * Post IDs returned after filter request */
								 
								$output .= '<input type="hidden" name="post_ids" id="post_ids" value="'.implode(',', $post_ids).'" />';
								
								/**
								 * The ID of the page that executes the shortcode */
								 
								$output .= '<input type="hidden" name="shortcode_page_id" id="shortcode_page_id" value="'.$shortcode_page_id.'" />';
								
								/**
								 * The page template name of the page that contains the shortcode.
								 * Useful to hide/show or customize elements by page template. */
								 
								$output .= '<input type="hidden" name="page_template" id="page_template" value="'.$page_template.'" />';
								
								/**
								 * The taxonomy of the page template.
								 * Used for archive and taxonomy page templates. */
								 
								$output .= '<input type="hidden" name="template_tax_query" id="template_tax_query" value="'.$template_tax_query.'" />';
								
								/**
								 * The devider to use befor pagination (? or &) */
								 
								$output .= '<input type="hidden" name="divider" id="divider" value="'.$divider.'" />';
								
								/**
								 * Whether to show or hide listings */
								 
								$output .= '<input type="hidden" name="show_listings" id="show_listings" value="'.$show_listings.'" />';
								
								/**
								 * Whether to show or hide listings that doesn't have LatLng coordinates */
								 
								$output .= '<input type="hidden" name="optional_latlng" id="optional_latlng" value="'.$optional_latlng.'" />';

								if(!empty($post_ids) && !empty($init_post_ids) && $show_listings == 'true'){
									
									if ( get_query_var('paged') ) $paged = get_query_var('paged');
									elseif ( get_query_var('page') ) $paged = get_query_var('page');
									else $paged = 1;
									
									$total_pages = ceil(count($post_ids)/$posts_per_page); 
					 
									/**
									 * Check to see if we are using rewrite rules */
									 
									$rewrite = $wp_rewrite->wp_rewrite_rules();
										$format = (!empty($rewrite)) ? 'page/%#%/?paginate=true' : ''.$divider.'page=%#%&paginate=true';
									
									/**
									 * Get the link of the page where the shortcode was executed */
									 
									if($page_template != 'author')
										$shortcode_page_link = get_permalink($shortcode_page_id).$format;
									else $shortcode_page_link = get_author_posts_url($shortcode_page_id).$format;
									
									$show_all_pages = ($this->cspml_show_all == 'false') ? false : true;
									
									/**
									 * Get the sort args when a sort request was sent */
									 
									if(isset($_POST['sort_call']) && isset($_POST['data_sort']) && isset($_POST['data_order'])){
										
										$sort_args = $this->cspml_set_posts_order(esc_attr($_POST['data_sort']), esc_attr($_POST['data_order']));
															
										$_SESSION['cspml_sort_args'][$map_id] = $sort_args;
					
									} 
		
									$query_args = array(
										'post_type' => $post_type, 
										'post__in' => $post_ids, 
										'posts_per_page' => $posts_per_page, 
										'post_status' => $post_status,
										'paged' => $paged,
										'orderby' => $orderby,
										'order' => $order,
									);
	
									/**
									 * Remove the default query order atts and replace them with the new atts ...
									 * ... of the listings "sort by" feature */
									 
									if(count($sort_args) > 0){
										
										unset($query_args['orderby']);
										unset($query_args['meta_key']);
										unset($query_args['order']);	
										
									}
									
									/**
									 * Make the query args and the sort data one array */
									 
									$combined_query_args = $query_args + $sort_args;						

									/**
									 * The listings */
									 
									query_posts( $combined_query_args );
									
									if(have_posts()){ 
										
										$pagination = paginate_links( array(
											'base' => $shortcode_page_link, //@add_query_arg('paged','%#%'), // the base URL, including query arg
											'format' => '', // this defines the query parameter that will be used, in this case "p"
											'prev_text' => sprintf( esc_html__('%s', 'cspml'), $this->cspml_prev_page_text ), // text for previous page
											'next_text' => sprintf( esc_html__('%s', 'cspml'), $this->cspml_next_page_text ), // text for next page
											'total' => $total_pages, // the total number of pages we have
											'current' => $paged, // the current page
											'show_all' => $show_all_pages,
											'end_size' => 1,
											'mid_size' => 2,
											'type' => 'array',
										));

										/**
										 * Pagination */
										 
										if($paginate_position == 'top' || $paginate_position == 'both') 
											$output .= $this->cspml_pagination($map_id, $pagination, $paginate_align);								
										
										$i = 1;
										
										$output .= apply_filters('cspml_before_listing_loop', '');
										
										while ( have_posts() ) : the_post(); 
										
											$post_id = $post->ID;
											
											/**
											 * Get lat and lng data */
											 
											$lat = get_post_meta($post_id, CSPM_LATITUDE_FIELD, true);
											$lng = get_post_meta($post_id, CSPM_LONGITUDE_FIELD, true);
										
											//$secondary_latlng = get_post_meta($post_id, CSPM_SECONDARY_LAT_LNG_FIELD, true);
											
											/**
											 * Show items only if lat and lng are not empty.
											 * Note: To display all items even those without Lat&Lng, we use the attribute @optional_latlng. */
											
											if(!empty($lat) && !empty($lng) || esc_attr($optional_latlng) == 'true'){
												
												$marker_img_array = apply_filters('cspml_post_thumb', wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'cspml-listings-thumbnail' ), $post_id);
												
												$marker_img = isset($marker_img_array[0]) ? $marker_img_array[0] : $this->plugin_url.'img/thumbnail.jpg';																			
												
												$item_title = $this->cspml_items_title($post_id, $this->cspml_listings_title, true);
												
												$item_details = $this->cspml_items_details($post_id, $this->cspml_listings_details);
												
												$the_permalink = get_permalink($post_id); 																			
																			
												if($current_view == "list"){
													
													$list_view_atts = array(
														'index' => $i,
														'map_id' => $map_id,
														'post_id' => $post_id,
														'lat' => $lat,
														'lng' => $lng,
														'marker_img' => $marker_img,
														'item_title' => $item_title,
														'item_details' => apply_filters('cspml_list_item_description', $item_details, $post_id, 'list', esc_attr($list_filter), $map_id, $lat.'_'.$lng),
														'the_permalink' => $the_permalink,
													);
													
													$output .= $this->cspml_list_view_output($list_view_atts);
												
												}else{
																									
													$grid_view_atts = array(
														'index' => $i,
														'map_id' => $map_id,
														'post_id' => $post_id,
														'lat' => $lat,
														'lng' => $lng,
														'marker_img' => $marker_img,
														'item_title' => $item_title,
														'item_details' => apply_filters('cspml_grid_item_description', $item_details, $post_id, 'grid', esc_attr($list_filter), $map_id, $lat.'_'.$lng),
														'the_permalink' => $the_permalink,
													);
													 
													$output .= $this->cspml_grid_view_output($grid_view_atts);
												
												}
												
												$i++;
																				
											}
											
										endwhile; 
										
										$output .= apply_filters('cspml_after_listing_loop', '');
										
										/**
										 * Pagination */
										 
										if($paginate_position == 'bottom' || $paginate_position == 'both') 
											$output .= $this->cspml_pagination($map_id, $pagination, $paginate_align);							
														
									}else{
							
										$output .= '<div class="cspml_no_results">'.apply_filters('cspml_no_results_msg', esc_html__('We couldn\'t find any results!', 'cspml')).'</div>';
									
									}
									
									wp_reset_query();
									
								}else $output .= '<div class="cspml_no_results">'.apply_filters('cspml_no_results_msg', esc_html__('We couldn\'t find any results!', 'cspml')).'</div>';
						
							/**
							 * Note: When doing an AJAX request, the result returns this element. 
							 * We need this element only when loading the listings for the first time.
							 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
							 
							if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
								$output .= '</div><div class="clear-both"></div>';
						
						/**
						 * Note: When doing an AJAX request, the result returns this element. 
						 * We need this element only when loading the listings for the first time.
						 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
						 
						if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
							$output .= '</div>';
						
					/**
					 * Note: When doing an AJAX request, the result returns this element. 
					 * We need this element only when loading the listings for the first time.
					 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
					 
					if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
						$output .= '</div>';
					
				/**
				 * Note: When doing an AJAX request, the result returns this element. 
				 * We need this element only when loading the listings for the first time.
				 * To prevent this, we must detect AJAX requests and hide/show this element accordingly! */
				 
				if(!isset($_POST['ajax_call']) && !isset($_POST['sort_call']))
					$output .= '</div><div class="clear-both"></div>';
			
			/**
			 * In case we don't want to display the listings. And in case we want to display a fullscreen map with the filter,
			 * we'll need this hidden inputs for the filter query */
			 	
			}else{
				
				$output .= '<div class="cspml_listings_area_'.$map_id.'" data-paginate-position="'.$paginate_position.'" data-paginate-align="'.$paginate_align.'">'; 					
				
					/** 
					 * Post Type of list items */
					 
					$output .= '<input type="hidden" name="post_type" id="post_type" value="'.$post_type.'" />';
								
					/** 
					 * Post IDs loaded for the first time */
					 
					$output .= '<input type="hidden" name="init_post_ids" id="init_post_ids" value="'.$init_post_ids.'" />';
					
					/**
					 * Number of posts loaded for the first time */
					 
					$output .= '<input type="hidden" name="count_init_post_ids" id="count_init_post_ids" value="'.$count_init_post_ids.'" />';							
					
					/**
					 * Post IDs returned after filter request */
					 
					$output .= '<input type="hidden" name="post_ids" id="post_ids" value="'.implode(',', $post_ids).'" />';
					
					/**
					 * The ID of the page that executes the shortcode */
					 
					$output .= '<input type="hidden" name="shortcode_page_id" id="shortcode_page_id" value="'.$shortcode_page_id.'" />';
					
					/**
					 * The page template name of the page that contains the shortcode.
					 * Useful to hide/show or customize elements by page template. */
					 
					$output .= '<input type="hidden" name="page_template" id="page_template" value="'.$page_template.'" />';
					
					/**
					 * The taxonomy of the page template.
					 * Used for archive and taxonomy page templates. */
					 
					$output .= '<input type="hidden" name="template_tax_query" id="template_tax_query" value="'.$template_tax_query.'" />';
					
					/**
					 * The devider to use befor pagination (? or &) */
					 
					$output .= '<input type="hidden" name="divider" id="divider" value="'.$divider.'" />';
					
					/**
					 * Whether to show or hide listings */
					 
					$output .= '<input type="hidden" name="show_listings" id="show_listings" value="'.$show_listings.'" />';
								
					/**
					 * Whether to show or hide listings that doesn't have LatLng coordinates */
					 
					$output .= '<input type="hidden" name="optional_latlng" id="optional_latlng" value="'.$optional_latlng.'" />';

				$output .= '</div>';
						
			}
					
			if(isset($_POST['ajax_call']) || isset($_POST['sort_call']) || isset($_POST['filter_form_call'])) die($output);
			else return $output;
			
		}
		
		
		/**
		 * The list view output
		 *
		 * @since 1.0
		 */
		function cspml_list_view_output($atts = array()){
			
			$defaults = array(
				'index' => '',
				'map_id' => '',
				'post_id' => '',
				'lat' => '',
				'lng' => '',
				'marker_img' => '',
				'item_title' => '',
				'item_details' => '',
				'the_permalink' => '',
			);	
			
			extract(wp_parse_args($atts, $defaults));
			
			$output = '';
			
			$output .= '<div id="'.$map_id.'_listing_item_'.$post_id.'" data-coords="'.$lat.'_'.$lng.'" data-view="list" data-map-id="'.$map_id.'" data-post-id="'.$post_id.'" class="cspml_item_holder row cspm_animated fadeIn">';
				
				$output .= '<div class="list_view_holder col-lg-12 col-md-12 col-sm-12 col-xs-12">';				
					
					/**
					 * A filter to display custom HTML/code before the item's HTML */
					
					$output .= apply_filters('cspml_before_listing_item', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
					
					$output .= '<div class="clear-both"></div>';	
							
					$output .= '<div class="'.apply_filters('cspml_item_holder_class', 'cspml_item', $post_id).' row row-no-margin cspm_border_shadow cspm_border_radius">';
						
						$output .= '<div class="cspml_thumb_container '.apply_filters('cspml_thumb_container_class', 'col-lg-2 col-md-2 col-sm-2 col-xs-12', 'listview').' no-padding">';
					
							/**
							 * A filter to display custom HTML/code before the item's thumb */
							 
							$output .= apply_filters('cspml_before_listing_thumb', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
							
							$show_pinpoint_btn = ($this->cspml_show_fire_pinpoint_btn == 'yes' && !empty($lat) && !empty($lng)) ? 'yes' : 'no';
							
							$link_overlay_corner_class = $show_pinpoint_btn == 'yes' ? 'cspm_remove_bg_corner' : '';
							
							/**
							 * Item's permalink */
							 								
							$output .= '<a href="'.$the_permalink.'"><div class="cspml_item_link_overlay '.$link_overlay_corner_class.' cspm_animated fadeIn">';
							
								$output .= '<img alt="" src="'.$this->plugin_url.'img/link.png" title="'.esc_html__('Detail', 'cspml').'" class="cspml_item_link" />';
							
							$output .= '</div></a>';

							/**
							 * Item's thumb */
							 
							$output .= '<img alt="" src="'.$marker_img.'" class="thumb img-responsive" />';
							
							/**
							 * Item's Pinpoint */
							 
							if($show_pinpoint_btn == 'yes'){
								 
								$output .= '<div class="cspml_item_pinpoint_overlay cspml_fire_pinpoint" data-map-id="'.$map_id.'" data-coords="'.$lat.'_'.$lng.'" data-post-id="'.$post_id.'">';
									
									$output .= '<img alt="" src="'.$this->plugin_url.'img/pinpoint.png" title="'.esc_html__('Ukaž na mapě', 'cspml').'" class="cspml_item_pinpoint" />';
									
									$output .= '<div class="cspml_item_pinpoint_triangle"></div>';
									
								$output .= '</div>';
							
							}
							
							/**
							 * A filter to display custom HTML/code after the item's thumb */
							 
							$output .= apply_filters('cspml_after_listing_thumb', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
							
						$output .= '</div>';
						
						/**
						 * Title & Content */
						 								
						$output .= '<div class="'.apply_filters('cspml_details_container_class', 'cspml_details_container col-lg-10 col-md-10 col-sm-10 col-xs-12', 'listview').'" data-view="list">';	
					
							/**
							 * A filter to display custom HTML/code before the item's title container */
							 
							$output .= apply_filters('cspml_before_title_container', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);

							$output .= '<div class="cspml_details_title '.apply_filters('cspml_details_title_extra_class', '').' col-lg-12 col-xs-12 col-sm-12 col-md-12">';
					
								/**
								 * A filter to display custom HTML/code before the item's title */
								 
								$output .= apply_filters('cspml_before_listing_title', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
								
								/** 
								 * Item's title */
								 
								$output .= stripslashes_deep($item_title);
					
								/**
								 * A filter to display custom HTML/code before the item's title */
								 
								$output .= apply_filters('cspml_after_listing_title', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
								
							$output .= '</div>';
							
							/**
							 * Item's excerpt/content */
							 
							$output .= '<div class="cspml_details_content '.apply_filters('cspml_details_content_extra_class', '').' col-lg-12 col-xs-12 col-sm-12 col-md-12">';
								
								$output .= '<hr class="no-margin-top margin-bottom-15" />';
								
								/**
								 * A filter to display custom HTML/code before the item's title */
								 
								$output .= apply_filters('cspml_before_listing_excerpt', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
								
								/** 
								 * Item's excerpt/content */
								 
								$output .= stripslashes_deep($item_details);
								
								/**
								 * A filter to display custom HTML/code after the item's title */
								 
								$output .= apply_filters('cspml_after_listing_excerpt', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
								
							$output .= '</div>';	
					
							/**
							 * A filter to display custom HTML/code after the item's excerpt/content container */
							 
							$output .= apply_filters('cspml_after_excerpt_container', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
							
						$output .= '</div>';
							
						$output .= '<div class="clear-both"></div>';
							
					$output .= '</div>';
				
					$output .= '<div class="clear-both"></div>';

					/**
					 * A filter to display custom HTML/code after the item's HTML */
					 							
					$output .= apply_filters('cspml_after_listing_item', '', $post_id, $map_id, 'listview', $lat.'_'.$lng, $the_permalink);
			
				$output .= '</div>';				
	
			$output .= '</div>';						
			
			return apply_filters('cspml_list_view_item', $output, $post_id, $map_id, $lat.'_'.$lng, $the_permalink);						
																
		}
		
		
		/**
		 * The grid view output
		 *
		 * @since 1.0
		 */
		function cspml_grid_view_output($atts = array()){
			
			$defaults = array(
				'index' => '',
				'map_id' => '',
				'post_id' => '',
				'lat' => '',
				'lng' => '',
				'marker_img' => '',
				'item_title' => '',
				'item_details' => '',
				'the_permalink' => '',
			);	
			
			extract(wp_parse_args($atts, $defaults));
			
			/**
			 * @cols1, displays one item per row */
			 
			if($this->cspml_grid_cols == 'cols1'){
				
				$grid_classes = ' col-lg-12 col-md-12 col-sm-12 col-xs-12';
			
			/**
			 * @cols2, displays two items per row */
			 
			}elseif($this->cspml_grid_cols == 'cols2'){
				
				$grid_classes = ' col-lg-6 col-md-6 col-sm-6 col-xs-12';
			
			/**
			 * @cols4, displays four items per row */
			 
			}elseif($this->cspml_grid_cols == 'cols4') {
				
				$grid_classes = ' col-lg-3 col-md-3 col-sm-6 col-xs-12';
			
			/**
			 * @cols6, displays six items per row */
			 
			}elseif($this->cspml_grid_cols == 'cols6') {
				
				$grid_classes = ' col-lg-2 col-md-2 col-sm-6 col-xs-12';
			
			/**
			 * @cols3, displays three items per row */
			 
			}else $grid_classes = ' col-lg-4 col-md-4 col-sm-6 col-xs-12';
			
			/**
			 * A filter to change the grid classes */
			 
			$grid_classes = apply_filters('cspml_listing_grid_classes', $grid_classes);
			
			/**
			 * Displaying items */
			 	
			$output = '';
			
			$output .= '<div id="'.$map_id.'_listing_item_'.$post_id.'" data-coords="'.$lat.'_'.$lng.'" data-view="grid" data-map-id="'.$map_id.'" data-post-id="'.$post_id.'" class="cspml_item_holder grid '.$grid_classes.' cspm_animated fadeIn">';
				
				$output .= '<div class="list_view_holder">';				
					
					/**
					 * A filter to display custom HTML/code before the item's HTML */
					 
					$output .= apply_filters('cspml_before_listing_item', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
					
					$output .= '<div class="clear-both"></div>';	
							
					$output .= '<div class="'.apply_filters('cspml_item_holder_class', 'cspml_item', $post_id).' col-lg-12 col-xs-12 col-sm-12 col-md-12 cspm_border_shadow cspm_border_radius">';
						
						$output .= '<div class="cspml_thumb_container '.apply_filters('cspml_thumb_container_class', 'col-lg-12 col-xs-12 col-sm-12 col-md-12', 'gridview').' no-padding">';
					
							/**
							 * A filter to display custom HTML/code before the item's thumb */
							 
							$output .= apply_filters('cspml_before_listing_thumb', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
							$show_pinpoint_btn = ($this->cspml_show_fire_pinpoint_btn == 'yes' && !empty($lat) && !empty($lng)) ? 'yes' : 'no';
							
							$link_overlay_corner_class = $show_pinpoint_btn == 'yes' ? 'cspm_remove_bg_corner' : '';
							
							/**
							 * Item's permalink */
							 								
							$output .= '<a href="'.$the_permalink.'"><div class="cspml_item_link_overlay '.$link_overlay_corner_class.' cspm_animated fadeIn">';
							
								$output .= '<img alt="" src="'.$this->plugin_url.'img/link.png" title="'.esc_html__('Detail', 'cspml').'" class="cspml_item_link" />';
							
							$output .= '</div></a>';

							/**
							 * Item's thumb */
							 
							$output .= '<img alt="" src="'.$marker_img.'" class="thumb img-responsive" />';
							
							/**
							 * Item's Pinpoint */
							 
							if($show_pinpoint_btn == 'yes'){
								 
								$output .= '<div class="cspml_item_pinpoint_overlay cspml_fire_pinpoint" data-map-id="'.$map_id.'" data-coords="'.$lat.'_'.$lng.'" data-post-id="'.$post_id.'">';
									
									$output .= '<img alt="" src="'.$this->plugin_url.'img/pinpoint.png" title="'.esc_html__('Ukaž na mapě', 'cspml').'" class="cspml_item_pinpoint" />';
									
									$output .= '<div class="cspml_item_pinpoint_triangle"></div>';
									
								$output .= '</div>';
							
							}
							
							/**
							 * A filter to display custom HTML/code after the item's thumb */
							 
							$output .= apply_filters('cspml_after_listing_thumb', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
						$output .= '</div>';
						
						/**
						 * Title & Content */
						 								
						$output .= '<div class="'.apply_filters('cspml_details_container_class', 'cspml_details_container col-lg-12 col-xs-12 col-sm-12 col-md-12', 'gridview').'" data-view="grid">';	
					
							/**
							 * A filter to display custom HTML/code before the item's title container */
							 
							$output .= apply_filters('cspml_before_title_container', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
							$output .= '<div class="cspml_details_title '.apply_filters('cspml_details_title_extra_class', '').' col-lg-12 col-xs-12 col-sm-12 col-md-12">';
					
								/**
								 * A filter to display custom HTML/code before the item's title */
								 
								$output .= apply_filters('cspml_before_listing_title', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
								
								/** 
								 * Item's title */
								 
								$output .= stripslashes_deep($item_title);
					
								/**
								 * A filter to display custom HTML/code after the item's title */
								 
								$output .= apply_filters('cspml_after_listing_title', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
							$output .= '</div>';
							
							$output .= '<div class="cspml_details_content '.apply_filters('cspml_details_content_extra_class', 'grid').' col-lg-12 col-xs-12 col-sm-12 col-md-12">';
								
								$output .= '<hr class="no-margin-top margin-bottom-15" />';
								
								/**
								 * A filter to display custom HTML/code before the item's title */
								 
								$output .= apply_filters('cspml_before_listing_content', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
								
								/** 
								 * Item's excerpt/content */
								 
								$output .= stripslashes_deep($item_details);
					
								/**
								 * A filter to display custom HTML/code after the item's title */
								 
								$output .= apply_filters('cspml_after_listing_excerpt', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
							$output .= '</div>';											
					
							/**
							 * A filter to display custom HTML/code after the item's excerpt/content container */
							 
							$output .= apply_filters('cspml_after_excerpt_container', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
							
						$output .= '</div>';
						
						$output .= '<div class="clear-both"></div>';
						
					$output .= '</div>';
					
					/**
					 * A filter to display custom HTML/code after the item's HTML */
					 
					$output .= apply_filters('cspml_after_listing_item', '', $post_id, $map_id, 'gridview', $lat.'_'.$lng, $the_permalink);
				
				$output .= '</div>';
									
			$output .= '</div>';
			
			return apply_filters('cspml_grid_view_item', $output, $post_id, $map_id, $lat.'_'.$lng, $the_permalink);						
																
		}
		
		
		/**
		 * Listings pagination
		 *
		 * @since 1.0
		 */
		function cspml_pagination($map_id, $pagination = array(), $paginate_align){
			
			if(count($pagination) > 0){
				
				$output = '<div class="clear-both"></div>';
				
				$output .= '<div class="row no-margin">';
					
					$output .= '<div id="'.$map_id.'" class="cspml_pagination_'.$map_id.' col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:'.$paginate_align.'">'; 
						
						$output .= '<ul>';
						
							$output .= '<div class="cspml_transparent_layer"></div>';
						
							foreach($pagination as $pagination_link)
								$output .= '<li>'.$pagination_link.'</li>';						
							
							$output .= '<div class="clear-both"></div>';
							
						$output .= '</ul>';
						
					$output .= '</div>';
				
				$output .= '</div>';
				
				$output .= '<div class="clear-both"></div>';
				
				/**
				 * [@wp_link_pages] For the sake of Theme Check Plugin 
				 
				$wp_link_pages = wp_link_pages(array('echo' => false));*/
				
			}else $output = '';
					
			return $output;
			
		}
		
		
		/**
		 * This will display an option on top of the listings.
		 * The option bar will contain the following items:
		 * - Widget that displays the number of listings
		 * - A list that allow sorting listings
		 * - Two buttons to switch between different views (List/Grid)
		 * - Open/Close Faceted search button
		 *
		 * @since 1.0
		 */
		function cspml_options_bar($map_id, $nbr_items = 0, $page_id, $current_view, $list_filter, $filter_position){

			/**
			 * Options Bar */
			
			$output = '';
					
			/**
			 * A filter that allows adding extra HTML before the options bar */
			 
			$output .= apply_filters('cspml_before_options_bar', '');

			/**
			 * Options Bar Container */
		 	
			$show_posts_count = $this->cspml_show_posts_count;						
			$show_view_options = $this->cspml_show_view_options;
			$show_sort_option = $this->cspml_show_sort_option;

			$output .= '<div class="cspml_options_bar_'.$map_id.' row">';					
				
				$pull_fs_direction = ($filter_position == 'left') ? 'pull-right' : '';
				
				if($list_filter == 'yes')
					$output .= '<div class="'.$pull_fs_direction.' col-lg-9 col-md-9 col-sm-12 col-xs-12">';
					
					if($list_filter == 'yes')
						$output .= '<div class="row">';
					
						/**
						 * Number of listings */
							
						$post_count_classes = ($show_view_options == 'yes') ? 'col-lg-4 col-md-4 col-sm-10 col-xs-8' : 'col-lg-4 col-md-4 col-sm-12 col-xs-12';
						
						$output .= '<div class="cspml_nbr_items_container '.apply_filters('cspml_post_count_class', $post_count_classes).'">';				
						
							if($show_posts_count == "yes")
								$output .= '<div class="cspml_nbr_items cspm_border_radius cspm_border_shadow text-center">'.$this->cspml_posts_count_clause($nbr_items, $map_id).'</div>';									
					
						$output .= '</div>';
																
						/**
						 * View options */																
						
						if($show_view_options == 'yes'){
							
							$output .= '<div class="cspml_view_options_container '.apply_filters('cspml_view_options_class', 'col-lg-2 col-md-2 col-sm-2 col-xs-4').' pull-right">';
								
								$output .= '<div class="cspml_transparent_layer"></div>';
								
								/**
								 * A filter that allows adding extra HTML before the view options */
								 
								$output .= apply_filters('cspml_before_view_options', '', $page_id);
								
								/**
								 * Switch between the grid view & the list view */
								
								$next_view = ($current_view == 'grid') ? 'list' : 'grid'; 
								
								$output .= '<div class="'.$current_view.'_view view_option '.apply_filters('cspml_view_option_class', 'col-lg-12 col-sm-12 col-xs-12 col-md-12').' cspm_border_right_radius cspm_border_shadow" data-map-id="'.$map_id.'">';
									
									$output .= '<a class="cspml_view_icon push" data-map-id="'.$map_id.'" data-current-view="'.$current_view.'" data-next-view="'.$next_view.'" title="'.sprintf(esc_html__('Změňit výpis výsledků', 'cspml'), $next_view).'">';
									
										$output .= '<img src="'.$this->plugin_url.'img/'.$next_view.'.png" style="height:15px; width:auto;" />';
									
									$output .= '</a>';
									
								$output .= '</div>';		
								
								/**
								 * A filter that allows adding extra HTML after the view options */
																
								$output .= apply_filters('cspml_after_view_options', '', $page_id);
								
							$output .= '</div>';
						
						}
						
						if($show_posts_count == 'yes')
							$output .= '<div class="clear-both visible-sm visible-xs margin-bottom-10"></div>';
						
						/**
						 * Sort by container */

						if($show_posts_count == 'yes'){
							
							$sort_list_classes = 'col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-right';
							
						}else{
							
							if($show_view_options == 'yes')
								$sort_list_classes = 'col-lg-6 col-md-6 col-sm-10 col-xs-8 pull-right';
							else $sort_list_classes = 'col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-right';
							
						}
						
						$output .= '<div class="'.apply_filters('cspml_sort_and_view_options_class', $sort_list_classes).'">';					
							
							$output .= '<div class="cspml_transparent_layer"></div>';
							
							/**
							 * Drop-down sort list */
									
							if($show_sort_option == 'yes')
								$output .= $this->cspml_build_sort_options_list($map_id);

						$output .= '</div>';
						
						$output .= '<div class="clear-both"></div>';
					
					if($list_filter == 'yes')
						$output .= '</div>';
					
				if($list_filter == 'yes')
					$output .= '</div>';
						
				/**
				 * Open/Close button for Faceted Search container */					
				 
				if($list_filter == 'yes'){
					
					$output .= '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">';
						
						$output .= '<div class="visible-sm visible-xs margin-top-10"></div>';
						
						/**
						 * Faceted Search Open/Close button */					
						
						$output .= '<div class="cspml_fs_title cspm_border_radius cspm_border_shadow" data-map-id="'.$map_id.'">';
						
							$output .= '<img src="'.$this->plugin_url.'img/refine.png" title="'.esc_html__('Filtr', 'cspml').'" style="width:15px; height:auto;" />';
						
							$output .= esc_html__('Filtr', 'cspml');
							
							$output .= '<span class="cspml_close_fs cspm_border_right_radius cspm_border_shadow" data-map-id="'.$map_id.'">';
																						
								$output .= '<img src="'.$this->plugin_url.'img/close.png" title="'.esc_html__('Skrytí filtru', 'cspml').'" style="width:15px; height:auto;" />';
							
							$output .= '</span>';
							
						$output .= '</div>';
					
					$output .= '</div>';

				}
				
				$output .= '<div class="clear-both"></div>';
					
			$output .= '</div>';
							
			/**
			 * A filter that allows adding extra HTML after the options bar */
						 
			$output .= apply_filters('cspml_after_options_bar', '');
				
			return $output;
				
		}
		
		/**
		 * Build the sort data HTML list
		 *
		 * @since 1.0
		 */
		function cspml_build_sort_options_list($map_id){
			
			$output = '';
			
			$output .= '<div class="cspml_sort_list_container cspm_border_radius cspm_border_shadow">';//.$this->cspml_sort_option_clause.
			
				$output .= '<img src="'.$this->plugin_url.'img/sort.png" title="'.esc_html__('Řazení výsledků', 'cspml').'" style="height:15px; width:auto;" />';
				
				$output .= '<span class="cspml_sort_val">'.esc_html__('Řazení', 'cspml').'</span>';
				
				$output .= '<span class="cspml_sort_btn cspm_border_right_radius cspm_border_shadow">';
					
					$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" title="'.esc_html__('Řazení výsledků', 'cspml').'" style="height:15px; width:auto;" />';
				
				$output .= '</span>';
				
			$output .= '</div>';
			
			$output .= '<ul class="cspml_sort_list cspm_border_radius cspm_border_shadow">';
				
				/** 
				 * Display the sort list */
				 
				$output .= $this->cspml_build_sort_data($map_id, 'list', NULL);
				
			$output .= '</ul>';
			
			return $output;
			
		}
		
		
		/**
		 * Create the list of the sort data (Default data)
		 *
		 * @since 1.0
		 */
		function cspml_build_sort_data($map_id, $type, $post_id = NULL){
			
			/**
			 * Usage Example:
			 *
			 * $sort_options_array = array(
			 		'default|Default|init' => 'Default',
					'data-date-created|Date (Newest first)|desc' => 'Date (Newest first)',
					'data-date-created|Date (Oldest first)|asc' => 'Date (Oldest first)',
					'data-title|A -> Z|asc' => 'A -> Z',
					'data-title|Z -> A|desc' => 'Z -> A'
			   ); */
	
			$output = '';
				
			/**
			 * The default sort by data */
			 
			foreach($this->cspml_sort_options as $sort_option){
			
				$explode_sort_option = explode("|", $sort_option);
				
				if(isset($explode_sort_option[0]) && isset($explode_sort_option[1]) && isset($explode_sort_option[2])){
					
					/**
					 * Build the list */
					 
					if($type == 'list'){
						
						$output .= '<li class="sort push" data-map-id="'.$map_id.'" ';
						
							$output .= 'data-sort="'.$explode_sort_option[0].'"';
						
							if($explode_sort_option[2] != 'init')
								$output .= ' data-order="'.$explode_sort_option[2].'"';
								
							else $output .= ' data-order="desc"';
									
						$output .= '>'.$explode_sort_option[1].'</li>';
					
					/**
					 * Build the data attributes */
					 
					}elseif($type == 'data' && $post_id != NULL){
						
						/**
						 * data-title attribute/value */
						 
						if($explode_sort_option[0] == 'data-title'){
							
							$item_title = $this->cspml_items_title($post_id, $this->cspml_listings_title);
							
							$output .= ' data-title="'.stripslashes_deep($item_title).'"';
						
						/**
						 * data-date-created attribute/value */
						 
						}elseif($explode_sort_option[0] == 'data-date-created'){
							
							$output .= ' data-date-created="'.$post_id.'"';
							
						}
					
					}
					
				}
				
			}
			
			/**
			 * The custom sort data (Custom fields) */
			
			if(!empty($this->cspml_custom_sort_options)){
				
				if($type == 'list'){
					
					$output .= $this->cspml_build_custom_sort_data($map_id, $type, NULL, $this->cspml_custom_sort_options);
							
				}elseif($type == 'data' && $post_id != NULL){
					
					$output .= $this->cspml_build_custom_sort_data($map_id, $type, $post_id, $this->cspml_custom_sort_options);
		
				}
					
			}
			
			return $output;
			
		}
		
		
		/**
		 * Create the list of the custom sort data (Custom fields)
		 *
		 * @since 1.0
		 */
		function cspml_build_custom_sort_data($map_id, $type, $post_id = NULL, $sort_by_structure){
			
			/**
			 * Usage Example:
			 *
			 * $sort_by_structure = array(
				'cspm_price' => array(
					'tag_sort_options_name' => 'cspm_price', // Custom field name
					'tag_sort_options_label' => 'Price', // Custom field Label
					'tag_sort_options_order' => array('desc', 'asc'), // Possible values (desc/asc)
					'tag_sort_options_desc_suffix' => '(Highest first)', // The suffix to add to end of the label for the descending order. e.g. "(Highest first)"
					'tag_sort_options_asc_suffix' => '(Lowest first)', // The suffix to add to end of the label for the ascending order. e.g. "(Lowest first)"
					'tag_sort_options_visibilty' => 'yes', // Possible values (yes/no)
				)
			  ); */

			$output = '';
			
			if(!empty($sort_by_structure)){
						
				/**
				 * Get custom sort by structure */
				 
				$sort_data_attrs = (array) json_decode($sort_by_structure);
							
				/**
				 * Loop throught sort by data */
				 
				foreach($sort_data_attrs as $sort_data_key => $sort_data_value){
					
					/**
					 * Get the custom field name */
					 
					$custom_field_name = $sort_data_key;
					
					/**
					 * Convert Object to array */
					 
					$sort_data_value = (array) $sort_data_value;
										
					$sort_options_name = $this->cspml_setting_exists('tag_sort_options_name', $sort_data_value);
					$sort_options_label = $this->cspml_setting_exists('tag_sort_options_label', $sort_data_value);
					$sort_options_order = $this->cspml_setting_exists('tag_sort_options_order', $sort_data_value, array('desc', 'asc'));
					$sort_options_desc_suffix = $this->cspml_setting_exists('tag_sort_options_desc_suffix', $sort_data_value);
					$sort_options_asc_suffix = $this->cspml_setting_exists('tag_sort_options_asc_suffix', $sort_data_value);
					$sort_options_visibilty = $this->cspml_setting_exists('tag_sort_options_visibilty', $sort_data_value, 'yes');
	
					/**
					 * Loop throught the sort order and create the sort list */
					 
					foreach($sort_options_order as $sort_order){
						
						if($type == 'list'){
							
							$label_suffix = ($sort_order == 'desc') ? $sort_options_desc_suffix : $sort_options_asc_suffix;
								
							$output .= '<li class="sort push" data-map-id="'.$map_id.'" ';
							
								$output .= 'data-sort="'.$sort_options_name.'"';
							
								$output .= ' data-order="'.$sort_order.'"';
										
							$output .= '>'.$sort_options_label.' '.$label_suffix.'</li>';
					
						}elseif($type == 'data' && $post_id != NULL){
							
							$post_meta = $sort_options_name;
							
							$meta_data = get_post_meta($post_id, $post_meta, true);
							
							if(!empty($meta_data))
								$output .= ' data-'.$post_meta.'="'.$meta_data.'"';
							
							else $output .= ' data-'.$post_meta.'="'.$post_id.'"';
					
						}
						
					}
								
				}			
				
			}
			
			return $output;
			
		}
		
		
		/**
		 * Preapare the sort order Query when a sort by request is sent via ajax
		 *
		 * @since 1.0
		 */
		function cspml_set_posts_order($sort_data, $sort_order){
			
			$output = array();
			
			if(!empty($sort_data) && !empty($sort_order)){
				
				/**
				 * Check the value of the $sort_data
				 * If the data is a title sort request */
				 
				if($sort_data == "data-title"){
					
					$orderby = array('orderby' => 'title');
				
				/**
				 * If the data is a date sort request */
				 
				}elseif($sort_data == "data-date-created"){
					
					$orderby = array('orderby' => 'date');
				
				/**
				 * If the data is a default sort request */
				 
				}elseif($sort_data == "default"){
					
					$orderby = array('orderby' => 'post__in');
				
				/**
				 * If the data is a custom field request */
				 
				}else{
					
					$orderby = array('meta_key' => $sort_data, 'orderby' => 'meta_value');			   
					
				}
				
				$output = $order = $orderby + array('order' => strtoupper($sort_order));
				
			}
			
			return $output;
			
		}
		
		
		/**
		 * show the number of listings 
		 *
		 * @since 1.0
		 */
		function cspml_posts_count_clause($count, $map_id){
			
			return str_replace('[posts_count]', '<span class="cspml_the_count_'.$map_id.'">'.$count.'</span>', esc_attr($this->cspml_posts_count_clause));
					
		}
		
		
		/**
		 * Parse item custom title
		 *
		 * @since 1.0
		 */
		function cspml_items_title($post_id, $title, $click_title = false){
			
			/**
			 * Custom title structure */
			 
			$post_meta = esc_attr($title);
			
			$permalink = get_permalink($post_id);
			
			/**
			 * Init vars */
			 
			$items_title = '';		
			$items_title_lenght = 0;
			
			/**
			 * If no custom title is set ...
			 * ... Call item original title */
			 
			if(empty($post_meta)){						
				
				$items_title = get_the_title($post_id);
				
			/**
			 * If custom title is set ... */
			 
			}else{
				
				/**
				 * ... Get post metas from custom title structure */
				 
				$explode_post_meta = explode('][', $post_meta);
				
				/**
				 * Loop throught post metas */
				 
				foreach($explode_post_meta as $single_post_meta){
					
					/**
					 * Clean post meta name */
					 
					$single_post_meta = str_replace(array('[', ']'), '', $single_post_meta);
					
					/**
					 * Get the first two letters from post meta name */
					 
					$check_string = substr($single_post_meta, 0, 2);
					$check_title = substr($single_post_meta, 0, 5);
					
					/**
					 * Title case */
					 
					if($check_title == 'title'){
						
						$items_title .= get_the_title($post_id);
						
					/**
					 * Separator case */
					 
					}elseif($check_string === 's='){
						
						/**
						 * Add separator to title */
						 
						$items_title .= str_replace('s=', '', $single_post_meta);
					
					/**
					 * Lenght case */
					 
					}elseif($check_string === 'l='){
						
						/**
						 * Define title lenght */
						 
						$items_title_lenght = str_replace('l=', '', $single_post_meta);
					
					/**
					 * Empty space case */
					 
					}elseif($single_post_meta == '-'){
						
						/**
						 * Add space to title */
						 
						$items_title .= ' ';
					
					/**
					 * Taxonomy case */
					 
					}elseif($check_string === 'x='){
						
						/**
						 * Add taxonomy term(s) */
						 
						$taxonomy = str_replace('x=', '', $single_post_meta);
						$items_title .= implode(', ', wp_get_post_terms($post_id, $taxonomy, array("fields" => "names")));
						
					/**
					 * Post metas case */
					 
					}else{
						
						/**
						 * Add post meta value to title */
						 
						$items_title .= get_post_meta($post_id, $single_post_meta, true);
							
					}
					
				}
				
				/**
				 * If custom title is empty (Maybe someone will type something by error), call original title */
				 
				if(empty($items_title)) $items_title = get_the_title($post_id);
				
			}
			
			/**
			 * Show title as title lenght is defined */
			 
			if($items_title_lenght > 0) $items_title = substr($items_title, 0, $items_title_lenght);
			
			if($click_title)
				return apply_filters('cspml_item_title', '<a href="'.$permalink.'" title="'.addslashes_gpc($items_title).'">'.addslashes_gpc($items_title).'</a>', $post_id);
			else return apply_filters('cspml_item_title', addslashes_gpc($items_title), $post_id);
			
		}
		
		
		/**
		 * Parse item custom details 
		 *
		 * @since 1.0
		 */		 
		function cspml_items_details($post_id, $details){			
			
			/**
			 * Custom details structure */
			 
			$post_meta = esc_attr($details);		
			
			/**
			 * Init vars */
			 
			$items_details = '';
			$items_title_lenght = 0;
			
			/**
			 * If new structure is set ... */
			 
			if(!empty($post_meta)){
				
				/**
				 * ... Get post metas from custom details structure */
				 
				$explode_post_meta = explode('][', $post_meta);
				
				/**
				 * Loop throught post metas */
				 
				foreach($explode_post_meta as $single_post_meta){
					
					/**
					 * Clean post meta name */
					 
					$single_post_meta = str_replace(array('[', ']'), '', $single_post_meta);
								
					/**
					 * Get the first two letters from post meta name */
					 
					$check_string = substr($single_post_meta, 0, 2);
					$check_taxonomy = substr($single_post_meta, 0, 4);
					$check_content = substr($single_post_meta, 0, 7);
					
					/**
					 * Taxonomy case */
					 
					if(!empty($check_taxonomy) && $check_taxonomy == 'tax='){
						
						/**
						 * Add taxonomy term(s) */
						 
						$taxonomy = str_replace('tax=', '', $single_post_meta);
						$items_details .= implode(', ', wp_get_post_terms($post_id, $taxonomy, array("fields" => "names")));
						
					/**
					 * The content */
					 
					}elseif(!empty($check_content) && $check_content == 'content'){
						
						$explode_content = explode(';', str_replace(' ', '', $single_post_meta));
						
						/**
						 * Get original post details */
						 
						$post_record = get_post($post_id, ARRAY_A);
						
						/**
						 * Post content */
						 
						$post_content = trim(preg_replace('/\s+/', ' ', $post_record['post_content']));
						
						/**
						 * Post excerpt */
						 
						$post_excerpt = trim(preg_replace('/\s+/', ' ', $post_record['post_excerpt']));
						
						/**
						 * Excerpt is recommended */
						 
						$the_content = (!empty($post_excerpt)) ? $post_excerpt : $post_content;
				
						/**
						 * Show excerpt/content as details lenght is defined */
						 
						if(isset($explode_content[1]) && $explode_content[1] > 0) $items_details .= substr($the_content, 0, $explode_content[1]).'&hellip;';
									
					/**
					 * Separator case */
					 
					}elseif(!empty($check_string) && $check_string == 's='){
						
						/**
						 * Add separator to details */
						 
						$separator = str_replace('s=', '', $single_post_meta);
						
						$separator == 'br' ? $items_details .= '<br />' : $items_details .= $separator;
						
					/**
					 * Meta post title OR Label case */
					 
					}elseif(!empty($check_string) && $check_string == 't='){
						
						/**
						 * Add label to details */
						 
						$items_details .= str_replace('t=', '', $single_post_meta);
						
					/**
					 * Lenght case */
					 
					}elseif(!empty($check_string) && $check_string == 'l='){
						
						/**
						 * Define details lenght */
						 
						$items_details_lenght = str_replace('l=', '', $single_post_meta);
						
					/**
					 * Empty space case */
					 
					}elseif($single_post_meta == '-'){
						
						/**
						 * Add space to details */
						 
						$items_details .= ' ';
					
					/**
					 * Post metas case */
					 
					}else{
	
						/**
						 * Add post metas to details */
						 
						$items_details .= get_post_meta($post_id, $single_post_meta, true);
							
					}
					
				}						
				
			}
			
			/**
			 * If no custom details structure is set ... */
			 
			if(empty($post_meta) || empty($items_details)){
				
				/**
				 * Get original post details */
				 
				$post_record = get_post($post_id, ARRAY_A);
				
				/**
				 * Post content */
				 
				$post_content = trim(preg_replace('/\s+/', ' ', $post_record['post_content']));
				
				/**
				 * Post excerpt */
				 
				$post_excerpt = trim(preg_replace('/\s+/', ' ', $post_record['post_excerpt']));
				
				/**
				 * Excerpt is recommended */
				 
				$items_details = (!empty($post_excerpt)) ? $post_excerpt : $post_content;
				
				/**
				 * Show excerpt/content as details lenght is defined */
				 
				if($items_details_lenght > 0){
					
					/**
					 * Remove the last word from the content/excerpt ...
					 * ... as a proof against foreign characters encoded ...
					 * ... when the last word of the content is cut off */
					 
					$items_details = substr($items_details, 0, $items_details_lenght);
					$items_details = explode(' ', $items_details);
					$last_word = array_pop($items_details);
					$items_details = implode(' ', $items_details).apply_filters('cspml_description_more', '&hellip;');
                    
					
				}
				
			}
			
			return apply_filters('cspml_item_description', addslashes_gpc($items_details), $post_id);
			
		}
		
		
		/**
		 * Create the "Listings faceted search form"
		 *
		 * @since 1.0
		 */
		function cspml_listings_faceted_search_form($map_id, $post_type, $post_ids = array(), $list_filter){
			
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
				
			$output = '';

			if($list_filter == 'yes'){
				
				$output .= '<div class="cspml_transparent_layer"></div>';	
				
				/**
				 * A filter that allows adding extra HTML before the filter form */
				 
				$output .= apply_filters('cspml_before_filter_form', '');

				$output .= '<form action="" method="post" id="cspml_listings_filter_form" class="cspml_filter_form col-lg-12 col-md-12 col-sm-12 col-xs-12 cspm_border_radius cspm_border_shadow no-padding cspm_animated fadeIn" data-map-id="'.$map_id.'">';
				
					/**
					 * Loop throught taxonomies */
					 
					if(count($this->cspml_taxonomies) > 0){
						
						foreach($this->cspml_taxonomies as $taxonomy_data){
							
							$taxonomy_data = (array) $taxonomy_data;
							
							if(count($taxonomy_data) > 0){
								
								$taxonomy_label = $this->cspml_setting_exists('tag_taxonomy_label', $taxonomy_data);
								$taxonomy_name  = $this->cspml_setting_exists('tag_taxonomy_name', $taxonomy_data);
								$taxonomy_visbility = $this->cspml_setting_exists('tag_taxonomy_visibilty', $taxonomy_data, 'yes');
								$taxonomy_exists = taxonomy_exists($taxonomy_name);
													
								if(!empty($taxonomy_label) && !empty($taxonomy_name) && $taxonomy_visbility == "yes" && $taxonomy_exists){
									
									/**
									 * Hide empty terms */
									 
									$taxonomy_hide_empty = $this->cspml_setting_exists('tag_taxonomy_hide_empty', $taxonomy_data, "yes");
										$hide_empty = ($taxonomy_hide_empty == 'yes') ? true : false;									
										
									/**
									 * Terms to exclude */
									 
									$exclude_term_ids = str_replace(' ', '', $this->cspml_setting_exists('tag_taxonomy_exclude_terms', $taxonomy_data));
									
									/**
									 * Get taxonomy terms */
									 
									$terms = (array) get_terms($taxonomy_name, array("orderby" => "name",
																					 "exclude" => $exclude_term_ids, 
																					 "hide_empty" => $hide_empty));
									
									/**
									 * Show the field only when terms are more than 0 */
									 
									if(count($terms) > 0){																
										
										$search_all_option = $this->cspml_setting_exists('tag_taxonomy_search_all_option', $taxonomy_data, 'no');
										$search_all_text = $this->cspml_setting_exists('tag_taxonomy_search_all_text', $taxonomy_data, 'All');
										$show_count = $this->cspml_setting_exists('tag_taxonomy_show_count', $taxonomy_data, 'no');
										$symbol = $this->cspml_setting_exists('tag_taxonomy_symbol', $taxonomy_data);
										$symbol_position = $this->cspml_setting_exists('tag_taxonomy_symbol_position', $taxonomy_data, 'before');
												
										$field_atts = array(
														'field_name' => $taxonomy_name,
														'field_label' => $taxonomy_label,
														'field_options' => $terms,
														'symbol' => $symbol,
														'symbol_position' => $symbol_position,
														'search_all_option' => $search_all_option,
														'search_all_text' => $search_all_text,
														'show_count' => $show_count
													);
					
										$output .= '<div class="cspml_fs_item_container col-lg-12 col-xs-12 col-sm-12 col-md-12">';
											
											$display_type = $this->cspml_setting_exists('tag_taxonomy_display_type', $taxonomy_data);
											
											if(!empty($display_type))
												$output .= $this->cspml_faceted_search_fields($display_type, $field_atts, true, $post_type, $post_ids);		
											
										$output .= '</div>';
										
									}
									
								}
	
							}
							
						}
										
					}

					/**
					 * Loop throught custom fields */
					 
					if(count($this->cspml_custom_fields) > 0){

						foreach($this->cspml_custom_fields as $custom_field_data){
							
							$custom_field_data = (array) $custom_field_data;

							if(count($custom_field_data) > 0){
								
								$custom_field_label = $this->cspml_setting_exists('tag_custom_field_label', $custom_field_data);
								$custom_field_name  = $this->cspml_setting_exists('tag_custom_field_name', $custom_field_data);
								$custom_field_visbility = $this->cspml_setting_exists('tag_custom_field_visibilty', $custom_field_data, 'yes');
								
								if(!empty($custom_field_label) && !empty($custom_field_name) && $custom_field_visbility == "yes"){
								
									/**
									 * Get All Post Meta And Count The Number Of Each One */
									 
									$custom_field_values = $this->cspml_get_meta_values($custom_field_name, $post_type);

									$custom_field_options = array_unique($custom_field_values);
								
									/**
									 * Show options only when custom_field_options are more than 0 */
									 
									if(count($custom_field_options) > 0){
										
										$term_ids_array = array();
										
										$search_all_option = $this->cspml_setting_exists('tag_custom_field_search_all_option', $custom_field_data, 'no');
										$search_all_text = $this->cspml_setting_exists('tag_custom_field_search_all_text', $custom_field_data, 'All');
										$show_count = $this->cspml_setting_exists('tag_custom_field_show_count', $custom_field_data, 'no');
										$symbol = $this->cspml_setting_exists('tag_custom_field_symbol', $custom_field_data);
										$symbol_position = $this->cspml_setting_exists('tag_custom_field_symbol_position', $custom_field_data, 'before');
												
										$field_atts = array(
														'field_name' => $custom_field_name,
														'field_label' => $custom_field_label,
														'field_options' => $custom_field_options,
														'symbol' => $symbol,
														'symbol_position' => $symbol_position,
														'search_all_option' => $search_all_option,
														'search_all_text' => $search_all_text,
														'show_count' => $show_count,
													);
										
										$output .= '<div class="cspml_fs_item_container col-lg-12 col-xs-12 col-sm-12 col-md-12">';
											
											$display_type = $this->cspml_setting_exists('tag_custom_field_display_type', $custom_field_data);
											
											if(!empty($display_type))
												$output .= $this->cspml_faceted_search_fields($display_type, $field_atts, false, $post_type, $post_ids);		
										
										$output .= '</div>';
										
									}
									
								}
							
							}
							
						}
										
					}
					
					$output .= '<div class="clearfix"></div>';
					
					$output .= '<div class="row row-no-margin cspm_filter_btns">';	
						
						$output .= '<a class="cspml_reset_lsitings_filter cspml_btn cspm_border_shadow col-lg-4 col-md-4 col-sm-3 col-xs-3" title="'.esc_html__('Vyresetovat hodnoty filtru', 'cspml').'" data-request-type="reset" data-map-id="'.$map_id.'" data-display-location="listings">';
							$output .= '<img src="'.$this->plugin_url.'img/refresh.png" class="no-margin-right" />';
						$output .= '</a>';
									
						$output .= '<a class="cspml_submit_listings_filter cspml_btn cspm_border_shadow col-lg-8 col-md-8 col-sm-9 col-xs-9" title="'.esc_html__('Filtrovat', 'cspml').'" data-request-type="filter" data-map-id="'.$map_id.'" data-display-location="listings">';
							$output .= '<img src="'.$this->plugin_url.'img/filter.png" />';
							$output .= $this->cspml_filter_btn_text;
						$output .= '</a>';
					
					$output .= '</div>';
					
					$output .= '<div class="clear-both"></div>';
					
				$output .= '</form>';
				
				/**
				 * A filter that allows adding extra HTML after the filter form */
				 
				$output .= apply_filters('cspml_after_filter_form', '');

				$output .= '<div class="clear-both visible-sm visible-xs"></div>';
				
			}
			
			return $output;
		
		}
		
		
		/**
		 * Prepare the fields for the filter form 
		 *
		 * @since 1.0
		 */
		function cspml_faceted_search_fields($field_type, $field_atts, $is_terms = false, $post_type, $post_ids = array(), $filter_container = "listings"){
			
			$defaults = array(
				'field_name' => '',
				'field_label' => '',
				'field_options' => array(),
				'symbol' => '',
				'symbol_position' => 'text',
				'search_all_option' => 'false',
				'search_all_text' => 'All',
				'show_count' => '',
				'operator_param' => '',
				'default_value' => '',
			);
			
			extract( wp_parse_args( $field_atts, $defaults ) );
				
			$option_ids_array = array();
			
			$output = '';				
			
			$i = 1;
			
			$count_options = count($field_options);
				  
			/** 
			 * Checkbox field */
			 
			if($field_type == "checkbox"){
				
				if($filter_container != "map"){
															
					$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
						$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
						$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
							$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
						$output .= '</span>';
					$output .= '</div>';
					
				}else{
					
					$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
						$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
					$output .= '</div>';
					
				}
					
				$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
					
					$filter_type = ($is_terms) ? "taxonomy" : "custom_field";
					
					$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
					
					/**
					 * List of options */
					 
					foreach($field_options as $option){
						
						if($is_terms){
															
							$label = $option->name;
							$name  = $option->slug;
							$id    = $option->term_id;
							$value = $option->term_id;
							
							$filter_type = "taxonomy";
							
							if($show_count == "yes")																									
								$count_posts = $this->cspml_count_term_posts($field_name, $id, $post_type, $post_ids);
							
						}else{
							
							$label = $name = $id = $value = $option;
							
							if($show_count == "yes"){		
								$count_posts = $this->cspml_count_custom_field_posts($post_type, $field_name);
								$count_posts = $count_posts[$name];
							}
							
							$filter_type = "custom_field";
						
						}
						
							
						$option_ids_array[] = $id;
					
													 
						$output .= '<div class="cspml_input_container">';
						$output .= '<input type="checkbox" name="'.$field_name.'[]" id="'.$filter_container.'_'.$field_name.$id.'" data-filter-type="'.$filter_type.'" value="'.$value.'" />';
						$output .= '<label for="'.$filter_container.'_'.$field_name.$id.'" id="'.$filter_container.'_'.$field_name.$id.'">';
							$output .= ($symbol_position == "before") ? $symbol : ''; 
							$output .= ($show_count == "yes") ? $label.' ('.$count_posts.')' : $label;
							$output .= ($symbol_position == "after") ? $symbol : '';
						$output .= '</label>';	
						$output .= '</div>';				
					
						/**
						 * Show all option */
						 
						if($search_all_option == "yes" && $i++ == $count_options){
							$output .= '<div class="cspml_input_container">';
							$output .= '<input type="checkbox" name="'.$field_name.'[]" class="cspml_show_all" id="show_all_'.$field_name.'" data-filter-type="'.$filter_type.'" value="'.implode(',', $option_ids_array).'" />';
							$output .= '<label for="show_all_'.$field_name.'" id="show_all_'.$field_name.'">'.$search_all_text.'</label>';
							$output .= '</div>';
						}
		
					}
				
				$output .= '</div>';
			
			/**
			 * Radio field */
			 
			}elseif($field_type == "radio"){
					
				if($filter_container != "map"){
															
					$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
						$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
						$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
							$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
						$output .= '</span>';
					$output .= '</div>';
					
				}else{
					
					$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
						$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
					$output .= '</div>';
					
				}
					
				$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
					
					$filter_type = ($is_terms) ? "taxonomy" : "custom_field";
					
					$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
					
					/**
					 * List of options */
					 
					foreach($field_options as $option){
						
						if($is_terms){
															
							$label = $option->name;
							$name  = $option->slug;
							$id    = $option->term_id;
							$value = $option->term_id;
							
							$filter_type = "taxonomy";
							
							if($show_count == "yes")																									
								$count_posts = $this->cspml_count_term_posts($field_name, $id, $post_type, $post_ids);
							
						}else{
							
							$label = $name = $id = $value = $option;
							
							if($show_count == "yes"){		
								$count_posts = $this->cspml_count_custom_field_posts($post_type, $field_name);
								$count_posts = $count_posts[$name];
							}
							
							$filter_type = "custom_field";
						
						}
						
						$option_ids_array[] = $id;
																																					
						$output .= '<div class="cspml_input_container">';
						$output .= '<input type="radio" name="'.$field_name.'" id="'.$filter_container.'_'.$field_name.$id.'" data-filter-type="'.$filter_type.'" value="'.$value.'" />';
						$output .= '<label for="'.$filter_container.'_'.$field_name.$id.'" id="'.$filter_container.'_'.$field_name.$id.'">';
							$output .= ($symbol_position == "before") ? $symbol : ''; 
							$output .= ($show_count == "yes") ? $label.' ('.$count_posts.')' : $label;
							$output .= ($symbol_position == "after") ? $symbol : '';
						$output .= '</label>';
						$output .= '</div>';	
						
							
						if($i++ == $count_options){
							
							/**
							 * Show all option */
							 
							if($search_all_option == "yes"){
								$output .= '<div class="cspml_input_container">';
								$output .= '<input type="radio" name="'.$field_name.'" class="cspml_show_all" id="show_all_'.$field_name.'" data-filter-type="'.$filter_type.'" value="'.implode(',', $option_ids_array).'" />';
								$output .= '<label for="show_all_'.$field_name.'" id="show_all_'.$field_name.'">'.$search_all_text.'</label>';
								$output .= '</div>';
							}
								
						}
						
					}
						
				$output .= '</div>';					
				
			/**
			 * Select field */
			 
			}elseif($field_type == "select"){
					
				if($is_terms)
					$filter_type = "taxonomy";			
				else
					$filter_type = "custom_field";
				
				$select_extra_class = ($search_all_option == "yes") ? ' cspml_show_all' : '';
					$filter_type;
				
				if($filter_container != "map"){
															
					$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
						$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
						$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
							$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
						$output .= '</span>';
					$output .= '</div>';
					
				}else{
					
					$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
						$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
					$output .= '</div>';
					
				}
					
				$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
					
					$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
										
					$output .= '<div class="cspml_input_container">';
					$output .= '<select name="'.$field_name.'" id="'.$field_name.'" data-filter-type="'.$filter_type.'" class="cspml_fs_selectize'.$select_extra_class.'">';
						$output .= '<option value=""></option>';
						
						/**
						 * List of options */
						 
						foreach($field_options as $option){
							
							if($is_terms){
																
								$label = $option->name;
								$name  = $option->slug;
								$id    = $option->term_id;
								$value = $option->term_id;
								
								$filter_type = "taxonomy";
								
								if($show_count == "yes")																									
									$count_posts = $this->cspml_count_term_posts($field_name, $id, $post_type, $post_ids);
								
							}else{
								
								$label = $name = $id = $value = $option;
								
								if($show_count == "yes"){		
									$count_posts = $this->cspml_count_custom_field_posts($post_type, $field_name);
									$count_posts = $count_posts[$name];
								}
								
								$filter_type = "custom_field";
							
							}
							
							$option_ids_array[] = $id;
																																
							$output .= '<option value="'.$value.'">';
								$output .= ($symbol_position == "before") ? $symbol : ''; 
								$output .= ($show_count == "yes") ? $label.' ('.$count_posts.')' : $label;
								$output .= ($symbol_position == "after") ? $symbol : '';
							$output .= '</option>';											
						
							/**
							 * Show all option */
							 
							if($search_all_option == "yes" && $i++ == $count_options){
								$output .= '<option class="cspml_show_all" value="'.implode(',', $option_ids_array).'">';
									$output .= ($symbol_position == "before") ? $symbol : ''; 
									$output .= $search_all_text;
									$output .= ($symbol_position == "after") ? $symbol : '';
								$output .= '</option>';											
							}
		
						}
				
					$output .= '</select>';
					$output .= '</div>';
				
				$output .= '</div>';		
				
			/**
			 * Number field */
			 
			}elseif($field_type == "number"){
					
				if($is_terms)
					$filter_type = "taxonomy";			
				else
					$filter_type = "custom_field";
				
				$field_options = str_replace($this->cspml_exclude_from_field_options, array('', ''), $field_options);
				$implode_options = implode('', $field_options);
				
				if(is_numeric($implode_options)){
						
					$min_value = min($field_options);
					$max_value = max($field_options) == $min_value ? $min_value + 10 : max($field_options);
					
					if($filter_container != "map"){
																
						$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
							$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
							$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
								$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
							$output .= '</span>';
						$output .= '</div>';
						
					}else{
						
						$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
							$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
						$output .= '</div>';
						
					}
					
					$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
						
						$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
					
						$output .= '<div class="cspml_input_container">';
						
							$output .= '<div data-trigger="spinner" class="input_spinner cspml_fs_number_field">';
								
								$output .= '<div class="cspml_spinner_btn"><a href="javascript:;" data-spin="down"><span>&nbsp;</span></a></div>';
								
								$output .= '<div class="cspml_spinner_symbol">';
									$output .= ($symbol_position == "before" && !empty($symbol)) ? $symbol : '&nbsp;';
								$output .= '</div>';
								
								$output .= '<div class="cspml_input_container">';
								
									$output .= '<input type="text" name="'.$field_name.'" value="'.$min_value.'" data-filter-type="'.$filter_type.'" data-rule="quantity" data-min="'.$min_value.'" data-max="'.$max_value.'" />';
								
								$output .= '</div>';
								
								$output .= '<div class="cspml_spinner_symbol">';
									$output .= ($symbol_position == "after" && !empty($symbol)) ? $symbol : '&nbsp;';
								$output .= '</div>';
								
								$output .= '<div class="cspml_spinner_btn"><a href="javascript:;" data-spin="up"><span>&nbsp;</span></a></div>';
								
							$output .= '</div>';
							
							$output .= '<div class="clear-both"></div>';
													
							$output .= '<div class="cspml_reset_spinner"><a data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">'.__('Any', 'cspml').'</a></div>';	
							
						$output .= '</div>';
						
					$output .= '</div>';	
				
				}
											
			/**
			 * Double Range slider */
			 
			}elseif($field_type == "double_slider"){
								
				if($is_terms)
					$filter_type = "taxonomy";			
				else
					$filter_type = "custom_field";
				
				$field_options = str_replace($this->cspml_exclude_from_field_options, array('', ''), $field_options);
				$implode_options = implode('', $field_options);
				
				if(is_numeric($implode_options)){

					$min_value = (min($field_options)-10 < 1) ? 1 : min($field_options)-10;
					$max_value = max($field_options) == $min_value ? $min_value + 10 : max($field_options)+10;
					
					if($filter_container != "map"){
																
						$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
							$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
							$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
								$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
							$output .= '</span>';
						$output .= '</div>';
						
					}else{
						
						$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
							$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
						$output .= '</div>';
						
					}
					
					$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
						
						if($symbol_position == "before")
							$prefix = 'data-prefix="'.$symbol.'"';				
						elseif($symbol_position == "after")
							$prefix = 'data-postfix="'.$symbol.'"';
						else $prefix = ''; 
					
						$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
					
						$output .= '<div class="cspml_input_container">';
							$output .= '<input type="text" name="'.$field_name.'" class="cspml_fs_slider_range" data-min="'.$min_value.'" data-max="'.$max_value.'" data-values-separator=" to " data-prettify-separator="," data-filter-type="'.$filter_type.'" data-type="double" '.$prefix.' />';
						$output .= '</div>';
						
					$output .= '</div>';	
					
				}
											
			/**
			 * Single slider */
			 
			}elseif($field_type == "single_slider"){
					
				if($is_terms)
					$filter_type = "taxonomy";			
				else
					$filter_type = "custom_field";
				
				$field_options = str_replace($this->cspml_exclude_from_field_options, array('', ''), $field_options);
				$implode_options = implode('', $field_options);
				
				if(is_numeric($implode_options)){
								
					$min_value = (min($field_options)-10 < 1) ? 1 : min($field_options)-10;
					$max_value = max($field_options) == $min_value ? $min_value + 10 : max($field_options)+10;
					
					if($filter_container != "map"){
																
						$output .= '<div class="cspml_fs_label row" for="'.$field_name.'" id="'.$field_name.'">';
							$output .= '<span class="cspml_label_text">'.$field_label.'</span>';
							$output .= '<span class="cspml_toggle_btn" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
								$output .= '<img src="'.$this->plugin_url.'img/arrow-down.png" style="height:15px; width:auto;" />';
							$output .= '</span>';
						$output .= '</div>';
						
					}else{
						
						$output .= '<div class="cspml_mfs_label row" for="'.$field_name.'" id="'.$field_name.'">';										
							$output .= '<span class="cspml_label_text col-lg-10 col-sm-10 col-xs-11 col-md-10">'.$field_label.'</span>';
						$output .= '</div>';
						
					}
					
					$output .= '<div class="cspml_fs_options_list" data-field-name="'.$field_name.'" data-display-location="'.$filter_container.'">';
					
						if($symbol_position == "before")
							$prefix = 'data-prefix="'.$symbol.'"';				
						elseif($symbol_position == "after")
							$prefix = 'data-postfix="'.$symbol.'"';
						else $prefix = ''; 
					
						$output .= '<input type="hidden" name="'.$field_name.'_filter_type" value="'.$filter_type.'" />';
							
						$output .= '<div class="cspml_input_container">';
							$output .= '<input type="text" name="'.$field_name.'" class="cspml_fs_slider_range" value="'.$min_value.'" data-min="'.$min_value.'" data-max="'.$max_value.'" data-filter-type="'.$filter_type.'" data-type="single" '.$prefix.' />';
						$output .= '</div>';	
						
					$output .= '</div>';	
				
				}
											
			}
			
			return $output;
			
		}
		
		
		/**
		 * The filter form Query
		 *
		 * @since 1.0
		 */
		function cspml_faceted_search_query(){
			
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
			
			if(!isset($_POST['filter_args']) && !isset($_POST['map_id'])){
				$post_ids = array();
				print_r(json_encode($post_ids));
				die();
			}
		
			$post_ids = $tax_query = $custom_fields = array();
			
			/**
			 * [@filter_args] The selected fields and their values */
			 
			$filter_args = $_POST['filter_args'];
		
			$map_id = esc_attr($_POST['map_id']);
			
			/**
			 * [@post_type] Post Type */
			 
			$post_type = esc_attr($_POST['post_type']);
			
			/**
			 * [@post_ids] Post IDs of all post to show */
			 
			$post_ids = $_POST['post_ids'];
			
			/**
			 * [@init_post_ids] Post IDs of all post we've started with before doing any filter */
			 
			$init_post_ids = !empty($_POST['init_post_ids']) ? explode(',', esc_attr($_POST['init_post_ids'])) : array();			
												
			/**
			 * Get The ID Of The Page Where The Shortcode was Executed */
			 
			$shortcode_page_id = esc_attr($_POST['shortcode_page_id']);
			$page_template = esc_attr($_POST['page_template']);
			$template_tax_query  = esc_attr($_POST['template_tax_query']);
			
			$post__in = array();
			
			/**
			 * Usefull for re-executing hooks after AJAX load */
			 
			do_action('cspml_before_faceted_search_query', $shortcode_page_id, $page_template, $template_tax_query);

			if(count($filter_args) > 0){
				
				/**
				 * Get the post_ids that were the result of the search form in the map ("Progress Map Search").
				 * That way, the faceted search query will take in concediration that it should only search within a given post_ids */
				 
				if(isset($_SESSION['cspml_posts_in_search'][$map_id]['filtering']) && $_SESSION['cspml_posts_in_search'][$map_id]['filtering'] == true && isset($_SESSION['cspml_posts_in_search'][$map_id]['post_ids']))
					$post__in = $_SESSION['cspml_posts_in_search'][$map_id]['post_ids'];
				
				/**
				 * Taxonomies
				 * Note: Do not use the variables initialized in the constructor.
				 * Vars in the constructor cannot be called in an AJAX request! */
				 
				$cspml_taxonomies = (array) json_decode($this->cspml_get_setting('list_filter', 'taxonomies', ''));
				$cspml_taxonomy_relation_param = $this->cspml_get_setting('list_filter', 'taxonomy_relation_param', 'AND');

				/**
				 * Custom fields
				 * Note: Do not use the variables initialized in the constructor.
				 * Vars in the constructor cannot be called in an AJAX request! */
				 
				$cspml_custom_fields = (array) json_decode($this->cspml_get_setting('list_filter', 'custom_fields', ''));
				$cspml_custom_field_relation_param = $this->cspml_get_setting('list_filter', 'custom_field_relation_param', 'AND');

				$taxonomies_array = $meta_query_array = array();
				
				/**
				 * When used in a taxonomy page (archive), we need to add the current tax to the query */
				  
				if(!empty($template_tax_query)){
					
					$explode_template_tax_query = explode(',', $template_tax_query);
					
					if(count($explode_template_tax_query == 2)){
						
						$taxonomies_array[] = array(
							'taxonomy' => $explode_template_tax_query[0], // taxonomy name
							'field' => 'slug', // Compare with term_ids
							'terms' => array($explode_template_tax_query[1]), // Selected values
							'operator' => 'IN', // Operator parameter for select values
						);
											   
					}
					
				}

				/**
				 * Loop throught the fields in the filter form */
				 
				foreach($filter_args as $field_key => $field_values){
	
					/**
					 * Get the name of the field which in fact could be the name of ...
					 * ... the taxonomy or the custom_field */
					 
					$attr_name = $field_key;
					
					/**
					 * The field values are:
					 * @The field_type (taxonomy or custom_field)
					 * @The selected values (terms, tags, custom field value, ...) */
						
					/**
					 * If the field_values contains more that one value
					 * The first value in the array is always the field_type (taxonomy or custom_field) ...
					 * ... So if there's only the field_type, no need to contniue cause there will be no selected values */

					if(count($field_values) > 1){
						
						/**
						 * If the field_type is a taxonomy
						 * Create taxonomy args */
						 
						if(isset($field_values[0]) && $field_values[0] == "taxonomy"){
							
							/**
							 * Get Taxonomy Name */
							 
							$tax_name = $attr_name;
							
							/**
							 * Remove the field_type from the array of selected values */
							 
							unset($field_values[0]);
							
							/**
							 * Get the value of the field from the data registered for its tag in the admin */
							 
							$tag_array_value = (array) $cspml_taxonomies[$tax_name];
							
							/**
							 * Call the taxonomy operator and make sure it exists */
							 
							if(isset($tag_array_value['tag_taxonomy_operator_param'])){	
								
								$operator = $tag_array_value['tag_taxonomy_operator_param'];
								
								/**
								 * Check if the select values array is not empty */
								 
								if(count($field_values) > 0){
									
									$array_compare = array('IN', 'NOT IN');
									
									if(in_array($operator, $array_compare))
										$value = array_values(array_unique($field_values));
									else $value = implode('', $field_values);
									
									if($tag_array_value['tag_taxonomy_display_type'] == "double_slider")
										$value = explode(';', implode('', $value));
									
									if(!empty($value)){
											
										/**
										 * And create the args array for the query */
										 
										$taxonomies_array[] = array(
											'taxonomy' => $tax_name, // taxonomy name
											'field' => 'id', // Compare with term_ids
											'terms' => $value, // Selected values
											'operator' => $operator, // Operator parameter for select values
										);
									
									}
									
								}
							
							}
						
						/**
						 * If the field_type is a custom field
						 * Create custom fields args */
						 
						}elseif(isset($field_values[0]) && $field_values[0] == "custom_field"){
	
							/**
							 * Get custom field Name */
							 
							$custom_field_name = $attr_name;
							
							/**
							 * Remove the field_type from the array of selected values */
							 
							unset($field_values[0]);
							
							/**
							 * Get the value of the field from the data registered for its tag in the admin */
							 
							$tag_array_value = (array) $cspml_custom_fields[$custom_field_name];
							
							/**
							 * Call the custom field operator and make sure it exists */
							 
							if(isset($tag_array_value['tag_custom_field_compare_param'])){	
								
								$compare = $tag_array_value['tag_custom_field_compare_param'];
								
								/**
								 * Check if the select values array is not empty */
								 
								if(count($field_values) > 0){
								
									$array_compare = array('IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN');
									
									if(in_array($compare, $array_compare))
										$value = array_values(array_unique($field_values));
									else $value = implode('', $field_values);
									
									if($tag_array_value['tag_custom_field_display_type'] == "double_slider")
										$value = explode(';', implode('', $value));
									
									if(!empty($value)){
										
										$args = array(
											'key' => $custom_field_name, // custom field name
											'value' => $value, // Selected values
											'compare' => $compare, // Operator parameter for select values
										);
										
										$numeric_fields = array('double_slider', 'single_slider', 'number');
										
										if(in_array($tag_array_value['tag_custom_field_display_type'], $numeric_fields))
											$args = array_merge($args, array('type' => 'numeric'));
													
										/**
										 * And create the args array for the query */
										 
										$meta_query_array[] = $args;
									
									}
									
								}
							
							}
							
						}
						
					}
					
				}
					
				/**
				 * At the end, collect all taxonomies and custom fields args array for the query */
				 
				$tax_query = (count($taxonomies_array) == 0) ? array() : array('tax_query' => array_merge(array('relation' => $cspml_taxonomy_relation_param), $taxonomies_array));
				$custom_fields = (count($meta_query_array) == 0) ? array() : array('meta_query' => array_merge(array('relation' => $cspml_custom_field_relation_param), $meta_query_array));		

			}
			
			if(count($init_post_ids) > 0){

				/**
				 * post status */
				 
				$status = (count($ProgressMapClass->post_status) == 0) ? 'publish' : $ProgressMapClass->post_status;
				
				/**
				 * Query */
				 
				$query_args = array(
					'post_type' => (!empty($post_type)) ? $post_type : $ProgressMapClass->post_type, 
					'post_status' => $status,
					'post__in' => (is_array($post__in) && count($post__in) > 0) ? $post__in : $init_post_ids,
					'posts_per_page' => -1,
					'orderby' => 'post__in',							
					'fields' => 'ids'
				);
				
				if(count($tax_query) > 0 && isset($tax_query['tax_query'])) $query_args = array_merge($query_args, array('tax_query' => $tax_query['tax_query']));
				
				if(count($custom_fields) > 0 && isset($custom_fields['meta_query'])){ 
					$query_args = array_merge(
						$query_args, 
						array('meta_query' => $custom_fields['meta_query'])				
					);			
				}						
				
				/**
				 * Add Progress Map Custom Fields to the query */
				 
				if(!empty($ProgressMapClass->custom_fields)){
					
					$cspm_custom_fields = $ProgressMapClass->cspm_parse_query_meta_fields($ProgressMapClass->custom_fields);
					
					if(count($custom_fields) == 0 && count($cspm_custom_fields) > 0){		
									
						$query_args = array_merge(
							$query_args,
							array('meta_query' => $cspm_custom_fields['meta_query'])
						);
						
					}elseif(count($custom_fields) > 0 && count($cspm_custom_fields) > 0){
						
						unset($cspm_custom_fields['meta_query']['relation']);
						
						$custom_field_query = array_merge(
							$query_args['meta_query'],
							$cspm_custom_fields['meta_query']
						);
						
						$query_args['meta_query'] = $custom_field_query;
						
					}
				}

				$post_ids = query_posts( $query_args );

			}else $post_ids = $init_post_ids;
			
			/**
			 * Save the post_ids in a session in order to not start ...
			 * ... calling all posts when paginating or sorting by or filtering */
			 
			$_SESSION['cspml_listings_filter_post_ids'][$map_id] = $post_ids;

			print_r(json_encode($post_ids));
			
			die();
			
		}
		
		
		/**
		 * Count the number of posts per term
		 *
		 * @since 1.0
		 */
		function cspml_count_term_posts($taxonomy, $term_id, $post_type, $post_ids = array()){
					
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
				
			/**
			 * post status */
			 
			$status = (count($ProgressMapClass->post_status) == 0) ? 'publish' : $ProgressMapClass->post_status;				

			if(count($post_ids) > 0){
																	   
				$posts = get_posts(array(
					'post_type' => $post_type,
					'post_status' => $status,
					'tax_query' => array(
						array(
							'taxonomy' => $taxonomy,
							'field' => 'id',
							'terms' => $term_id
						)
					),
					'post__in' => $post_ids,
					'fields' => 'ids',
					'posts_per_page' => -1,
					'suppress_filters' => false,
				)); 

				wp_reset_postdata();
	
			}else $posts = array();
			
			return count($posts);
		
		}
		
		
		/**
		 * Count the number of posts by custom field value
		 *
		 * @since 1.0
		 */
		function cspml_count_custom_field_posts($post_type, $custom_field_name){
						
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
			$ProgressMapClass = CodespacingProgressMap::this();
				
			/**
			 * post status */
			 
			$status = (count($ProgressMapClass->post_status) == 0) ? 'publish' : $ProgressMapClass->post_status;				
				
			$custom_field_values = $this->cspml_get_meta_values( $custom_field_name, $post_type, $status );
			
			$meta_counts = array();
			
			if( !empty( $custom_field_values ) ) {
				
				$meta_counts = array();
				
				foreach( $custom_field_values as $meta_value ){
					
					$meta_counts[$meta_value] = ( isset( $meta_counts[$meta_value] ) ) ? $meta_counts[$meta_value] + 1 : 1;
					
				}
				
			}
			
			return $meta_counts;
			
		}
		
		
		/**
		 * Get All Values For A Custom Field Key 
		 *
		 * @key: The meta_key of the post meta
		 * @type: The name of the custom post type
		 * @status: The status of the post
		 * 
		 * @since 1.0
		 */
		function cspml_get_meta_values($key = '', $post_type = 'post', $status = 'publish') {
			
			global $wpdb;
			
			if( empty( $key ) )
				return;
			
			$results = $wpdb->get_col( $wpdb->prepare( "
				SELECT pm.meta_value FROM {$wpdb->postmeta} pm
				LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
				WHERE pm.meta_key = '%s' 
				AND pm.meta_value != '' 
				AND p.post_status = '%s' 
				AND p.post_type = '%s'
			", $key, $status, $post_type ) );

			return $results;
			
		}
	
		function cspml_wpsf_before_settings(){	
			
			if (!class_exists('CodespacingProgressMap'))
				return; 
						
			global $wp_settings_pml;
								
			$sections = array();
			
			echo '<div class="codespacing_container" style="padding:0px; margin-top:30px; height:auto; width:800px; position:relative; box-shadow:rgba(0,0,0,.298039) 0 1px 2px -1px">';
				
				echo '<div class="cspm_admin_square_loader"></div>';
				
				echo '<div class="codespacing_header"><img src="'.$this->plugin_url.'settings/img/list-and-filter-admin-logo.png" /></div>';
				
				echo '<div class="codespacing_menu_container" style="width:auto; float:left; height:auto;">';
					
					echo '<ul class="codespacing_menu">';
					
						if(!empty($wp_settings_pml)){
							
							usort($wp_settings_pml, array(&$this->cspml_wpsf, 'cspml_sort_array'));
							
							$first_section = $wp_settings_pml[0]['section_id'];
							
							foreach($wp_settings_pml as $section){
								
								if(isset($section['section_id']) && $section['section_id'] && isset($section['section_title'])){
									
									echo '<li class="codespacing_li" id='.$section['section_id'].'>'.$section['section_title'].'</li>';
									
									$sections[$section['section_id']] = $section['section_title'];								
									
								}
								
							}
								
						}
					
					echo '</ul>';
					
				echo '</div>';
				 
				echo '<div style="width:539px; height:auto; min-height:570px; padding:30px; float:left; border-left: 1px solid #e8ebec; border-top:0px solid #008fed; background:#f7f8f8 url('.$this->plugin_url.'settings/img/bg.png) repeat;">';	
				
		}
		
		function cspml_wpsf_after_settings(){
						
			if (!class_exists('CodespacingProgressMap'))
				return; 
			
				$ProgressMapClass = CodespacingProgressMap::this();
				
				echo '<div class="cspm_admin_btm_square_loader"></div>';
				
				echo '</div>';
				
				echo '<div style="clear:both"></div>';
				
			echo '</div>';	
			
			echo '<div class="codespacing_rates_fotter"><a target="_blank" href="http://codecanyon.net/item/progress-map-wordpress-plugin/5581719"><img src="'.$ProgressMapClass->cspm_plugin_url.'settings/img/rates.jpg" /></a></div>';
			
			echo '<div class="codespacing_copyright">&copy; All rights reserved CodeSpacing. Progress Map List & Filter'.$this->plugin_version.'</div>';
			
		}
	
	}
	
}	

if( class_exists( 'ProgressMapList' ) )
	new ProgressMapList();

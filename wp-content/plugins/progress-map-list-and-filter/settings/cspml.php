<?php

global $wp_settings_pml;
	
/**
 * List Layout Settings section */

$wp_settings_pml[] = array(
    'section_id' => 'layout',
    'section_title' => 'Layout Settings',
    'section_description' => '',
    'section_order' => 1,
    'fields' => array(
        array(
            'id' => 'list_layout',
            'title' => 'Layout Type',
            'desc' => 'Choose a layout type.
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"list_layout"</strong>.
			<br />Possible values are "vertical", "horizontal-left" and "horizontal-right". The words "left" & "right" refers to the position of the map!
			<br />Usage example: [codespacing_progress_map list_ext="yes" list_layout="horizontal-right"]</span>',
            'type' => 'radio',
            'std' => 'vertical',
            'choices' => array(
				'vertical' => 'Vertical',
				'horizontal-left' => 'Horizontal - Map on the Left, List & Filter on the Right',
				'horizontal-right' => 'Horizontal - Map on the Right, List & Filter on the Left',
            )
        ),
		array(
            'id' => 'map_height',
            'title' => 'Map Height',
            'desc' => 'Specify the height of the map in pixels. Don\'t type the word "px". Default to "400"
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"map_height"</strong>.
			<br />Usage example: [codespacing_progress_map list_ext="yes" map_height="300px"]</span>',
            'type' => 'text',
            'std' => '400',
        ),		
        array(
            'id' => 'list_height',
            'title' => 'List Max-Height',
            'desc' => 'Specify the height of the list in pixels. Don\'t type the word "px". By default, the list has no height!
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"list_height"</strong>.
			<br />Usage example: [codespacing_progress_map list_ext="yes" list_height="600px"]</span>',
            'type' => 'text',
            'std' => '',
        ),				
	)
);

/**
 * Options Bar Settings section */

$wp_settings_pml[] = array(
    'section_id' => 'options_bar',
    'section_title' => 'Options Bar Settings',
    'section_description' => '',
    'section_order' => 2,
    'fields' => array(
        array(
            'id' => 'show_options_bar',
            'title' => 'Options Bar Visibility',
            'desc' => 'Show/Hide the Options bar.<br />The <u>"Options bar"</u> is the top area containing the "View options", the "Sort by" option and the "Posts count".,
            <br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"show_options_bar"</strong>.
			<br />Possible values are "yes" and "no".
			<br />Usage example: [codespacing_progress_map list_ext="yes" show_options_bar="no"]</span>',
			'type' => 'radio',
            'std' => 'yes',
            'choices' => array(
				'yes' => 'Show',
				'no' => 'Hide',
            )
        ),		
		array(
            'id' => 'view_options_section',
            'title' => '<span class="section_sub_title">View options settings</span>',
            'desc' => '',
            'type' => 'custom',
        ),														
        array(
            'id' => 'show_view_options',
            'title' => 'View Options Visibility',
            'desc' => 'Show/Hide the "View options".',
            'type' => 'radio',
            'std' => 'yes',
            'choices' => array(
				'yes' => 'Show',
				'no' => 'Hide',
            )
        ),
        array(
            'id' => 'default_view_option',
            'title' => 'Initial view',
            'desc' => 'Select the initial view. Default to "List".
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"default_view"</strong>.
			<br />Possible value are "list" and "grid".
			<br />Usage example: [codespacing_progress_map list_ext="yes" default_view="grid"]</span>',
            'type' => 'radio',
            'std' => 'list',
            'choices' => array(
				'list' => 'List',
				'grid' => 'Grid',
            )
        ),
        array(
            'id' => 'grid_cols',
            'title' => 'Grid columns',
            'desc' => 'Select the number of items to display in single row for the "Grid view".',
            'type' => 'radio',
            'std' => 'cols3',
            'choices' => array(
				'cols1' => 'One column',
				'cols2' => 'Two columns',
				'cols3' => 'Three columns',
				'cols4' => 'Four columns',
				'cols6' => 'Six columns'
            )
        ),	
		array(
            'id' => 'post_count_section',
            'title' => '<span class="section_sub_title">Posts count settings</span>',
            'desc' => '',
            'type' => 'custom',
        ),	
        array(
            'id' => 'show_posts_count',
            'title' => 'Posts Count Visibility',
            'desc' => 'Show/Hide the "Posts count".',
            'type' => 'radio',
            'std' => 'yes',
            'choices' => array(
				'yes' => 'Show',
				'no' => 'Hide',
            )
        ),	
        array(
            'id' => 'posts_count_clause',
            'title' => 'Posts count clause',
            'desc' => 'Use this field to enter your custom clause.<br /><strong>Syntaxe:</strong> YOUR CLAUSE [posts_count] YOUR CLAUSE',
            'type' => 'text',
            'std' => '[posts_count] Result(s)',
        ),
	)
);


/**
 * List Items settings */

$wp_settings_pml[] = array(
    'section_id' => 'list_items',
    'section_title' => 'List Items Settings',
    'section_description' => '',
    'section_order' => 3,
    'fields' => array(
        array(
            'id' => 'listings_title',
            'title' => 'List Item Title',
            'desc' => 'Create your customized title by entering the name of your custom syntaxe. You can combine the title with your custom fields. Leave this field empty to use the default title.
					<br /><strong>Syntax:</strong> [meta_key<sup>1</sup>][separator<sup>1</sup>][meta_key<sup>2</sup>][separator<sup>2</sup>][meta_key<sup>n</sup>]...[title lenght].
					<br /><strong>Example of use:</strong> [property_price][s=,][-][title][l=50]
					<br /><strong>*</strong> To add the title enter [title]
					<br /><strong>*</strong> To insert empty an space enter [-]
					<br /><strong>* Make sure there\'s no empty spaces between ][</strong>',
            'type' => 'textarea',
            'std' => '',
        ),
        array(
            'id' => 'listings_details',
            'title' => 'List Item Description',
            'desc' => 'Create your customized description content. You can combine the content with your custom fields & taxonomies. Leave this field empty to use the default description.
					<br /><strong>Syntax:</strong> [content;content_length][separator][t=label:][meta_key][separator][t=Category:][tax=taxonomy_slug][separator]...[description length]
					<br /><strong>Example of use:</strong> [content;80][s=br][t=Category:][-][tax=category][s=br][t=Address:][-][post_address]
					<br /><strong>*</strong> To specify a description length, use <strong>[l=LENGTH]</strong>. Change LENGTH to a number (e.g. 100).
					<br /><strong>*</strong> To add a label, use <strong>[t=YOUR_LABEL]</strong>
					<br /><strong>*</strong> To add a custom field, use <strong>[CUSTOM_FIELD_NAME]	</strong>				
					<br /><strong>*</strong> To insert a taxonomy, use <strong>[tax=TAXONOMY_SLUG]</strong>
					<br /><strong>*</strong> To insert new line enter <strong>[s=br]</strong>
					<br /><strong>*</strong> To insert an empty space enter <strong>[-]</strong>
					<br /><strong>*</strong> To insert the content/excerpt, use <strong>[content;LENGTH]</strong>. Change LENGTH to a number (e.g. 100).
					<br /><strong>* Make sure there\'s no empty spaces between ][</strong>',
            'type' => 'textarea',
            'std' => '[l=700]',
        ),	
		array(
            'id' => 'marker_position_btn_section',
            'title' => '<span class="section_sub_title">"Position on the map" Button</span>',
            'desc' => '',
            'type' => 'custom',
        ),																
        array(
            'id' => 'show_fire_pinpoint_btn',
            'title' => 'Button Visibilty',
            'desc' => 'Show/Hide the button that shows the item\'s location on the map.',
            'type' => 'radio',
            'std' => 'yes',
            'choices' => array(
				'yes' => 'Show',
				'no' => 'Hide',
            )
        ),
	)
);

 
/**
 * Sort listings settings */

$wp_settings_pml[] = array(
    'section_id' => 'sort_option',
    'section_title' => 'Sort Listings Settings',
    'section_description' => '',
    'section_order' => 4,
    'fields' => array(
        array(
            'id' => 'show_sort_option',
            'title' => 'Turn On/Off',
            'desc' => 'Enable/Disable the sort option.',
            'type' => 'radio',
            'std' => 'yes',
            'choices' => array(
				'yes' => 'Enable',
				'no' => 'Disable',
            )
        ),		
        array(
            'id' => 'sort_options',
            'title' => 'Default Sort Options',
            'desc' => 'Select the sort options to use from the available options.',
            'type' => 'checkboxes_array',
            'std' => '',
            'choices' => array(
				'default|Výchozí|init' => 'Výchozí',
				'data-date-created|Date (Newest first)|desc' => 'Date (Newest first)',
				'data-date-created|Date (Oldest first)|asc' => 'Date (Oldest first)',
				'data-title|A -> Z|asc' => 'A -> Z',
				'data-title|Z -> A|desc' => 'Z -> A'
            )
        ),	
		array(
            'id' => 'custom_sort_options',
            'title' => 'Custom Sort Options', 
            'desc' => 'Add your custom sort options. You can sort list items by custom fields.<br />
			           <strong>!Important:</strong> The data are not going to be saved automatically in the Database, you still have to click the "Save" button below to 
					   confirm your data. You can add as many custom sort options before saving.',
            'type' => 'tag',
            'std' => '',
			'helpers_container_id' => 'sort_options',
            'helpers' => array(	
				array(
					'id' => 'tag_sort_options_name',
					'title' => 'Custom field name', 
					'desc' => 'The name of the custom field to sort with. e.g. "property_price"',
					'type' => 'text',
					'std' => '',
					'class' => 'required'
				),
				array(
					'id' => 'tag_sort_options_label',
					'title' => 'Label', 
					'desc' => 'The label to use to describe the sort option. e.g. "Price"',
					'type' => 'text',
					'std' => '',
					'class' => 'required'					
				),				
				array(
					'id' => 'tag_sort_options_order',
					'title' => 'Sort order', 
					'desc' => 'Select the sort order.',
					'type' => 'checkboxes_array',
					'std' => array('desc', 'asc'),
					'choices' => array(
						'desc' => 'Descending <sup>e.g. (Highest first)</sup>',
						'asc' => 'Ascending <sup>e.g. (Lowest first)</sup>'
					)
				),
				array(
					'id' => 'tag_sort_options_desc_suffix',
					'title' => 'Descending suffix', 
					'desc' => 'Enter the suffix to add to the end of the label for the descending order. e.g. "(Highest first)"',
					'type' => 'text',
				),
				array(
					'id' => 'tag_sort_options_asc_suffix',
					'title' => 'Ascending suffix', 
					'desc' => 'Enter the suffix to add to the end of the label for the ascending order. e.g. "(Lowest first)"',
					'type' => 'text',
				),
				array(
					'id' => 'tag_sort_options_visibilty',
					'title' => 'Visibilty', 
					'desc' => 'Show/hide the custom sort option from being displayed. Default "Show"',
					'type' => 'radio',
					'std' => 'yes',
					'choices' => array(
						'yes' => 'Show',
						'no' => 'Hide'
					)
				),
				array(
					'id' => 'add_sort_options',
					'helpers_id' => 'sort_options',
					'type' => 'submit_tag'
				)
			)
		),			
	)
);


/**
 * Pagination Settings section */

$wp_settings_pml[] = array(
    'section_id' => 'pagiantion',
    'section_title' => 'Pagination Settings',
    'section_description' => '',
    'section_order' => 5,
    'fields' => array(
		array(
			'id' => 'posts_per_page',
			'title' => 'Number of posts per page', 
			'desc' => 'Enter the number of the posts per page. Default to the number selected in "Settings / Reading / Blog pages show at most".
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"posts_per_page"</strong>.
			<br />Usage example: [codespacing_progress_map list_ext="yes" posts_per_page="20"]</span>',
			'type' => 'text',
			'std' => '',
		),
		array(
			'id' => 'pagination_position',
			'title' => 'Pagination position', 
			'desc' => 'Select the position of the pagination in the page. Default to "Bottom".
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"paginate_position"</strong>.
			<br />Possible value are "top", "bottom" and "both".
			<br />Usage example: [codespacing_progress_map list_ext="yes" paginate_position="both"]</span>',
			'type' => 'radio',
			'std' => 'bottom',
			'choices' => array(
				'top' => 'Top',
				'bottom' => 'Bottom',
				'both' => 'Both',
			)
		),
		array(
			'id' => 'pagination_align',
			'title' => 'Pagination alignment', 
			'desc' => 'Select the alignment of the pagination in the page. Default to "Center"
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"paginate_align"</strong>.
			<br />Possible value are "left", "center" and "right".
			<br />Usage example: [codespacing_progress_map list_ext="yes" paginate_align="right"]</span>',
			'type' => 'radio',
			'std' => 'center',
			'choices' => array(
				'left' => 'Left',
				'center' => 'Center',
				'right' => 'Right',
			)
		),		
		array(
			'id' => 'prev_page_text',
			'title' => 'Previous page text', 
			'desc' => 'Enter the text that appears in the pagination button "Previous". Default to "&laquo;"',
			'type' => 'text',
			'std' => '&laquo;',
		),
		array(
			'id' => 'next_page_text',
			'title' => 'Next page text', 
			'desc' => 'Enter the text that appears in the pagination button "Next". Default to "&raquo;"',
			'type' => 'text',
			'std' => '&raquo;',
		),
		array(
			'id' => 'show_all',
			'title' => 'Show all pages', 
			'desc' => 'Select True to show all of the pages in the pagination instead of a short list of pages near the current page. Default to "False".',
			'type' => 'radio',
			'std' => 'false',
			'choices' => array(
				'true' => 'True',
				'false' => 'False',
			)
		)
	)
);


/**
 * Filter serch settings section */

$wp_settings_pml[] = array(
    'section_id' => 'list_filter',
    'section_title' => 'Search Filter Settings',
    'section_description' => '',
    'section_order' => 6,
    'fields' => array(
        array(
            'id' => 'faceted_search_option',
            'title' => 'Search Filter option',
            'desc' => 'Select "Yes" to enable this option in the plugin. Default to "No".
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"list_filter"</strong>.
			<br />Possible value are "yes" and "no".
			<br />Usage example: [codespacing_progress_map list_ext="yes" list_filter="yes"]</span>',
            'type' => 'radio',
            'std' => 'no',
            'choices' => array(
				'yes' => 'Yes',
				'no' => 'No'
            )
        ),	
        array(
            'id' => 'faceted_search_position',
            'title' => 'Search Filter position',
            'desc' => 'Choose the position of the search filter on a page. Default to "Left".
			<br /><span style="color:red;">You can override this option in a shortcode by using the attribute <strong>"filter_position"</strong>.
			<br />Possible value are "left" and "right".
			<br />Usage example: [codespacing_progress_map list_ext="yes" filter_position="right"]</span>',
            'type' => 'radio',
            'std' => 'left',
            'choices' => array(
				'left' => 'Left',
				'right' => 'Right'
            )
        ),			
		array(
            'id' => 'taxonomies_section',
            'title' => '<span class="section_sub_title">Taxonomy Parameters</span>',
            'desc' => '',
            'type' => 'custom',
        ),
		array(
            'id' => 'taxonomies',
            'title' => 'Taxonomies', 
            'desc' => 'Add the taxonomies to use in the faceted search form.<br />
			           <strong>!Important:</strong> The data are not going to be saved automatically in the Database, you still have to click the "Save" button below to 
					   confirm your data. You can add as many taxonomies before saving.',
            'type' => 'tag',
            'std' => '',
			'helpers_container_id' => 'taxonomy',
            'helpers' => array(	
				array(
					'id' => 'tag_taxonomy_label',
					'title' => 'Label', 
					'desc' => 'Enter a label to describe the taxonomy. e.g. "Category".',
					'type' => 'text',
					'std' => '',
					'class' => 'required'
				),
				array(
					'id' => 'tag_taxonomy_name',
					'title' => 'Taxonomy name', 
					'desc' => 'Enter the taxonomy name. e.g "category".',
					'type' => 'text',
					'std' => '',
					'class' => 'required'					
				),
				array(
					'id' => 'tag_taxonomy_exclude_terms',
					'title' => 'Exclude term IDs',
					'desc' => 'Enter the term IDs that you want to exclude. Seperated by comma ",".',
					'type' => 'text',
					'std' => '',
				),
				array(
					'id' => 'tag_taxonomy_operator_param',
					'title' => '"Operator" parameter', 
					'desc' => 'Operator to test. Possible values are "IN", "NOT IN", "AND".<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Taxonomy_Parameters" target="_blank">More</a>',
					'type' => 'radio',
					'std' => 'IN',
					'choices' => array(
						'AND' => 'AND',
						'IN' => 'IN',
						'NOT IN' => 'NOT IN',
					)
				),
				array(
					'id' => 'tag_taxonomy_hide_empty',
					'title' => 'Hide empty terms', 
					'desc' => 'Hide the terms with no posts assigned to them. Default "Yes"',
					'type' => 'radio',
					'std' => 'yes',
					'choices' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				),
				array(
					'id' => 'tag_taxonomy_search_all_option',
					'title' => '"Search All" Option', 
					'desc' => 'Select "Yes" to enable this option for this taxonomy. Default "No"',
					'type' => 'radio',
					'std' => 'no',
					'choices' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				),
				array(
					'id' => 'tag_taxonomy_search_all_text',
					'title' => '"Search All" Label', 
					'desc' => 'Enter the label for the "Search all" option.',
					'type' => 'text',
					'std' => 'All',
				),
				array(
					'id' => 'tag_taxonomy_symbol',
					'title' => 'Symbol', 
					'desc' => 'The symbol is the indicator of the taxonomy, like currency symbol ($) or surface unit (m²).',
					'type' => 'text',
					'std' => '',
				),
				array(
					'id' => 'tag_taxonomy_symbol_position',
					'title' => 'Symbol position', 
					'desc' => 'Select in which side of the taxonomy the symbol will be displayed. Default (Before).',
					'type' => 'radio',
					'std' => 'before',
					'choices' => array(
						'before' => 'Before',
						'after' => 'After'
					)
				),				
				array(
					'id' => 'tag_taxonomy_show_count',
					'title' => 'Show count', 
					'desc' => 'Select "Yes" to show the number of posts assigned to each taxonomy term. Default "No".',
					'type' => 'radio',
					'std' => 'no',
					'choices' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				),
				array(
					'id' => 'tag_taxonomy_display_type',
					'title' => 'Display type', 
					'desc' => 'Select the display type for this taxonomy. Default "checkbox".',
					'type' => 'radio',
					'std' => 'checkbox',
					'choices' => array(
						'checkbox' => 'Checkbox',
						'radio' => 'Radio',
						'select' => 'Select',
						'number' => 'Number <sup>Use it only for numerical values</sup>',
						'double_slider' => 'Double range slider <sup>Use it only for numerical values</sup>',
						'single_slider' => 'Single range slider <sup>Use it only for numerical values</sup>'
					)
				),
				array(
					'id' => 'tag_taxonomy_visibilty',
					'title' => 'Visibilty', 
					'desc' => 'Show/hide the taxonomy from being displayed in the filter form. Default "Show".',
					'type' => 'radio',
					'std' => 'yes',
					'choices' => array(
						'yes' => 'Show',
						'no' => 'Hide'
					)
				),
				array(
					'id' => 'add_taxonomy',
					'helpers_id' => 'taxonomy',
					'type' => 'submit_tag'
				)
			)
		),	
		array(
			'id' => 'taxonomy_relation_param',
			'title' => '"Relation" parameter', 
			'desc' => 'Select the relationship between multiple taxonomies. Default is "AND". <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Taxonomy_Parameters" target="_blank">More</a>',
			'type' => 'radio',
			'std' => 'AND',
			'choices' => array(
				'AND' => 'AND',
				'OR' => 'OR',
			)
		),	
		array(
            'id' => 'custom_fields_section',
            'title' => '<span class="section_sub_title">Custom Fields Parameters</span>',
            'desc' => '',
            'type' => 'custom',
        ),
		array(
            'id' => 'custom_fields',
            'title' => 'Custom fields', 
            'desc' => 'Add the custom fields to use in the faceted search form.<br />
			           <strong>!Important:</strong> The data are not going to be saved automatically in the Database, you still have to click the "Save" button below to 
					   confirm your data. You can add as many custom fields before saving.',
            'type' => 'tag',
            'std' => '',
			'helpers_container_id' => 'custom_field',
            'helpers' => array(
				array(
					'id' => 'tag_custom_field_label',
					'title' => 'Label', 
					'desc' => 'Enter a label to describe the custom field. e.g. "Price".',
					'type' => 'text',
					'std' => '',
					'class' => 'required'	
				),
				array(
					'id' => 'tag_custom_field_name',
					'title' => 'Custom field name',
					'desc' => 'Enter the name of custom field. e.g. "property_price"',
					'type' => 'text',
					'std' => '',
					'class' => 'required'
				),
				array(
					'id' => 'tag_custom_field_symbol',
					'title' => 'Symbol', 
					'desc' => 'The symbol is the indicator of the custom field, like currency symbol ($) or surface unit (m²).',
					'type' => 'text',
					'std' => '',
				),
				array(
					'id' => 'tag_custom_field_symbol_position',
					'title' => 'Symbol position', 
					'desc' => 'Select in which side of the custom field the symbol will be displayed. Default (Before).',
					'type' => 'radio',
					'std' => 'before',
					'choices' => array(
						'before' => 'Before',
						'after' => 'After'
					)
				),
				array(
					'id' => 'tag_custom_field_compare_param',
					'title' => '"Compare" parameter', 
					'desc' => 'Operator to test. Use it carefully. If you choose "BETWEEN", then your <u>"Display type"</u> should be <u>"Double range slider"</u>. More about compare, please visit. <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters" target="_blank">More</a>',
					'type' => 'select',
					'std' => '=',
					'choices' => array(
						'=' => '=',
						'!=' => '!=',
						'>' => '>',
						'>=' => '>=',
						'<' => '<',
						'<=' => '<=',
						'LIKE' => 'LIKE',						
						'NOT LIKE' => 'NOT LIKE',
						'IN' => 'IN',												
						'NOT IN' => 'NOT IN',						
						'BETWEEN' => 'BETWEEN',
						'NOT BETWEEN' => 'NOT BETWEEN',
						'EXISTS' => 'EXISTS',
						'NOT EXISTS' => 'NOT EXISTS',						
					)
				),				
				array(
					'id' => 'tag_custom_field_search_all_option',
					'title' => '"Search All" Option', 
					'desc' => 'Select "Yes" to enable this option for the "" taxonomy. Default "No"',
					'type' => 'radio',
					'std' => 'no',
					'choices' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				),
				array(
					'id' => 'tag_custom_field_search_all_text',
					'title' => '"Search All" Label', 
					'desc' => 'Enter yout custom label for the "search all" option.',
					'type' => 'text',
					'std' => 'All',
				),
				array(
					'id' => 'tag_custom_field_show_count',
					'title' => 'Show count', 
					'desc' => 'Select "Yes" to show the number of posts assigned to the custom field. Default "No".',
					'type' => 'radio',
					'std' => 'no',
					'choices' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				),
				array(
					'id' => 'tag_custom_field_display_type',
					'title' => 'Display type', 
					'desc' => 'Select the display type of the custom field. Default "checkbox".',
					'type' => 'radio',
					'std' => 'checkbox',
					'choices' => array(
						'checkbox' => 'Checkbox',
						'radio' => 'Radio',
						'select' => 'Select',
						'number' => 'Number <sup>Use it only for numerical values</sup>',
						'double_slider' => 'Double range slider <sup>Use it only for numerical values</sup>',
						'single_slider' => 'Single range slider <sup>Use it only for numerical values</sup>'
					)
				),
				array(
					'id' => 'tag_custom_field_visibilty',
					'title' => 'Visibilty', 
					'desc' => 'Show/hide the custom field from being displayed in the filter form. Default "Yes"',
					'type' => 'radio',
					'std' => 'yes',
					'choices' => array(
						'yes' => 'Show',
						'no' => 'Hide'
					)
				),
				array(
					'id' => 'add_custom_field',
					'helpers_id' => 'custom_field',
					'type' => 'submit_tag'
				)
			)
        ),
		array(
			'id' => 'custom_field_relation_param',
			'title' => '"Relation" parameter', 
			'desc' => 'Select the relationship between multiple custom fields. Default is "AND". <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters" target="_blank">More</a>',
			'type' => 'radio',
			'std' => 'AND',
			'choices' => array(
				'AND' => 'AND',
				'OR' => 'OR',
			)
		),
		array(
            'id' => 'customization_section',
            'title' => '<span class="section_sub_title">Custmizations</span>',
            'desc' => '',
            'type' => 'custom',
        ),
		array(
			'id' => 'filter_btn_text',
			'title' => 'Filter button text', 
			'desc' => 'Enter your customized text for the "Filter" button.',
			'type' => 'text',
			'std' => 'Filter',
		),
	)
);

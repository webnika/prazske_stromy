function disableEnterKey(e)
{
     var key;

     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox

     if(key == 13)
          return false;
     else
          return true;
}

function alerte(obj) {
		
	if (typeof obj == 'object') {
		var foo = '';
		for (var i in obj) {
			if (obj.hasOwnProperty(i)) {
				foo += '[' + i + '] => ' + obj[i] + '\n';
			}
		}
		alert(foo);
	}else {
		alert(obj);
	}
	
}

jQuery(document).ready(function($){ 
				   
	// Control over the menu

	$("li#"+cspml_admin_vars.first_section+"").addClass('current');
	
	$("div[class^=custom_section_]").hide();
	
	$("ul.codespacing_menu li").click(function(){
		$('div[class^=custom_section_]').hide();
		var id = $(this).attr('id');
		$(".custom_section_" + id + "").show();
		$("li").removeClass('current');
		$(this).addClass('current');
		$.cookie('codespacing_pml_admin_menu', id, { expires: 1 });
	});	

	if($.cookie('codespacing_pml_admin_menu') == null){
		$(".custom_section_"+cspml_admin_vars.first_section+"").show();
		$("li").removeClass('current');
		$("li#"+cspml_admin_vars.first_section+"").addClass('current');
	}else{
		$(".custom_section_" + $.cookie('codespacing_pml_admin_menu') + "").show();
		$("li").removeClass('current');
		$("li#" + $.cookie('codespacing_pml_admin_menu') + "").addClass('current');
	}
	

	// Customize Checkboxes and Radios button
	
	/*$('div[class^=custom_section_] input').iCheck({
	  checkboxClass: 'icheckbox_minimal-blue',
	  radioClass: 'iradio_minimal-blue',
	  increaseArea: '20%',
	});*/
	
	
	// Add colspan attr to <th></th> of subtitle section
	
	$('span.section_sub_title, span.section_sub_title_child').parent().parent().next('div.cspml_field').remove();
	$('span.section_sub_title, span.section_sub_title_child').parent().parent().addClass('cspml_colspan_2');
	
	
	// Multi select
		
	/*$('.cspml_multi_select').multiSelect({
		
		selectableHeader: "<input type='text' class='cspml-search-input' autocomplete='off' placeholder='Search'>",
		selectionHeader: "<input type='text' class='cspml-search-input' autocomplete='off' placeholder='Search'>",
		selectableFooter: "Selectable items",
		selectionFooter: "Selected items",
		afterInit: function(ms){
			var that = this,
				$selectableSearch = that.$selectableUl.prev(),
				$selectionSearch = that.$selectionUl.prev(),
				selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
				selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
			
			that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
			.on('keydown', function(e){
			  if (e.which === 40){
				that.$selectableUl.focus();
				return false;
			  }
			});
			
			that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
			.on('keydown', function(e){
			  if (e.which == 40){
				that.$selectionUl.focus();
				return false;
			  }
			});
		},
		afterSelect: function(){
			this.qs1.cache();
			this.qs2.cache();
		},
		afterDeselect: function(){
			this.qs1.cache();
			this.qs2.cache();
		}		
		
	});
	
	$('a.cspml_ms_select_all').click(function(){
		
		var id = $(this).attr('id');
		
		$('.cspml_multi_select#'+id+'').multiSelect('select_all');
		
		return false;
		
	});
	
	$('a.cspml_ms_deselect_all').click(function(){
		
		var id = $(this).attr('id');
		
		$('.cspml_multi_select#'+id+'').multiSelect('deselect_all');
		
		return false;
		
	});	
	
	$('a.cspml_ms_refresh').livequery('click', function(){
		
		var id = $(this).attr('id');
		
		$('.cspml_multi_select#'+id+'').multiSelect('refresh');	
			
	});*/
	
	
	// Open the tag fields container
	
	$('div.cspml_add_tag').livequery('click', function(){
		
		var id = $(this).attr('id');
		var helpers_id = $(this).attr('data-helpers-container-id');
		
		// Show the submit button
		$('a[class=cspml_submit_tag_form][data-helpers-id='+helpers_id+']').show(function(){
			// Hide the update button
			$('a[class=cspml_update_tag_form][data-helpers-id='+helpers_id+']').hide();		
		});		
		
		$('input[type=text][id^=tag_'+helpers_id+']').each(function(){ $(this).val(''); });
				
		if(!$('div.cspml_add_tag_container#'+id+'').is(":visible"))
		$('div.cspml_add_tag_container#'+id+'').slideToggle(500);
		
	});
	
	// Cancel
	
	$('a.cspml_cancel_tag_form').livequery('click', function(){
		
		var helpers_id = $(this).attr('data-helpers-id');
		
		// Show the submit button
		$('a[class=cspml_submit_tag_form][data-helpers-id='+helpers_id+']').show(function(){
			// Hide the update button
			$('a[class=cspml_update_tag_form][data-helpers-id='+helpers_id+']').hide();		
		});		

		$('input[type=text][id^=tag_'+helpers_id+']').each(function(){ $(this).val(''); });
		
		if($('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').is(":visible"))
		$('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').slideToggle(500);
		
	});
	
	// Add/Edit tag		
	
	var updated_tag = [];
	
	$('a.cspml_submit_tag_form, a.cspml_update_tag_form').livequery('click', function(){

		// Get the helpers id
		var helpers_id = $(this).attr('data-helpers-id');
		
		var clicked_element = $(this).attr('class');
		
		// Validate the form
		jQuery.validator.setDefaults({
			success: "valid",
			wrapper: "em",
		});
		
		var form = $("#cspml_form");
		
		form.validate();
				
		if(!form.valid()){
			
			jQuery('html, body').animate({
				scrollTop: jQuery("div[class=cspml_add_tag_container][data-helpers-container-id="+helpers_id+"]").offset().top
			});
				
		}else{
			
			// The old value of the textarea containing the tags
			var textarea_val = $('textarea[data-helpers-textarea='+helpers_id+']').val();
			
			var tag_fields_val = {};
			
			// Get the label of the tag
			var tag_label = $('input[type=text][id=tag_'+helpers_id+'_label]').val();
			var tag_name = $('input[type=text][id=tag_'+helpers_id+'_name]').val();
			
			var multi_input_values = [];
			
			// Loop throught each field in the tag container and collecte the field values
			$('input[type=text][id^=tag_'+helpers_id+'], input[type=radio][id^=tag_'+helpers_id+']:checked, input[type=checkbox][id^=tag_'+helpers_id+']:checked, textarea[id^=tag_'+helpers_id+'], select[id^=tag_'+helpers_id+']').each(function() {
				
				if(!($(this).attr('name') in tag_fields_val)){
				
					// Checkbox array			
					if($(this).is("input[type=checkbox]")){
						
						var name_array = $(this).attr('name').split('[]');
						var name = name_array[0];
						
						// Clear the array
						if(jQuery.inArray($(this).attr('value'), tag_fields_val[name]) > -1)
							multi_input_values = [];	
						
						if(name_array.length == 2){
							
							multi_input_values.push($(this).attr('value'));
							tag_fields_val[name] = multi_input_values;
							
						}else{
							
							tag_fields_val[$(this).attr('name')] = $(this).val();
						
						}
					
					// Multiple select
					}else if($(this).is("select[multiple=multiple]")){
						
						// Clear the array
						multi_input_values = [];
						
						var name_array = $(this).attr('name').split('[]');
						var name = name_array[0];
						var select_id = $(this).attr('id');
						
						if(name_array.length == 2){
							
							// Loop throught all the list
							$('div#ms-'+select_id+' div.ms-selection ul li').each(function(){
								
								// Get the visible items in the list
								if($(this).is(":visible")){
									
									// Get the item caption
									//var caption = $(this).find('span').html();
									var caption = $(this).attr('id').split('-')[0];
									
									var option_value = caption;
							
									multi_input_values.push(option_value);
									tag_fields_val[name] = multi_input_values;
									
								}
								
							});
							
						}
						
					}else{
						
						tag_fields_val[$(this).attr('name')] = $(this).val();
						
					}
					
				}
				
			});
				
			var new_field_obj = {};
			new_field_obj[tag_name] = tag_fields_val;
			
			if(textarea_val != ''){
			
				var old_fields_obj = {};
				old_fields_obj = JSON.parse(textarea_val);
				
				var new_textarea_val = jQuery.extend({}, old_fields_obj, new_field_obj);
				
			}else var new_textarea_val = new_field_obj;

			// Copy the fields in the textarea
			$('textarea[data-helpers-textarea='+helpers_id+']').val(JSON.stringify(new_textarea_val));
			
			if(clicked_element == "cspml_update_tag_form"){
				
				// Update the tag container
				if(tag_label)
					$('div.cspml_tags_container.'+helpers_id+' div[class=cspml_tag_container][data-tag-name='+tag_name+'] strong.cspml_tag_label').empty().html(tag_label);
		
			}else{
			
				// Create the new tag container
				$('div.cspml_tags_container.'+helpers_id+'').append('<div class="cspml_tag_container" data-helpers-id="'+helpers_id+'" data-tag-name="'+tag_name+'"><strong>'+tag_label+'</strong><span class="cspml_remove_tag" data-helpers-id="'+helpers_id+'" data-tag-name="'+tag_name+'">Remove</span><span class="cspml_update_tag" data-helpers-id="'+helpers_id+'" data-tag-name="'+tag_name+'">Update</span></div>');
			
			}
			
			// Close the tag helpers container
			if($('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').is(":visible"))
				$('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').slideToggle(500, function(){
					jQuery('html, body').animate({
						scrollTop: jQuery('div.cspml_add_tag[data-helpers-container-id='+helpers_id+']').offset().top-150
					},function(){
						$('div.cspml_tag_warning[data-helpers-container-id='+helpers_id+']').fadeIn(500);
						updated_tag.push(1);
					});	
				});
		
		}
		
	});
	
	
	// Sort tags
	
	$('div.cspml_tags_container').sortable({
		
		stop: function(event, ui) {
			
			var helpers_id = $(this).attr('data-helpers-id');
			var order = [];
	
			$('div.cspml_tags_container div.cspml_tag_container[data-helpers-id='+helpers_id+']').each(function(i, el){
				var tag_name = $(el).attr('data-tag-name');
				order.push(tag_name);
			});
			
			// The old value of the textarea containing the tags
			var textarea_val = $('textarea[data-helpers-textarea='+helpers_id+']').val();
			
			var new_field_obj = {};			
			
			if(textarea_val != '' && order.length > 1){
			
				var old_fields_obj = {};
				old_fields_obj = JSON.parse(textarea_val);
				
				for (var i=0; i < order.length; i++){
					new_field_obj[order[i]] = old_fields_obj[order[i]];
				}

			}
			
			// Copy the fields in the textarea
			$('textarea[data-helpers-textarea='+helpers_id+']').val(JSON.stringify(new_field_obj));
			
			$('div.cspml_tag_warning[data-helpers-container-id='+helpers_id+']').fadeIn(500);
			
			updated_tag.push(1);

		}
		
	}).disableSelection();
	
	
	// Remove tag
	
	$('span.cspml_remove_tag').livequery('click', function(){
		
		var result = confirm("Are you sure?");
		
		if (result == true) {
	
			// Get the helpers id
			var helpers_id = $(this).attr('data-helpers-id');
			
			// Get the name of the tag to be removed
			var tag_name = $(this).attr('data-tag-name');

			// Get the value of the textarea containing the tags
			var textarea_val = JSON.parse($('textarea[data-helpers-textarea='+helpers_id+']').val());
						
			// Remove the tag
			delete textarea_val[tag_name];
			
			// Copy the fields in the textarea
			$('textarea[data-helpers-textarea='+helpers_id+']').val(JSON.stringify(textarea_val));
			
			// Create the new tag container
			$('div[class=cspml_tag_container][data-helpers-id='+helpers_id+'][data-tag-name='+tag_name+']').remove();
			
			$('div.cspml_tag_warning[data-helpers-container-id='+helpers_id+']').fadeIn(500);
			
			updated_tag.push(1);

		}else return;
		
	});
	
	
	// Set tag container with data when Update tag request is sent

	$('span.cspml_update_tag').livequery('click', function(){
	
		// Get the helpers id
		var helpers_id = $(this).attr('data-helpers-id');

		// Get the name of the tag to be updated	
		var tag_name = $(this).attr('data-tag-name');
					
		// Get the value of the textarea containing the tags
		var textarea_val = JSON.parse($('textarea[data-helpers-textarea='+helpers_id+']').val());

		// Loop throught each field in the tag container and add the fields values
		$('input[type=text][id^=tag_'+helpers_id+'], input[type=radio][id^=tag_'+helpers_id+'], input[type=checkbox][id^=tag_'+helpers_id+'], textarea[id^=tag_'+helpers_id+'], select[id^=tag_'+helpers_id+']').each(function(index, value) {
							
			// Checkbox and radio
			if($(this).is("input[type=radio]")){
		
				var field_name = $(this).attr('name');
				
				var checked_value = textarea_val[tag_name][field_name];

				$('input[name="'+field_name+'"]').each(function(){
					
					var checked = ($(this).attr('value') === checked_value);
					
					// The attribute should be set or unset...
					if(checked){
						//$(this).iCheck('check'); 
						$(this).prop('checked', true);
					}else{
						//$(this).iCheck('uncheck');
						$(this).prop('checked', false);
					}
					
				});
			
			}else if($(this).is("input[type=checkbox]")){
		
				var field_name = $(this).attr('name');
				var field_val = $(this).attr('value');		
				
				var name_array = field_name.split('[]');
				
				// Check from the name if the checkbox is an array
				if(name_array.length == 2){
				
					// If the checbox is an array
					// Get its elements from the data stored in the textarea
					var checkbox_array_elements = textarea_val[tag_name][name_array[0]];
					
					// Loop throught the checkbox elements
					$('input[name="'+field_name+'"]').each(function(){
						
						// Check the cheboxes if its value exists in the elements stored in the textarea
						var checked = jQuery.inArray($(this).attr('value'), checkbox_array_elements);
						
						// The attribute should be set or unset...
						if(checked > -1){
							//$(this).iCheck('check'); 
							$(this).prop('checked', true);
						}else{
							//$(this).iCheck('uncheck');
							$(this).prop('checked', false);
						}
						
					});
					
				}else{
						
					var checked_value = textarea_val[tag_name][field_name];
					var checked = (field_val === checked_value);
						
					if (checked){
						//$(this).iCheck('check');
						$(this).prop('checked', true);
					}else{
						//$(this).iCheck('uncheck');
						$(this).prop('checked', false);
					}
				
				}
							
			// Multiple select
			}else if($(this).is("select[multiple=multiple]")){
				
				var name_array = $(this).attr('name').split('[]');
				var field_name = name_array[0];
				var select_id = $(this).attr('id');
				
				if(name_array.length == 2){
					
					// Loop throught all the list // selection
					$('div#ms-'+select_id+' div.ms-selectable ul li').each(function(){
						
						var caption = $(this).attr('id').split('-')[0];
						
						// Hide the list item if it exists in the array
						if(jQuery.inArray(caption, textarea_val[tag_name][field_name]) > -1){
							
							$('.cspml_multi_select#'+field_name+'').multiSelect('deselect', caption);
							
						}
						
					});

					$('div#ms-'+select_id+' div.ms-selection ul li').each(function(){
						
						var caption = $(this).attr('id').split('-')[0];
						
						// show the list item if it exists in the array
						if(jQuery.inArray(caption, textarea_val[tag_name][field_name]) > -1){
							
							$('.cspml_multi_select#'+field_name+'').multiSelect('select', caption);
							
						}
						
					});
					
				}
				
			}else{
		
				var field_name = $(this).attr('name');
				var field_val =textarea_val[tag_name][field_name];	
							
				$(this).val(field_val);
				
			}
			
        });
		
		// Hide the submit button
		$('a[class=cspml_submit_tag_form][data-helpers-id='+helpers_id+']').hide(function(){
			// Show the update button and add to it the number of the tag to be updated
			$('a[class=cspml_update_tag_form][data-helpers-id='+helpers_id+']').attr('data-tag-name', tag_name).show();		
		});		
		
		// Open the tag helpers container
		if(!$('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').is(":visible"))
			$('div[class=cspml_add_tag_container][data-helpers-container-id='+helpers_id+']').slideToggle(500);
		
	});
	
	// Check if there's any unsaved tag data
	$(window).on('beforeunload', function(){
		
		if(updated_tag.length > 0)
			return 'You\'ve made changes on your settings which aren\'t saved. If you leave you will lose these changes.';
		
	});
	
	$('form#cspml_form').submit(function() {

   		$(window).unbind('beforeunload');
   
	});
	
});
	
Cufon.replace("div[class^=custom_section_] > h3, ul.codespacing_menu li, div[class^=custom_section_] > p, p.lorem, input.custom-button-primary, span.section_sub_title, span.section_sub_title_child");			


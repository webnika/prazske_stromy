<?php
/**
 * WordPress Settings Framework
 * 
 * @author Gilbert Pellegrom
 * @link https://github.com/gilbitron/WordPress-Settings-Framework
 * @version 1.4
 * @license MIT
 */

if( !class_exists('CsPml_WordPressSettingsFramework') ){
    /**
     * WordPressSettingsFramework class
     */
    class CsPml_WordPressSettingsFramework {
    
        /**
         * @access private
         * @var string 
         */
        private $option_group;
    	
		private $plugin_url;
		
		private $plugin_path;
		
		private $plugin_get_var = 'cspml';
		
		private static $_this;	
		
        /**
         * Constructor
         * 
         * @param string path to settings file
         * @param string optional "option_group" override
         */
        function __construct( $settings_file, $option_group = '' )
        {
             
			self::$_this = $this;       
			           
			if( !is_file( $settings_file ) ) return;
            require_once( $settings_file );
            
			$this->plugin_url = plugin_dir_url( __FILE__ );
			$this->plugin_path = plugin_dir_path( __FILE__ );
			
            $this->option_group = preg_replace("/[^a-z0-9]+/i", "", basename( $settings_file, '.php' ));
            if( $option_group ) $this->option_group = $option_group;
             
            add_action('admin_init', array(&$this, 'cspml_admin_init'));
            add_action('admin_notices', array(&$this, 'cspml_admin_notices'));
            add_action('admin_enqueue_scripts', array(&$this, 'cspml_admin_enqueue_scripts'));
			
        }
	
		static function this() {
			
			return self::$_this;
		
		}
   	
        /**
         * Get the option group for this instance
         * 
         * @return string the "option_group"
         */
        function cspml_get_option_group()
        {
            return $this->option_group;

        }
        
        /**
         * Registers the internal WordPress settings
         */
        function cspml_admin_init()
    	{	
			register_setting( $this->option_group, $this->option_group .'_settings', array(&$this, 'cspml_settings_validate') );
			$this->cspml_process_settings();
    	}
        
        /**
         * Displays any errors from the WordPress settings API
         */
        function cspml_admin_notices()
    	{
        	if (is_admin()) {
				
				// Get out if the loaded page is not our plguin settings page
				if (isset($_GET['page']) && $_GET['page'] == $this->plugin_get_var )
					settings_errors();
					
			}
			
    	}
    	
    	/**
         * Enqueue scripts and styles
         */
    	function cspml_admin_enqueue_scripts()
    	{
            
			if (is_admin()) {
						
				if (!class_exists('CodespacingProgressMap'))
					return; 
				
				$ProgressMapClass = CodespacingProgressMap::this();
				
				// Get out if the loaded page is not our plguin settings page
				if (isset($_GET['page']) && $_GET['page'] == $this->plugin_get_var ){
			
					// CSS
					
					wp_enqueue_style('farbtastic');
					wp_enqueue_style('thickbox');
		
					wp_register_style('cspacing_pml_admin_css', $this->plugin_url .'settings/css/admin_style.css');
					 wp_enqueue_style('cspacing_pml_admin_css');
					
					wp_register_style('cspacing_pml_multi_select_css', $ProgressMapClass->cspm_plugin_url .'settings/css/multi-select.css');
					 wp_enqueue_style('cspacing_pml_multi_select_css');
					
					// JS
					 
					wp_enqueue_script('jquery');
					wp_enqueue_script('farbtastic');
					wp_enqueue_script('media-upload');
					wp_enqueue_script('thickbox');
				
					wp_register_script('cspacing_pml_jquery_cookie', $ProgressMapClass->cspm_plugin_url .'settings/js/jquery_cookie.js', array( 'jquery' ), false, true);
					 wp_enqueue_script('cspacing_pml_jquery_cookie');
		
					wp_register_script('cspacing_pml_jquery_validate', $ProgressMapClass->cspm_plugin_url .'settings/js/jquery.validate.min.js', array( 'jquery' ), false, true);
					 wp_enqueue_script('cspacing_pml_jquery_validate');
					
					wp_register_script('cspacing_pml_cufon_font', $ProgressMapClass->cspm_plugin_url.'settings/js/cufon/cufon-yui.js', array(), false, true);
					 wp_enqueue_script('cspacing_pml_cufon_font');
					 
					wp_register_script('cspacing_pml_cufon_Linux_Biolinum', $ProgressMapClass->cspm_plugin_url.'settings/js/cufon/Linux_Biolinum_400.font.js', array(), false, true);
					 wp_enqueue_script('cspacing_pml_cufon_Linux_Biolinum');
					
					wp_register_script('cspacing_pml_jquery-ui_js', $this->plugin_url .'settings/js/jquery-ui-1.10.3.custom.min.js', array( 'jquery' ));
 					 wp_enqueue_script('cspacing_pml_jquery-ui_js');
					
					wp_register_script('cspacing_pml_admin_js', $this->plugin_url .'settings/js/admin_script.js', array( 'jquery' ), false, true);
					 wp_enqueue_script('cspacing_pml_admin_js');
					
					global $wp_settings_pml;
					$first_section = '';
					if(!empty($wp_settings_pml)){
						usort($wp_settings_pml, array(&$this, 'cspml_sort_array'));
						$first_section = $wp_settings_pml[0]['section_id'];				
					}
					
					wp_localize_script('cspacing_pml_admin_js', 'cspml_admin_vars', array(
						'plugin_url' => $this->plugin_url,
						'first_section' => $first_section,
					));
					
				}
			 
			}
			
    	}
		
     	/**
         * Adds a filter for settings validation
         * 
         * @param array the un-validated settings
         * @return array the validated settings
         */
    	function cspml_settings_validate( $input )
    	{	
    		return apply_filters( $this->option_group .'_settings_validate', $input );
    	}
    	
    	/**
         * Displays the "section_description" if speicified in $wp_settings_pml
         *
         * @param array callback args from add_settings_section()
         */
    	function cspml_section_intro( $args )
    	{
        	global $wp_settings_pml;
        	if(!empty($wp_settings_pml)){
        		foreach($wp_settings_pml as $section){
                    if($section['section_id'] == $args['id']){
                        if(isset($section['section_description']) && $section['section_description']) echo '<p style="margin-left:10px;"><strong>'. $section['section_description'] .'</strong></p>';
                        break;
                    }
        		}
            }
    	}
		
    	/**
         * Processes $wp_settings_pml and adds the sections and fields via the WordPress settings API
         */
    	function cspml_process_settings()
    	{
            global $wp_settings_pml;		
		
        	if(!empty($wp_settings_pml)){
        	    usort($wp_settings_pml, array(&$this, 'cspml_sort_array'));
        		foreach($wp_settings_pml as $section){
            		if(isset($section['section_id']) && $section['section_id'] && isset($section['section_title'])){                		
						add_settings_section( $section['section_id'], $section['section_title'], array(&$this, 'cspml_section_intro'), $this->option_group );
                		if(isset($section['fields']) && is_array($section['fields']) && !empty($section['fields'])){
                    		foreach($section['fields'] as $field){
                        		if(isset($field['id']) && $field['id'] && isset($field['title'])){									
                        		    add_settings_field( $field['id'], '<strong>'.$field['title'].'</strong>', array(&$this, 'cspml_generate_setting'), $this->option_group, $section['section_id'], array('section' => $section, 'field' => $field) );
								}
                    		}
                		}
            		}
        		}
    		}
    	}
    	
    	/**
         * Usort callback. Sorts $wp_settings_pml by "section_order"
         * 
         * @param mixed section order a
         * @param mixed section order b
         * @return int order
         */
    	function cspml_sort_array( $a, $b )
    	{
        	return $a['section_order'] > $b['section_order'];
    	}
    	
    	/**
         * Generates the HTML output of the settings fields
         *
         * @param array callback args from add_settings_field()
         */
    	function cspml_generate_setting( $args )
    	{
    	    $section = $args['section'];
            $defaults = array(
        		'id'      => 'default_field',
        		'title'   => 'Default Field',
        		'desc'    => '',
        		'std'     => '',
        		'type'    => 'text',
        		'choices' => array(),
        		'class'   => ''
        	);
        	$defaults = apply_filters( 'cspml_wpsf_defaults', $defaults );
        	extract( wp_parse_args( $args['field'], $defaults ) );
        	
        	$options = get_option( $this->option_group .'_settings' );
        	$el_id = $this->option_group .'_'. $section['section_id'] .'_'. $id;
        	$val = (isset($options[$el_id])) ? $options[$el_id] : $std;
        	
        	do_action('cspml_wpsf_before_field');
        	do_action('cspml_wpsf_before_field_'. $el_id);
    		switch( $type ){
    		    case 'text':
    		        $val = esc_attr(stripslashes($val));
    		        echo '<input type="text" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="'. $val .'" class="regular-text '. $class .'" />';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'textarea':
    		        $val = esc_html(stripslashes($val));
    		        echo '<textarea aria-describedby="newcontent-description" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" rows="5" cols="60" class="'. $class .'">'. $val .'</textarea>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'select':
    		        $val = esc_html(esc_attr($val));
    		        echo '<select name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" class="'. $class .'" style="width: 25em;">';
    		        foreach($choices as $ckey=>$cval){
        		        echo '<option value="'. $ckey .'"'. (($ckey == $val) ? ' selected="selected"' : '') .'>'. $cval .'</option>';
    		        }
    		        echo '</select>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
				case 'multi_select':
					$val = (is_array($val)) ? $val : array();
    		        echo '<select name="'. $this->option_group .'_settings['. $el_id .'][]" id="'. $el_id .'" class="'. $class .' cspml_multi_select" multiple="multiple" style="width: 25em; height:250px;">';
    		        foreach($choices as $ckey=>$cval){
        		        echo '<option value="'. $ckey .'"'. ((in_array($ckey, $val)) ? ' selected="selected"' : '') .'>'. $cval .'</option>';
    		        }
    		        echo '</select>';
					echo '<a class="cspml_ms_refresh" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Refresh</a>, ';
					echo '<a class="cspml_ms_select_all" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Select all</a>, ';
					echo '<a class="cspml_ms_deselect_all" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Deselect all</a>'; 
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'radio':
    		        $val = esc_html(esc_attr($val));
    		        foreach($choices as $ckey=>$cval){
        		        echo '<input type="radio" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' /><label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'checkbox':
    		        $val = esc_attr(stripslashes($val));
    		        echo '<input type="hidden" name="'. $this->option_group .'_settings['. $el_id .']" value="0" />';
    		        echo '<input type="checkbox" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="1" class="'. $class .'"'. (($val) ? ' checked="checked"' : '') .' /><label class="custom_wpsf" id="'. $ckey .'" for="'. $el_id .'">'. $desc .'</label>';
    		        break;
    		    case 'checkboxes':
    		        foreach($choices as $ckey=>$cval){
    		            $val = '';
    		            if(isset($options[$el_id .'_'. $ckey])) $val = $options[$el_id .'_'. $ckey];
    		            elseif(is_array($std) && in_array($ckey, $std)) $val = $ckey;
    		            $val = esc_html(esc_attr($val));
        		        echo '<input type="hidden" name="'. $this->option_group .'_settings['. $el_id .'_'. $ckey .']" value="0" />';
        		        echo '<input type="checkbox" name="'. $this->option_group .'_settings['. $el_id .'_'. $ckey .']" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' /><label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'checkboxes_array':
    		        $val = (is_array($val)) ? $val : array();
    		        foreach($choices as $ckey=>$cval){
						echo '<input type="checkbox" name="'. $this->option_group .'_settings['. $el_id .'][]" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. ((in_array($ckey, $val)) ? ' checked="checked"' : '') .' /><label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }				
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;					
    		    case 'color':
                    $val = esc_attr(stripslashes($val));
                    echo '<div style="position:relative;">';
    		        echo '<input type="text" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="'. $val .'" class="'. $class .'" />';
    		        echo '<div id="'. $el_id .'_cp" style="position:absolute;top:0;left:190px;background:#fff;z-index:9999;"></div>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        echo '<script type="text/javascript">
    		        jQuery(document).ready(function($){ 
                        var colorPicker = $("#'. $el_id .'_cp");
                        colorPicker.farbtastic("#'. $el_id .'");
                        colorPicker.hide();
                        $("#'. $el_id .'").live("focus", function(){
                            colorPicker.show();
                        });
                        $("#'. $el_id .'").live("blur", function(){
                            colorPicker.hide();
                            if($(this).val() == "") $(this).val("#");
                        });
                    });
                    </script></div>';
    		        break;
    		    case 'file':
                    $val = esc_attr($val);
    		        echo '<input type="text" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="'. $val .'" class="regular-text '. $class .'" /> ';
                    echo '<input type="button" class="button wpsf-browse" id="'. $el_id .'_button" value="Browse" />';
                    echo '<script type="text/javascript">
                    jQuery(document).ready(function($){
                		$("#'. $el_id .'_button").click(function() {
                			tb_show("", "media-upload.php?post_id=0&amp;type=image&amp;TB_iframe=true");
                			window.original_send_to_editor = window.send_to_editor;
                        	window.send_to_editor = function(html) {
                        		var imgurl = $("img",html).attr("src");
                        		$("#'. $el_id .'").val(imgurl);
                        		tb_remove();
                        		window.send_to_editor = window.original_send_to_editor;
                        	};
                			return false;
                		});
                    });
                    </script>';
					if($desc)  echo '<p class="description">'. $desc .'</p>';
                    break;
                case 'editor':
    		        wp_editor( $val, $el_id, array( 'textarea_name' => $this->option_group .'_settings['. $el_id .']' ) );
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'custom':
    		        echo $std;
    		        break;
				case 'tag':
					echo '<div class="cspml_tags_container '.$helpers_container_id.'" data-helpers-id="'.$helpers_container_id.'">';						
						if(!empty($options[$el_id])){
							$explode_tag_option = json_decode($options[$el_id]);														
							$tag_label = 'tag_'.$helpers_container_id.'_label';
							$tag_name  = 'tag_'.$helpers_container_id.'_name';
							$i=1;
							foreach($explode_tag_option as $tag_option){							
								$tag_option = (array) $tag_option;
								if(count($tag_option) > 0){
									echo '<div class="cspml_tag_container" data-helpers-id="'.$helpers_container_id.'" data-tag-name="'.$tag_option[$tag_name].'">';
										echo '<strong class="cspml_tag_label">'.$tag_option[$tag_label].'</strong>';
										echo '<span class="cspml_remove_tag" data-helpers-id="'.$helpers_container_id.'" data-tag-name="'.$tag_option[$tag_name].'">Remove</span>';
										echo '<span class="cspml_update_tag" data-helpers-id="'.$helpers_container_id.'" data-tag-name="'.$tag_option[$tag_name].'">Update</span>';
									echo '</div>';
									$i++;
								}
							}												
						}
					echo '</div>';
					echo '<textarea name="'. $this->option_group .'_settings['. $el_id .']" id="'.$el_id.'" data-helpers-textarea="'.$helpers_container_id.'" style="display:none;">'. $val .'</textarea>';
					echo '<div class="cspml_tag_warning" data-helpers-container-id="'.$helpers_container_id.'">Data updated. Please don\'t forget to save your data when you finish!</div>';
					echo '<div class="cspml_add_tag" id="'.$el_id.'" data-helpers-container-id="'.$helpers_container_id.'">Add new</div>';
					echo '<div style="clear:both"></div>';					
					echo '<div class="cspml_add_tag_container" id="'.$el_id.'" data-helpers-container-id="'.$helpers_container_id.'">';
						foreach($helpers as $helper){ 
							$this->cspml_generate_tag_helpers($helper);
						}
					echo '</div>';
					if($desc)  echo '<p class="description">'. $desc .'</p>';
					break;
        		default:
        		    break;
    		}
    		do_action('cspml_wpsf_after_field');
        	do_action('cspml_wpsf_after_field_'. $el_id);
    	}
		
		/*
		 * Generate the helpers for the tag formulaire
		 * Designed only for the purpose of this plugin
		 */
		function cspml_generate_tag_helpers($helpers){
    	   
            $defaults = array(
        		'id'      => 'default_field',
        		'title'   => 'Default Field',
        		'desc'    => '',
        		'std'     => '',
        		'type'    => 'text',
        		'choices' => array(),
        		'class'   => ''
        	);
        	
        	extract( wp_parse_args( $helpers, $defaults ) );
        	
        	$el_id = $id;
        	$val = $std;
    		
			switch( $type ){
    		    case 'text':
    		        $val = esc_attr(stripslashes($val));
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        echo '<input type="text" name="'. $el_id .'" id="'. $el_id .'" value="'. $val .'" class="regular-text '. $class .'" onKeyPress="return disableEnterKey(event)" />';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'textarea':
    		        $val = esc_html(stripslashes($val));
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        echo '<textarea aria-describedby="newcontent-description" name="'. $el_id .'" id="'. $el_id .'" rows="5" cols="60" class="'. $class .'">'. $val .'</textarea>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'select':
    		        $val = esc_html(esc_attr($val));
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        echo '<select name="'. $el_id .'" id="'. $el_id .'" class="'. $class .'" style="width: 25em;">';
    		        foreach($choices as $ckey=>$cval){
        		        echo '<option value="'. $ckey .'"'. (($ckey == $val) ? ' selected="selected"' : '') .'>'. $cval .'</option>';
    		        }
    		        echo '</select>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
				case 'multi_select':
					$val = (is_array($val)) ? $val : array();
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        echo '<select name="'. $el_id .'[]" id="'. $el_id .'" class="'. $class .' cspml_multi_select" multiple="multiple" style="width: 25em; height:250px;">';
    		        foreach($choices as $ckey=>$cval){
        		        echo '<option value="'. $ckey .'"'. ((in_array($ckey, $val)) ? ' selected="selected"' : '') .'>'. $cval .'</option>';
    		        }
    		        echo '</select>';
					echo '<a class="cspml_ms_refresh" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Refresh</a>, ';
					echo '<a class="cspml_ms_select_all" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Select all</a>, ';
					echo '<a class="cspml_ms_deselect_all" id="'. $el_id .'" style="font-size:11px; cursor:pointer;">Deselect all</a>'; 
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'radio':
    		        $val = esc_html(esc_attr($val));
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label><br />';
    		        foreach($choices as $ckey=>$cval){
        		        echo '<input type="radio" name="'. $el_id .'" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' onKeyPress="return disableEnterKey(event)" />';
						echo '<label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'checkbox':
    		        $val = esc_attr(stripslashes($val));
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label><br />';
    		        echo '<input type="hidden" name="'. $el_id .'" value="0" />';
    		        echo '<input type="checkbox" name="'. $el_id .'" id="'. $el_id .'" value="1" class="'. $class .'"'. (($val) ? ' checked="checked"' : '') .' onKeyPress="return disableEnterKey(event)" />';
					echo '<label class="custom_wpsf" id="'. $ckey .'" for="'. $el_id .'">'. $desc .'</label>';
    		        break;
    		    case 'checkboxes':
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label><br />';
    		        foreach($choices as $ckey=>$cval){
    		            $val = '';
    		            if(is_array($std) && in_array($ckey, $std)) $val = $ckey;
    		            $val = esc_html(esc_attr($val));
        		        echo '<input type="hidden" name="'. $el_id .'_'. $ckey .'" value="0" />';
        		        echo '<input type="checkbox" name="'. $el_id .'_'. $ckey .'" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' onKeyPress="return disableEnterKey(event)" />';
						echo '<label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'checkboxes_array':
    		        $val = (is_array($val)) ? $val : array();
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label><br />';
    		        foreach($choices as $ckey=>$cval){
						echo '<input type="checkbox" name="'. $el_id .'[]" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. ((in_array($ckey, $val)) ? ' checked="checked"' : '') .' onKeyPress="return disableEnterKey(event)" />';
						echo '<label class="custom_wpsf" id="'. $el_id .'_'. $ckey .'" for="'. $el_id .'_'. $ckey .'">'. $cval .'</label><br />';
    		        }				
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;					
    		    case 'color':
                    $val = esc_attr(stripslashes($val));
                    echo '<div style="position:relative;">';
					echo '<label class="label_wpsf">'. $title .'</label>';
    		        echo '<input type="text" name="'. $el_id .'" id="'. $el_id .'" value="'. $val .'" class="'. $class .'" onKeyPress="return disableEnterKey(event)" />';
    		        echo '<div id="'. $el_id .'_cp" style="position:absolute;top:0;left:190px;background:#fff;z-index:9999;"></div>';
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        echo '<script type="text/javascript">
    		        jQuery(document).ready(function($){ 
                        var colorPicker = $("#'. $el_id .'_cp");
                        colorPicker.farbtastic("#'. $el_id .'");
                        colorPicker.hide();
                        $("#'. $el_id .'").live("focus", function(){
                            colorPicker.show();
                        });
                        $("#'. $el_id .'").live("blur", function(){
                            colorPicker.hide();
                            if($(this).val() == "") $(this).val("#");
                        });
                    });
                    </script></div>';
    		        break;
    		    case 'file':
                    $val = esc_attr($val);
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        echo '<input type="text" name="'. $el_id .'" id="'. $el_id .'" value="'. $val .'" class="regular-text '. $class .'" onKeyPress="return disableEnterKey(event)" /> ';
                    echo '<input type="button" class="button wpsf-browse" id="'. $el_id .'_button" value="Browse" />';
                    echo '<script type="text/javascript">
                    jQuery(document).ready(function($){
                		$("#'. $el_id .'_button").click(function() {
                			tb_show("", "media-upload.php?post_id=0&amp;type=image&amp;TB_iframe=true");
                			window.original_send_to_editor = window.send_to_editor;
                        	window.send_to_editor = function(html) {
                        		var imgurl = $("img",html).attr("src");
                        		$("#'. $el_id .'").val(imgurl);
                        		tb_remove();
                        		window.send_to_editor = window.original_send_to_editor;
                        	};
                			return false;
                		});
                    });
                    </script>';
					if($desc)  echo '<p class="description">'. $desc .'</p>';
                    break;
                case 'editor':
					echo '<label class="label_wpsf" id="'. $el_id .'" for="'. $el_id .'">'. $title .'</label>';
    		        wp_editor( $val, $el_id, array( 'textarea_name' => $el_id ) );
    		        if($desc)  echo '<p class="description">'. $desc .'</p>';
    		        break;
    		    case 'custom':
    		        echo $std;
    		        break;
				case 'submit_tag':
					echo '<a class="cspml_submit_tag_form" id="'.$el_id.'" data-helpers-id="'.$helpers_id.'">Add</a>';										
					echo '<a class="cspml_update_tag_form" id="'.$el_id.'" data-helpers-id="'.$helpers_id.'" style="display:none">Update</a>';
					echo '<a class="cspml_cancel_tag_form" id="'.$el_id.'" data-helpers-id="'.$helpers_id.'">Cancel</a>';
					echo '<br style="clear:both" />';
					break;
        		default:
        		    break;
    		}		
			
		}
    
    	/**
         * Output the settings form
         */
        function cspml_settings()
        {
            do_action('cspml_wpsf_before_settings');
            ?>
            <form action="options.php" method="post" id="cspml_form">
                <?php do_action('cspml_wpsf_before_settings_fields'); ?>
                <?php settings_fields( $this->option_group ); ?>
				<?php cspml_wpsf_custom_do_settings_sections( $this->option_group ); ?>
        		<p class="submit" style="margin-left:10px; border-top:1px solid #e8ebec"><input type="submit" style="height:40px;" class="custom-button-primary" value="Save" /></p>
			</form>
    		<?php
    		do_action('cspml_wpsf_after_settings');
        }
    
    }   
}

if( !function_exists('cspml_wpsf_get_option_group') ){
    /**
     * Converts the settings file name to option group id
     * 
     * @param string settings file
     * @return string option group id
     */
    function cspml_wpsf_get_option_group( $settings_file ){
        $option_group = preg_replace("/[^a-z0-9]+/i", "", basename( $settings_file, '.php' ));
        return $option_group;
    }
}

if( !function_exists('cspml_wpsf_get_settings') ){
    /**
     * Get the settings from a settings file/option group
     * 
     * @param string path to settings file
     * @param string optional "option_group" override
     * @return array settings
     */
    function cspml_wpsf_get_settings( $settings_file, $option_group = '' ){
        $opt_group = preg_replace("/[^a-z0-9]+/i", "", basename( $settings_file, '.php' ));
        if( $option_group ) $opt_group = $option_group;
        return get_option( $opt_group .'_settings' );
    }
}

if( !function_exists('cspml_wpsf_get_setting') ){
    /**
     * Get a setting from an option group
     * 
     * @param string option group id
     * @param string section id
     * @param string field id
     * @return mixed setting or false if no setting exists
     */
    function cspml_wpsf_get_setting( $option_group, $section_id, $field_id ){
        $options = get_option( $option_group .'_settings' );
        if(isset($options[$option_group .'_'. $section_id .'_'. $field_id])) return $options[$option_group .'_'. $section_id .'_'. $field_id];
        return false;
    }
}

if( !function_exists('cspml_wpsf_delete_settings') ){
    /**
     * Delete all the saved settings from a settings file/option group
     * 
     * @param string path to settings file
     * @param string optional "option_group" override
     */
    function cspml_wpsf_delete_settings( $settings_file, $option_group = '' ){
        $opt_group = preg_replace("/[^a-z0-9]+/i", "", basename( $settings_file, '.php' ));
        if( $option_group ) $opt_group = $option_group;
        delete_option( $opt_group .'_settings' );
    }
}

if( !function_exists('cspml_wpsf_custom_do_settings_sections') ){
	function cspml_wpsf_custom_do_settings_sections( $page ) {
		global $wp_settings_sections, $wp_settings_fields;
	
		if ( ! isset( $wp_settings_sections ) || !isset( $wp_settings_sections[$page] ) )
			return;
	
		foreach ( (array) $wp_settings_sections[$page] as $section ) {
			echo '<div class="custom_section_'.$section["id"].'">';
				if ( $section['title'] )
					echo "<h3>{$section['title']}</h3>\n";
				
				if ( $section['callback'] )
					call_user_func( $section['callback'], $section );
						
				echo '<p style="border-top:1px solid #e8ebec; margin:10px 0 20px 0"></p>';
				
				if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
					continue;
				echo '<div class="cspml_form_container">';
				cspml_wpsf_custom_do_settings_fields( $page, $section['id'] );
				echo '</div>';
			echo '</div>';
		}
	}
}

if( !function_exists('cspml_wpsf_custom_do_settings_fields') ){
	function cspml_wpsf_custom_do_settings_fields($page, $section) {
		global $wp_settings_fields;
	
		if ( ! isset( $wp_settings_fields[$page][$section] ) )
			return;
	
		foreach ( (array) $wp_settings_fields[$page][$section] as $field ) {
			echo '<div class="cspml_top">';
				if ( !empty($field['args']['label_for']) )
					echo '<div class="cspml_label"><label for="' . esc_attr( $field['args']['label_for'] ) . '">' . $field['title'] . '</label></div>';
				else
					echo '<div class="cspml_label">' . $field['title'] . '</div>';
				echo '<div class="cspml_field">';
				call_user_func($field['callback'], $field['args']);
				echo '</div>';
				echo '<div style="clear:both"></div>';
			echo '</div>';
		}
	}
}

?>
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="footer" class="site-footer" role="contentinfo">
			<div class="site-inner footer-top col-md-12">
				<div class="col-md-12 col-sm-12">
					<div class="footer-content site-inner">
						<div class="row autors-wrap">
								<h3>Web Pražské stromy</h3>
								<div class="row">
									<p class="col-md-5 col-sm-5">
										<strong>Pražské stromy - průvodce po památných a významných stromech Prahy</strong><br/>
										<strong>Autor:</strong> Ing. Aleš Rudl (dendrolog), <strong>e-mail:</strong> prazskestromy@seznam.cz
									</p>

									<p class="social-icons col-md-7 col-sm-7">
										<a class="social-item fb-social" href="https://www.facebook.com/Pra%C5%BEsk%C3%A9-stromy-146916971986666/?fref=ts" alt="Facebook - Pražské stromy"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
										<a class="social-item rss-social" href="/prazskestromy/feed/"><i class="fa fa-rss-square" aria-hidden="true"></i></a>
									</p>
								</div>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							<div class="col-md-8 col-sm-8">
								<div class="row">
									<p>
										Novou podobu stránek vytvořila pro Agenturu Koniklec, o.p.s. <strong>Veronika Petříková -
										<a href="http://webnika.cz" target="_blank">www.webnika.cz</a>.</strong><br/>
										Stránky vznikly v rámci projektu Významné stromy - téma pro zapojování veřejnosti do ochrany
										životního prostředí měst a obcí.
									</p>
									<a href="#" target="_blank">
										<img src="http://localhost/prazskestromy/wp-content/uploads/2016/06/ak_crop-1.jpg"
											alt="Agentura Koniklec, o.p.s"/>
									</a>
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<div class="row">
									<img class="swiss-logo" src="http://webnika.cz/devel/prazskestromy/wp-content/uploads/2016/05/SWISS_Cntrb_LOGO_BILE.jpg" alt="Podpořeno z Programu švýcarsko-české spolupráce" />
									<p>
										Podpořeno z Programu švýcarsko-české spolupráce.<br/>
										Supported by a grant from Switzerland through the Swiss Contribution to the enlarged European Union.
									</p>
								</div>
							</div>
							<div classs="clearfix"></div>
						</div>


					</div>

					<!--<div class="footer-adv col-md-12">
						<img src="http://placehold.it/980x120">
					</div>-->
				</div>
			</div>

				<div class="site-info">
					<div class="site-inner">
						<?php
							/**
							 * Fires before the twentysixteen footer text for footer customization.
							 *
							 * @since Twenty Sixteen 1.0
							 */
							do_action( 'twentysixteen_credits' );
						?>
						<?php /*<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
						<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentysixteen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentysixteen' ), 'WordPress' ); ?></a>
						*/?>
						<p>&copy; 2016, Pražské stromy, Všechna práva vyhrazena</p>
					</div>
				</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
</body>
</html>

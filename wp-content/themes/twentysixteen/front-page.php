<?php
/**
 * Template Name: Home page
 */

get_header(); ?>

<div class="site-inner">
    <div id="hp">
        <main id="main" class="site-main" role="main">

            <!-- ROZCESTNIK -->
            <?php

                $args = array(
                    'cat' => 4,
                    'orderby' => 'meta_value_num',
                    'meta_key' => 'order',
                    'order' => 'ASC',
                    'posts_per_page' => 6
                );

                $temp = $wp_query;
                $wp_query = null;
                $wp_query = new WP_Query();
                $wp_query->query($args);

            ?>
            <?php $post_counter == 0; ?>

            <div class="row guide-post">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php $post_counter++; ?>

                    <?php
                        if ($post_counter == 1) {
                            echo "<div class='col-md-9 col-sm-9'>";
                        } elseif ($post_counter == 6) {
                            echo "<div class='col-md-3 col-sm-3'>";
                        }
                    ?>
                    <?php
                        $box_link = get_post_meta($post->ID, $key='odkaz', $single=true);
                    ?>
                    <a id="post-<?php the_ID(); ?>" class='hp-box item-<?php echo $post_counter ?>' href='<?php echo $box_link; ?>'>
                        <div class="frame">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="bg-title"></div>
                        <div class="content">
                            <h2><?php the_title(); ?></h2>
                            <p><?php echo get_the_content(); ?></p>
                        </div>
                    </a>

                    <?php
                        if ($post_counter == 2) {
                            echo "<div class='clearfix'></div>";
                        } elseif ($post_counter == 5) {
                            echo "<div class='clearfix'></div>";
                            echo "</div>";
                        } elseif ($post_counter == 6) {
                            echo "</div>";
                        }
                    ?>

                <?php
                    endwhile;
                    endif;
                ?>
            </div>

            <!-- AKTUALITY -->
            <?php
                $args = array(
                    'cat' => 3,
                    'posts_per_page' => 9
                );

                query_posts($args);
                if ( have_posts() ) :
            ?>
            <div class="row">
                <div id="primary" class="col-md-9 col-sm-9 content-area actualities">
                        <h2 class="title col-md-12"><span>Aktuality</span></h2>
                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                           /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                           ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="row">

                                    <div class="col-md-12 col-sm-12 img">
                                        <?php twentysixteen_post_thumbnail(); ?>
                                        <div class="date">
                                            <div class="day"><?php the_time('d')?></div>
                                            <div class="month"><?php the_time('M')?></div>
                                        </div>
                                    </div>
                                   <div class="content">
                                        <div class="col-md-12">
                                            <header class="entry-header">
                                                <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
                                                    <span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
                                                <?php endif; ?>

                                                <h2 class="entry-title">
                                                    <a href="<?php the_permalink() ?>" rel="bookmark">
                                                        <?php
                                                            $titleOrig = get_the_title();
                                                            $titleHtml = htmlentities($titleOrig);
                                                            $title = html_entity_decode($titleHtml, ENT_QUOTES, 'UTF-8');
                                                            $limit = "45";
                                                            $pad = "...";

                                                            if(strlen($title) >= ($limit+3)) {
                                                                $title = mb_substr($title, 0, $limit) . $pad;
                                                            }
                                                            echo $title;
                                                        ?>
                                                    </a>
                                                </h2>

                                            </header><!-- .entry-header -->

                                            <div class="entry-content">
                                                <?php
                                                    $titleOrig = get_the_content();
                                                    $title = html_entity_decode($titleOrig, ENT_QUOTES, 'UTF-8');
                                                    $limit = "100";
                                                    $pad = "...";

                                                    if(strlen($title) >= ($limit+3)) {
                                                        $title = substr($title, 0, $limit) . $pad;
                                                    }
                                                    echo $title;
                                                ?>
                                            </div><!-- .entry-content -->
                                            <div class="text-center"><a class="btn btn-default link" href="<?php the_permalink(); ?>">Více</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            </article><!-- #post-## -->
                        <?php
                        // End the loop.
                        endwhile;

                    // If no content, include the "No posts found" template.
                    else :
                        get_template_part( 'template-parts/content-page.php', 'none' );

                    endif;
                    ?>
                    <div class="clearfix"></div>
                    <div class="text-center link-wrap">
                        <a href="./aktuality" class="btn btn-primary"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Všechny aktuality</a>
                    </div>
                    <?php get_sidebar( 'content-bottom' ); ?>
                </div>

                <div class="col-md-3 col-sm-3">
                    <?php get_sidebar(); ?>
                </div>
                <div class="clearfix"></div>
            </div>


        </main><!-- .site-main -->



    </div><!-- .content-area -->

</div>
<?php get_footer(); ?>

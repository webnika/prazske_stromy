<?php

/*
----------------------------------------------

    Plugin Name: Actions Recent Posts
    Plugin URI: 
    Description: Výpis nejnovějších akcí
    Version: 1.0
    Author: Veronika Petříková
    Author URI: www.webnika.cz

----------------------------------------------
*/

// Acd function to widgets_init that´ll load our widget
add_action('widgets_init', 'ps_action_widgets');

//Register widget
function ps_action_widgets() {
    register_widget('ps_Action_Widget');
}

//Widget class
class ps_action_widget extends WP_Widget {

    /* --------- WIDGET SETUP -------------*/
    function ps_Action_Widget() {
        // widget settings
        $widget_ops = array(
            'classname' => 'w_action', 
            'description' => __( "Recent posts from actions category.") 
        );
        // Create the widget
        $this->WP_Widget('ps_action_widget', __('Výpis nejnovějších akcí'), $widget_ops);
    }

    /* ---------- DISPLAY WIDGET ----------*/
    function widget ($args, $instance) {
        extract($args);

        // Our variables from the widget settings
        $title = apply_filters('widget_title', $instance['title']);
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;

        // Filter posts
        $r = new WP_Query( apply_filters( 'widget_posts_args', array(
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true,
            'cat'                 => 5
        ) ) );

        if ($r->have_posts()) :
        ?>
        <?php echo $args['before_widget']; ?>
        <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
        <?php while ( $r->have_posts() ) : $r->the_post(); ?>
            <div class="action-item">
                <strong>
                    <?php
                        global $post;
                        $datum = get_post_meta($post->ID, 'date', true);
                        echo $datum;
                    ?>
                </strong>
                <p>
                    <?php
                        global $post;
                        $misto = get_post_meta($post->ID, 'place', true);
                        echo $misto;
                    ?>
                </p>
                <h4><a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a></h4>
                <p><?php twentysixteen_excerpt(); ?></p>
            </div>
        <?php endwhile; ?>
        <div class="text-center link-wrap">
            <a class="btn btn-primary" href="/prazskestromy/akce/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Všechny akce</a>
        </div>
        <?php echo $args['after_widget']; ?>
        <?php
        // Reset the global $the_post as this query will have stomped on it
        wp_reset_postdata();

        endif;
    }

    /*----------------- UPDATE WIDGET -----------------------*/
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['number'] = (int) $new_instance['number'];
        return $instance;
    }

    /*---------------- FORM WIDGET -----------------------*/
    public function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
        <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

<?php
    }


}
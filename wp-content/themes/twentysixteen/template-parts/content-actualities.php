<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
        
        <div class="col-md-12 col-sm-12 img">
            <?php twentysixteen_post_thumbnail(); ?>
            <div class="date">
                <div class="day"><?php the_time('d')?></div>
                <div class="month"><?php the_time('M')?></div>
            </div>
        </div>
       <div class="content">
            <div class="col-md-12">
                <!--<?php twentysixteen_post_thumbnail(); ?>-->
                <header class="entry-header">
                    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
                        <span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
                    <?php endif; ?>

                    <?php 
                        $titleOrig = get_the_title();
                        $titleHtml = htmlentities($titleOrig);
                        $title = html_entity_decode($titleHtml, ENT_QUOTES, 'UTF-8');
                        $limit = "50";
                        $pad = "...";
                    
                        if(strlen($title) >= ($limit+3)) {
                            $title = mb_substr($title, 0, $limit) . $pad; 
                        }
                    ?>
                    <h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php echo $title; ?></a></h2>
                    <!--<p class="category"><?php the_category(' ') ?></p>-->
                    
                </header><!-- .entry-header -->

                <div class="entry-content">
                    <?php 
                        $titleOrig = get_the_content();
                        $title = html_entity_decode($titleOrig, ENT_QUOTES, 'UTF-8');
                        $limit = "100";
                        $pad = "&hellip;";
                    
                        if(strlen($title) >= ($limit+3)) {
                            $title = substr($title, 0, $limit) . $pad; 
                        }
                        echo $title;
                    ?>
                </div><!-- .entry-content -->
                <div class="text-center"><a class="btn btn-default link" href="<?php the_permalink(); ?>">Více</a></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

</article><!-- #post-## -->
<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            
       <div class="content-wrap">
            <div class="content">
                <?php if ( has_post_thumbnail() ) : ?>
                    <div class="col-md-4">
                        <?php twentysixteen_post_thumbnail(); ?>
                    </div>
               <?php endif; ?>
                <div class="<?php if ( has_post_thumbnail() ) { echo 'col-md-8'; } else { echo 'col-md-12'; } ?>">
                    <header class="entry-header">
                        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
                            <span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
                        <?php endif; ?>

                        <?php 
                            $title = get_the_title();
                            $title = strip_tags($title); 

                            global $post;
                            $datum = get_post_meta($post->ID, 'date', true);
                            $misto = get_post_meta($post->ID, 'place', true);
                            $lenght = get_post_meta($post->ID, 'lenght', true);
                            $route = get_post_meta($post->ID, 'route', true);
                            $route_info = get_post_meta($post->ID, 'route_info', true);
                        ?>
                        <?php if ( !empty ($datum) || !empty ($misto) ||  !empty ($route) || !empty ($lenght) || !empty ($route_info) ) : ?>
                            <p class="date">
                                
                                <strong> 
                                    <?php
                                        if ( !empty ($datum) ) {
                                            echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
                                            echo '<span>' . $datum . '</span>';
                                        }
                                        if ( !empty ($misto) ) {
                                            echo " - ";
                                        }

                                        if ( !empty ($route) ) {
                                            echo '<i class="fa fa-map-signs" aria-hidden="true"></i>';
                                            echo '<span>' . $route . '</span>';
                                        }
                                        if (!empty ($lenght) || !empty ($route_info) ) {
                                            echo " - ";
                                        }

                                        if ( !empty ($lenght) ) {
                                            echo '<i class="fa fa-rocket" aria-hidden="true"></i>';
                                            echo '<span>'. $lenght . '</span>';
                                        }
                                        if (!empty ($route_info) ) {
                                            echo ' - ';
                                        }

                                        if ( !empty ($route_info) ) {
                                            echo '<i class="fa fa-info-circle" aria-hidden="true"></i>';
                                            echo '<span>' . $route_info . '</span>';
                                        }
                                    ?>
                                </strong>
                                <?php
                                    if ( ! empty ($misto) ) {
                                        echo '<span>';
                                        echo '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                                        echo $misto;
                                        echo '</span>';
                                    }
                                ?>
                            </p>
                        <?php endif; ?>
                        <h2 class="entry-title">
                            <a href="<?php the_permalink() ?>" rel="bookmark">
                                <?php 
                                    if ( strlen(get_the_title()) > 90 ) {
                                        echo substr(get_the_title(), 0, 90)."..."; 
                                    }
                                    else {
                                        echo get_the_title();
                                    }
                                ?>
                            </a>
                        </h2>
                        <div class="clearfix"></div>
                        <!--<p class="category"><?php the_category(' ') ?></p>-->
                        
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php 
                            if ( has_excerpt() ) {
                                the_excerpt();
                            }
                            elseif ( strlen(get_the_content()) > 280 ) {
                                echo substr(get_the_content(), 0, 280)."..."; 
                            }
                            else {
                                echo get_the_content();
                            }
                        ?>
                    </div><!-- .entry-content -->
                    <p><i>Publikováno: <?php the_time('j. n. Y')?></i></p>
                </div>
                <!--<div class="text-center"><a class="btn btn-default link" href="<?php the_permalink(); ?>">Více</a></div> -->
            </div>
        </div>
    <div class="clearfix"></div>

</article><!-- #post-## -->
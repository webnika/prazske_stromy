<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php echo do_shortcode('[codespacing_progress_map list_ext="yes"]'); ?>
    <div class="clearfix"></div>

</article><!-- #post-## -->
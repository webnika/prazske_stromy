<?php
/**
 * Template Name: Výpis výletů - audio
 */

get_header(); ?>

<div class="site-inner">
    <div id="primary" class="actuality-page content-area col-md-9 col-sm-9">
        <div class="list-page">
            <h2><i class="fa fa-map-signs" aria-hidden="true"></i> Naučná audiostezka</h2>
            <?php
                global $post;
                $perex = get_post_meta($post->ID, 'perex', true);
            ?>
            <p class="perex">
               <?php echo $perex; ?>
            </p>
            <div class="content">
                <?php
                    global $post;
                    $content = $post->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);

                    if ( !empty( $content ) ) :
                        echo $content;
                    endif;
                ?>
            </div>
            <?php 
                global $paged;
                $paged = get_query_var('page') ? get_query_var('page') : get_query_var ('paged');

                $args = array(
                    'cat' => 14,
                    'posts_per_page' => 6,
                    'paged' => $paged
                );
                query_posts($args);
                if ( have_posts() ) : 
            ?>
            <div class="row">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();
                        
                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'template-parts/content', 'articleListPage' );

                    // End the loop.
                    endwhile;

                    // Previous/next page navigation.
                    the_posts_pagination( array(
                        'prev_text'          => __( 'Previous page', 'twentysixteen' ),
                        'next_text'          => __( 'Next page', 'twentysixteen' ),
                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
                    ) );

                // If no content, include the "No posts found" template.
                else :
                    get_template_part( 'template-parts/content-page.php', 'none' );

                endif;
                ?>
            </div>
        </div>

        <?php get_sidebar( 'content-bottom' ); ?>

    </div><!-- .content-area -->

    <div class="col-md-3 col-sm-3">
        <?php get_sidebar(); ?>
    </div>
    <div class="clearfix"></div>
    
</div>
<?php get_footer(); ?>
